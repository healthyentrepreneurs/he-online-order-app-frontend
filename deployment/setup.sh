#!/usr/bin/env bash
pip install ansible
set -e
if [ -z "$1" ]; then
    if [ -f ./hosts ]; then
        HOSTS=hosts
    else
        echo "usage: $0 <system>"
        exit
    fi
else
    HOSTS=$1
fi

KEY_FILE=$2
if [ -n "${KEY_FILE-}" ]; then
    KEY_FILE=$2
else
    echo 'no server key selected '
    exit 1
fi

RUNNER_ROOT=$3
if [ -n "${RUNNER_ROOT-}" ]; then
    RUNNER_ROOT=$3
fi
echo $RUNNER_ROOT
ansible-playbook  site.yml -i $HOSTS --extra-vars="ansible_ssh_private_key_file=$KEY_FILE runner_root_dir=$RUNNER_ROOT" --verbose
