const workboxBuild = require("workbox-build");
// NOTE: This should be run *AFTER* all your assets are built
const buildSW = () => {
  // This will return a Promise
  return workboxBuild
    .injectManifest({
      globDirectory: "build/",
      globPatterns: ["**/*.{json,ico,js,png,html,txt,css,svg}"],
      swDest: "build/sw.js",
      swSrc: "src/sw.js",
    })
    .then(({ count, size, warnings }) => {
      // Optionally, log any warnings and details.
      warnings.forEach(console.warn);
      console.log(`${count} files will be precached, totaling ${size} bytes.`);
    });
};
buildSW();
