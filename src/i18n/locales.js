export const LOCALES = {
  ENGLISH: "en",
  JAPANESE: "ja",
  FRENCH: "fr",
  GERMAN: "de",
};