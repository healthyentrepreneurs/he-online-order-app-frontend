import React from 'react'
import PropTypes from "prop-types"
import HomeButton from "./home-button"

const PaymentSuccessful = (props) => {
  const {
    paymentType,
    orderName,
    invoiceAmountPaid,
  } = props
  return (
    <div style={{width: "100%", height: "400px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "flex-start"}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='100px' height="100px" src="/images/check3.jpg" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", 
        justifyContent: "flex-start", alignItems: "center", flexGrow: 1, paddingBottom: 10}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <h3 style={{textDecoration: "underline"}}>
            Payment successful
          </h3>
        </div>
        {paymentType === "invoice" ? (
          <>
            <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
              <p><span style={{fontWeight: "bold"}}>{invoiceAmountPaid}</span> has been</p>
            </div>
            <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
              <p>deducted from your</p>
            </div>
            <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
              <p>outstanding balance</p>
            </div>
          </>
        ) : (
          <>
            <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
              <p>Your payment is successful</p>
            </div>
            <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
              <p>and your Order ID is</p>
            </div>
            <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
              <p>#{orderName}</p>
            </div>
          </>
        )}
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end", flexGrow: 1}}>
          <HomeButton onClick={props.onHomeBtnClick} />
        </div>
      </div>
    </div>
  )
}

PaymentSuccessful.propTypes = {
  paymentType: PropTypes.oneOf(["invoice", "order"]),
  invoiceAmountPaid: PropTypes.string,
  orderName: PropTypes.string
}

export default PaymentSuccessful