import { Form, Row, Col, Radio } from 'antd'
import Steppers from "../../components/Steppers";
import PropTypes from "prop-types"
import React from 'react'
import MyButton from "../../components/Button"
import { InputField } from "../../components/InputField"
import { getPaymentMethods, numberWithCommas, prefixPhoneNumber } from '../../util/helpers/reusableFunctions'
import AddPhoneNumber from './add-phone-number'
import { FormattedMessage } from 'react-intl';

const PaymentForm = (props) => {
  const {
    onSubmit,
    showAddNewNumber,
    service,
    verifyingPhoneNumber,
    savingDefaultPhoneNumber,
    setShowAddNewNumber,
    handleSaveNewPhoneNumber,
    balance,
    validatingPhoneNumber,
    user,
    onPhoneNumberChange,
    initialValues,
    paymentType,
    onPaymentChange,
    paymentValue,
    maxAmount
  } = props

  const [form] = Form.useForm()
  
  return (
    <div style={{width: "auto", minHeight: "400px", display: "flex", flexDirection: "column"}}>
      {paymentType === "order" && (
        <div className="stepper-wrapper">
          <Steppers current={2} />
        </div>
      )}
      <div style={{width: "100%", flexGrow: 3, padding: "10px 40px 20px 40px"}}>
        <Row style={{marginBottom: 10}}>
          <Col span={24} style={{textAlign: "center", textDecoration: "underline"}}>
            <h3 className='generic-modal-title'>{paymentType === "order" ? "Pay with Mobile Money" : "Add payment"}</h3>
          </Col>
        </Row>
        {paymentType === "invoice" && (
          <Row style={{marginBottom: 5}}>
            <Col span={24} style={{textAlign: "left"}}>
              <span style={{marginRight: 3}}>Balance: </span><span>{balance}</span>
            </Col>
          </Row>
        )}
        {paymentType === "order" && (
          <Row style={{marginBottom: 5}}>
            <Col span={24} style={{textAlign: "left"}}>
              <span style={{marginRight: 3}}>Amount to pay: </span><span style={{fontWeight: "bold"}}>{numberWithCommas(props.total)} {user?.country?.currency}</span>
            </Col>
          </Row>
        )}
        <Row>
          <Col span={24}>
            <p className="select-text">Select Payment Method</p>
            <Radio.Group onChange={onPaymentChange} value={paymentValue}>
              {getPaymentMethods().includes("momo") ? (
                <Radio className="payment-modal-radio" value={"momo"}>
                  MTN
                </Radio>
              ) : null}
              {getPaymentMethods().includes("airtel") ? (
                <Radio className="payment-modal-radio" value={"airtel"}>
                  Airtel
                </Radio>
              ) : null}
              {getPaymentMethods().includes("mpesa") ? (
                <Radio className="payment-modal-radio" value={"mpesa"}>
                  Mpesa
                </Radio>
              ) : null}
              {getPaymentMethods().includes("ihela") ? (
                <Radio className="payment-modal-radio" value={"ihela"}>
                  EcoCash
                </Radio>
              ) : null}
            </Radio.Group>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form
              form={form}
              onFinish={values => {
                onSubmit(values)
              }}
              initialValues={initialValues}
              autoComplete="off"
            >
              {paymentType === "invoice" && (
                <InputField
                  label={<FormattedMessage id="FILL_PAYMENT_AMOUNT" />}
                  name="amount"
                  type="number"
                  rule={[
                    {required: true, message: "Amount required"},
                  ]}
                  onChange={e => {
                    e.persist()
                    if(e.target.value > maxAmount && paymentType === "invoice") {
                      form.setFieldsValue({"amount": maxAmount})
                    }
                  }}
                />
              )}
              <InputField
                label="Phone number"
                name="phone_number"
                onChange={(e) => {
                  e.persist()
                  onPhoneNumberChange(e.target.value)
                }}
              />
              {/* <Form.Item>
                {verifyingPhoneNumber ? "Payment service loading..." : (
                  <>
                    <span>
                      Payment Provider: {displayServiceProvider(service) || "N/A"}
                      <SyncOutlined
                        onClick={() => handleVerifyPhoneNumber(form.getFieldValue("phone_number"))} 
                        style={{marginLeft: 5}}
                        spin={validatingPhoneNumber}
                      />
                    </span>
                  </>
                )}
              </Form.Item> */}
              <MyButton
                text="Pay"
                htmlType="submit"
                disabled={verifyingPhoneNumber || !service}
              />
            </Form>
          </Col>
        </Row>
      </div>
      {showAddNewNumber && (
        <div style={{width: "100%", height: "100%", flexGrow: 1}}>
          <AddPhoneNumber
            phoneNumber={form.getFieldValue("phone_number")}
            onAccept={() => {
              const inputPhoneNumber = form.getFieldValue("phone_number")
              if(inputPhoneNumber) {
                const prefixed = prefixPhoneNumber(inputPhoneNumber, user?.country?.phone_number_prefix)
                handleSaveNewPhoneNumber(user?.id, {default_phone_number: prefixed})
              }
            }}
            onCancel={() => {
              setShowAddNewNumber(false)
              //form.submit()
            }}
            savingDefaultPhoneNumber={savingDefaultPhoneNumber}
            validatingPhoneNumber={validatingPhoneNumber}
          />
        </div>
      )}
    </div>
  )
}

PaymentForm.propTypes = {
  paymentType: PropTypes.oneOf(["order", "invoice"]),
  currency: PropTypes.string,
  onSubmit: PropTypes.func,
  showAddPhoneNumber: PropTypes.bool,
  onPhoneNumberChange: PropTypes.func,
  service: PropTypes.string,
  verifyingPhoneNumber: PropTypes.bool,
  savingDefaultPhoneNumber: PropTypes.bool,
  saveDefaultPhoneNumber: PropTypes.func,
  setCheckNumber: PropTypes.func,
  invoice: PropTypes.object,
  order: PropTypes.any,
  initialValues: PropTypes.object,
  amountToPay: PropTypes.string
}

export default PaymentForm