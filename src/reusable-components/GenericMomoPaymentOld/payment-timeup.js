import React from 'react'
import PropTypes from "prop-types"
import { Button } from 'antd'

const PaymentTimeup = (props) => {
  const {
    onWaitPayment,
    onSetPaymentProcessing
  } = props
  return (
    <div style={{width: "100%", height: "400px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "flex-start", padding: 20}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='70px' height="70px" src="/images/loader-40.svg" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", justifyContent: "space-around", alignItems: "center", 
        flexGrow: 1}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <h4 className='generic-modal-title'>
            Payment not yet processed
          </h4>
        </div>
        <p className='generic-modal-text'>
          Would you like to keep waiting? It might still take a few more minutes
        </p>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center", marginBottom: 5}}>
          <Button size="large" style={{width: 140}} onClick={e => onWaitPayment()} type="default">Yes</Button>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end"}}>
          <Button size="large" className='yellow-btn' style={{width: 140}} onClick={e => onSetPaymentProcessing()}>Check later</Button>
        </div>
      </div>
    </div>
  )
}

PaymentTimeup.propTypes = {
  timeRemaining: PropTypes.string,
  onWaitPayment: PropTypes.func,
  onSetPaymentProcessing: PropTypes.func
}

export default PaymentTimeup