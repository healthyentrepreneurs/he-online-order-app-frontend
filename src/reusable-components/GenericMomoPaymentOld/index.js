import React, { useState, useEffect } from 'react'
import PropTypes from "prop-types"
import Modal from "react-modal";
import PaymentForm from './payment-form'
import { getPhoneNumberWithoutPrefix, getTelcoNameFromPhoneNumber, numberWithCommas, prefixPhoneNumber } from '../../util/helpers/reusableFunctions'
import { verifyNumber } from '../../services/api/payments'
import { PAYMENT_WAIT_TIME } from '../../util/configs'
import { debounce } from 'lodash'
import PaymentProcessing from './payment-processing'
import PaymentTimeup from './payment-timeup'
import { history } from '../../util/helpers/browserHistory'
import { Card, message } from 'antd'
import CancelIcon from "../../assets/cancel.svg";

const GenericMomoPayment = props => {

  const {
    visible,
    isMakingPayment,
    paymentError,
    paymentTimeup,
    setPaymentTimeup,
    paymentProcessing,
    paymentSuccess,
    user,
    paymentType,
    order,
    invoice,
    total,
    saveDefaultNumberSuccess,
    saveDefaultPhoneNumber,
    savingDefaultPhoneNumber,
    makePaymentRequest,
    setPaymentProcessing,
    waitForPayment,
    closeModal,
  } = props

  //#######################Local State#####################
  const [service, setService] = useState(null)
  const [verifyingPhoneNumber, setVerifyingPhoneNumber] = useState(false)
  const [validatingPhoneNumber, setValidatingPhoneNumber] = useState(false)
  const [waitTime, setWaitTime] = useState(PAYMENT_WAIT_TIME)
  const [showAddNewNumber, setShowAddNewNumber] = useState(false)
  const [paymentValue, setPaymentValue] = useState(null);


  //########################Local Variables #################
  const customStyles = {
    content: {
      transform: "translate(-50%, -50%)",
      position: "absolute",
      top: "54%",
      left: "50%",
      padding: "0px",
      width: "340px",
      height: "fit-content",
      zIndex: 99999999999,
    },
  };

  //################# Functions ##############

  const onPaymentChange = (e) => {
    let selectedOption = e.target.value;
    setPaymentValue(selectedOption);
  };

  const handleSaveNewPhoneNumber = async (userId, payload) => {
    const { default_phone_number } = payload
    setValidatingPhoneNumber(true)
    try {
      const result = await verifyNumber(default_phone_number);
      const responseData = result?.data
      const type = responseData.carrier.type || null

      if(type === "mobile" ) {
        saveDefaultPhoneNumber(userId, payload)
      } else {
        message.error("Invalid phone number")
      }
    } catch (error) {
      message.error("Phone number validation failed!")
      //handleError(error);
    } finally {
      setValidatingPhoneNumber(false)
    }
  }

  const handleOnSubmit = (values) => {
    const phoneNumberPrefix = user?.country?.phone_number_prefix
    const inputPhoneNumber = values.phone_number
    const prefixedInputPhoneNumber = prefixPhoneNumber(inputPhoneNumber, phoneNumberPrefix)
    
    setWaitTime(PAYMENT_WAIT_TIME)
    const invoicePayload = {
      partial: true,
      phone_number: prefixedInputPhoneNumber,
      amount: values.amount,
      payment_method: service,
      invoice_id: invoice?.id,
    }
    const orderPayload = {
      partial: false,
      phone_number: prefixedInputPhoneNumber,
      amount: total,
      payment_method: service,
      order_id: props?.order_id,
    }

    if(paymentType === "invoice") {
      makePaymentRequest(invoicePayload)
    } else {
      makePaymentRequest(orderPayload)
    }
  }

  const handleGetPhoneNumberTelcoName = async (phoneNumber) => {
    const prefixedNumber = prefixPhoneNumber(phoneNumber, user?.country?.phone_number_prefix)
    setVerifyingPhoneNumber(true)
    try {
      const result = await verifyNumber(prefixedNumber)
      setService(getTelcoNameFromPhoneNumber(result?.data))
    } catch (error) {
      const errorMsg = error?.response?.data?.errors[0]?.detail
      //message.error(errorMsg)
    } finally {
      setVerifyingPhoneNumber(false)
    }
  }

  const handleVerifyPhoneNumber = (phoneNumber) => {
    handleGetPhoneNumberTelcoName(phoneNumber)
  }

  const getBalanceDisplay = () => {
    if(paymentType === "invoice") {
      return `${numberWithCommas(invoice?.balance)} ${user?.country?.currency}`
    } else {
      return ""
    }
  }

  const handlePhoneNumberChange = debounce((phoneNumber)=> {
    const userPhoneNumber = user?.phone_number;
    const userDefaultPhoneNumber = user?.default_phone_number
    const prefix = user?.country?.phone_number_prefix
    if(phoneNumber.length >= 8) {
      handleGetPhoneNumberTelcoName(phoneNumber)
    }
    if(getPhoneNumberWithoutPrefix(userDefaultPhoneNumber, prefix)?.includes(phoneNumber) 
        || getPhoneNumberWithoutPrefix(userPhoneNumber, prefix).includes(phoneNumber) || phoneNumber.length <= 8) {
      setShowAddNewNumber(false)
    } else {
      if(phoneNumber.length > 7) {
        setShowAddNewNumber(true)
      }
    }
  }, 1000)

  //################## Effects ###############
  useEffect(() => {
    
    if(isMakingPayment && waitTime === 0 && !paymentProcessing) {
      setPaymentTimeup()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [waitTime, isMakingPayment, paymentSuccess])

  useEffect(() => {
    if(!savingDefaultPhoneNumber && saveDefaultNumberSuccess) {
      setShowAddNewNumber(false)
    }
  }, [savingDefaultPhoneNumber, saveDefaultNumberSuccess])

  useEffect(() => {
    if(!isMakingPayment) {
      setShowAddNewNumber(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isMakingPayment, paymentSuccess, paymentError])

  useEffect(()=> {
    const userDefaultPhoneNumber = user?.default_phone_number || user?.phone_number || null
    if(userDefaultPhoneNumber) {
      handleGetPhoneNumberTelcoName(userDefaultPhoneNumber)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    let myInterval = setInterval(() => {
      if(waitTime > 0) {
        setWaitTime(waitTime => waitTime - 1)
      }
      if(waitTime === 0) {
        clearInterval(myInterval)
      }
    }, 1000)
    return () => {
      clearInterval(myInterval)
    }
  })

  return (
    <>
      <Modal 
        isOpen={visible} 
        onRequestClose={() => closeModal()}
        style={customStyles}
      >
        <Card className="payment-modal"
          bodyStyle={{paddingLeft: 0, paddingRight: 0, paddingBottom: 0, overflowY: "auto", maxHeight: 550, overflowX: "hidden"}}>
          <img
            src={CancelIcon}
            alt="Cancel"
            onClick={() => closeModal()}
            className="close-icon"
          />
          {(!isMakingPayment && !paymentTimeup) ? (
          <PaymentForm
            maxAmount={invoice?.balance}
            initialValues = {{
              phone_number: getPhoneNumberWithoutPrefix(user?.default_phone_number || user?.phone_number),
              amount: paymentType === "invoice" ? "" : order?.amount
            }}
            currency={user?.country?.currency}
            paymentType={paymentType}
            balance={getBalanceDisplay()}
            service={service}
            verifyingPhoneNumber={verifyingPhoneNumber}
            onPhoneNumberChange={handlePhoneNumberChange}
            onSubmit={handleOnSubmit}
            showAddNewNumber={showAddNewNumber}
            handleSaveNewPhoneNumber={handleSaveNewPhoneNumber}
            savingDefaultPhoneNumber={savingDefaultPhoneNumber}
            setShowAddNewNumber={setShowAddNewNumber}
            user={user}
            validatingPhoneNumber={validatingPhoneNumber}
            handleVerifyPhoneNumber={handleVerifyPhoneNumber}
            paymentValue={paymentValue}
            onPaymentChange={onPaymentChange}
            {...props}
          />
        ) : null}
        {(isMakingPayment && !paymentSuccess && !paymentError && !paymentTimeup) ? (
          <PaymentProcessing 
            timeRemaining={waitTime}
          />
        ) : null}
        {(isMakingPayment && !paymentSuccess && paymentTimeup) ? (
          <PaymentTimeup 
            onSetPaymentProcessing={() => {
              setPaymentProcessing()
              history.push("/dashboard")
            }}
            onWaitPayment={() => {
              waitForPayment()
              setWaitTime(PAYMENT_WAIT_TIME)
            }}
          />
        ) : null}
        {/* {(!isMakingPayment && paymentSuccess && !paymentError) ? (
          <PaymentSuccessful
            paymentType={paymentType}
            invoiceAmountPaid={`${numberWithCommas(paymentAmount)} ${user?.country?.currency}`}
            orderName={props.order_name}
            onHomeBtnClick={handleOnHomeButtonClick}
          />
        ) : null} */}
        </Card>
      </Modal>
    </>
  )

  // return (
  //   <>
  //     <Modal
  //       visible={visible}
  //       onCancel={() => closeModal()}
  //       maskClosable={false}
  //       bodyStyle={{padding: "20px 0px 0px 0px"}}
  //       footer={false}
  //     >
  //       {(!isMakingPayment && !paymentTimeup) ? (
  //         <PaymentForm
  //           maxAmount={invoice?.balance} 
  //           initialValues = {{
  //             phone_number: getPhoneNumberWithoutPrefix(user?.default_phone_number || user?.phone_number),
  //             amount: paymentType === "invoice" ? "" : order?.amount
  //           }}
  //           currency={user?.country?.currency}
  //           paymentType={paymentType}
  //           balance={getBalanceDisplay()}
  //           service={service}
  //           verifyingPhoneNumber={verifyingPhoneNumber}
  //           onPhoneNumberChange={handlePhoneNumberChange}
  //           onSubmit={handleOnSubmit}
  //           showAddNewNumber={showAddNewNumber}
  //           saveDefaultPhoneNumber={saveDefaultPhoneNumber}
  //           savingDefaultPhoneNumber={savingDefaultPhoneNumber}
  //           setShowAddNewNumber={setShowAddNewNumber}
  //           user={user}
  //           {...props}
  //         />
  //       ) : null}
  //       {(isMakingPayment && !paymentSuccess && !paymentError && !paymentTimeup) ? (
  //         <PaymentProcessing 
  //           timeRemaining={waitTime}
  //         />
  //       ) : null}
  //       {(isMakingPayment && !paymentSuccess && paymentTimeup) ? (
  //         <PaymentTimeup 
  //           onSetPaymentProcessing={() => {
  //             setPaymentProcessing()
  //             history.push("/dashboard")
  //           }}
  //           onWaitPayment={() => {
  //             waitForPayment()
  //             setWaitTime(PAYMENT_WAIT_TIME)
  //           }}
  //         />
  //       ) : null}
  //       {/* {(!isMakingPayment && paymentSuccess && !paymentError) ? (
  //         <PaymentSuccessful
  //           paymentType={paymentType}
  //           invoiceAmountPaid={`${numberWithCommas(paymentAmount)} ${user?.country?.currency}`}
  //           orderName={props.order_name}
  //           onHomeBtnClick={handleOnHomeButtonClick}
  //         />
  //       ) : null} */}
  //     </Modal>
  //   </>
  // )
}

GenericMomoPayment.propTypes = {
  paymentType: PropTypes.oneOf(["order", "invoice"]).isRequired,
  onMakePayment: PropTypes.func,
  visible: PropTypes.bool.isRequired,
  setVisible: PropTypes.func,
  user: PropTypes.object,
  invoice: PropTypes.any,
  order: PropTypes.any
}

export default GenericMomoPayment