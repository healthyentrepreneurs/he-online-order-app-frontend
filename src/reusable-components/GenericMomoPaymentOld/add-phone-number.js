import { Row, Col, Button } from 'antd'
import React from 'react'
import PropType from "prop-types"

const AddPhoneNumber = (props) => {
  const {
    onAccept,
    onCancel,
    phoneNumber,
    savingDefaultPhoneNumber,
    validatingPhoneNumber
  } = props
  return (
    <>
      <Row style={{height: "auto"}}>
        <Col span={24} className="add-new-phone-number-wrapper">
          <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span>The phone number <span style={{fontWeight: "bold"}}>{phoneNumber}</span> is new</span>
            </Col>
          </Row>
          <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span>payment number.</span>
            </Col>
          </Row>
          <Row>
            <Col span={24} style={{textAlign: "center"}}>
              <span>You want to use this number for future</span>
            </Col>
          </Row>
          <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span>orders?</span>
            </Col>
          </Row>
          <Row>
            <Col span={12} style={{textAlign: "center"}}>
              <Button loading={savingDefaultPhoneNumber || validatingPhoneNumber} onClick={e => onAccept()} style={{width: 100}} type="default">Yes</Button>
            </Col>
            <Col span={12} style={{textAlign: "center"}}>
              <Button 
                onClick={e => onCancel()} style={{width: 100}} 
                type="default"
              >
                No, thanks
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  )
}

AddPhoneNumber.propTypes = {
  onAccept: PropType.func,
  onCancel: PropType.func,
  verifyingPhoneNumber: PropType.bool
}

export default AddPhoneNumber