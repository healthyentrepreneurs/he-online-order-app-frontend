import React, { useEffect } from "react";
import { Card } from "antd";
import Modal from "react-modal";

import Steppers from "../../components/Steppers";
import "../../styles/cart.less";
import Button from "../../components/Button";
import CancelIcon from "../../assets/cancel.svg";
import SuccessIcon from "../../assets/success.svg";
import ErrorIcon from "../../assets/error.svg";
import { FormattedMessage, useIntl } from "react-intl";

const GenericModal = (props) => {
  const intl = useIntl()
  const {
    visible,
    closeModal,
    title,
    text,
    isSuccess = true,
    amountPaid,
    paymentType="order",
    showReturnHomeButton = true,
    buttonText = intl.formatMessage({ id: "RETURN_TO_HOME" }),
    showSteppers = true,
    executeAction
  } = props
  
  const customStyles = {
    content: {
      transform: "translate(-50%, -50%)",
      position: "absolute",
      top: "54%",
      left: "50%",
      padding: "0px",
      width: "340px",
      height: "fit-content",
      zIndex: 50000,
    },
  };

  const displayMessage = () => {
    if(paymentType === "invoice") {
      if(isSuccess) {
        return (
          <span>
            <span style={{fontWeight: "bold"}}>{amountPaid}</span>&nbsp;<FormattedMessage id="HAS_BEEN_DEDUCTED_FROM_BALANCE" />
          </span>
        )
      } else {
        return text;
      }
    } else {
      return text;
    }
  }

  useEffect(()=> {
    Modal.setAppElement("#root")
  }, [])
  return (
    <Modal isOpen={visible} onRequestClose={() => closeModal()} style={customStyles}>
      <Card className="generic-modal">
        <img
          src={CancelIcon}
          alt="Cancel"
          onClick={() => closeModal()}
          className="close-icon"
        />
        {showSteppers && (
          <div className="stepper-wrapper">
            <Steppers current={4} />
          </div>
        )}
        
        <div className="text-content">
          {isSuccess ? (
            <img src={SuccessIcon} alt="success" className="status-logo" />
          ) : (
            <img src={ErrorIcon} alt="error" className="status-logo" />
          )}
          <h4 className="title">{title}</h4>
          <p className="text">{displayMessage()}</p>
          {showReturnHomeButton && (
            <Button
              className="action-btn"
              text={buttonText ? buttonText : "Try again"}
              onClick={(e) => {
                e.preventDefault();
                executeAction();
              }}
            />
          )}
        </div>
      </Card>
    </Modal>
  );
};

export default GenericModal;
