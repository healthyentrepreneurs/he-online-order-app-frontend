import React, { useEffect, useRef, useState } from 'react'
import "../../styles/product-header.less"
import SearchBar from './components/expandable-search'
import PropTypes from "prop-types" 
import CreditBalance from './components/credit-balance'

const ProductHeaderBar = (props) => {
  const {
    searchTerm,
    onSearch,
    creditBalanceOnclick,
    handleChange,
    cleanProduct,
    hasCreditBalance=true,
    balance="23,000 UGX",
    creditBalanceUpdatedAt="12-03-2022 14:50",
  } = props

  const inputRef = useRef()
  const [onFocus, setOnFocus] = useState(false)

  const handleOnSearchBtnClick = (e) => {
    setOnFocus(true)
    if(inputRef.current) {
      inputRef.current.focus()
    }
  }

  const handleOnFocus = (e) => {
    setOnFocus(true)
  }

  const handleOnBlur = (e) => {
    setOnFocus(false)
  }


  const showSearchIcon = () => {
    return !onFocus && hasCreditBalance
  }

  // useEffect(() => {
  //   console.log('focus', onFocus)
  // }, [onFocus])

  return (
    <div className='product-header-bar'>
      <div
        className={showSearchIcon() ? "search-button" : "search-button-hidden"} 
      >
        <img src='images/magnifier.png' width="40px" height="40px" onClick={handleOnSearchBtnClick} />
      </div>
      <SearchBar 
        ref={inputRef}
        onFocus={handleOnFocus}
        onBlur={handleOnBlur}
        onChange={handleChange}
        cleanProduct={cleanProduct}
        hasCreditBalance={hasCreditBalance}
        hasFocus={onFocus}
      />
      <CreditBalance
        onFocus={onFocus}
        hasCreditBalance={hasCreditBalance}
        balance={balance}
        creditBalanceUpdatedAt={creditBalanceUpdatedAt}
        searchOnFocus={onFocus}
      />
    </div>
  )
}

ProductHeaderBar.propTypes = {
  searchTerm: PropTypes.string,
  onSearch: PropTypes.func,
  creditBalanceOnclick: PropTypes.func,
  hasCreditBalance: PropTypes.bool,
  balance: PropTypes.string,
  creditBalanceUpdatedAt: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  cleanProduct: PropTypes.func
}

export default ProductHeaderBar