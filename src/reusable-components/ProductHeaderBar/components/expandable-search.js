import { Input } from 'antd'
import React, { forwardRef } from 'react'
import PropTypes from "prop-types"
import SearchIcon from "../../../assets/search.svg";


const SearchBar = forwardRef((props, ref) => {
  const {
    onChange,
    searchTerm,
    onFocus,
    hasFocus,
    onBlur,
    hasCreditBalance,
    cleanProduct,
  } = props
  
  const showSearchInput = () => {
    let show = hasFocus || !hasCreditBalance
    return show
  }

  return (
    <div>
      <Input
        className={showSearchInput() ? 'search-input' : 'search-input-hidden'}
        ref={ref}
        onBlur={onBlur}
        onFocus={onFocus}
        placeholder="Search"
        suffix={<img src={SearchIcon} alt="search" />}
        onChange={onChange}
        value={searchTerm}
      />
    </div>
  )
})

SearchBar.propTypes = {
  onChange: PropTypes.func,
  searchTerm: PropTypes.string,
  onChange: PropTypes.func,
  cleanProduct: PropTypes.func
}

export default SearchBar