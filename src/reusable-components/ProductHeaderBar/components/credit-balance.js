import React from 'react'
import PropTypes from "prop-types"

const CreditBalance = (props) => {
  const {
    balance,
    creditBalanceUpdatedAt,
    creditBalanceOnClick,
    hasCreditBalance,
    searchOnFocus
  } = props

  const displayBar = () => {
    let show = hasCreditBalance && !searchOnFocus;
    return show
  }

  return (
    <div 
      className= {displayBar() ? 'credit-balance-wrapper' : 'credit-balance-wrapper-hidden'} 
      onClick={e => creditBalanceOnClick()}
    >
      <div className='top-wrapper'>
        <span className="credit-btn-label">CREDIT BALANCE:</span>
        <span className='credit-btn-value'>{balance}</span>
      </div>
      <div className='bottom-wrapper'>
        <span className='credit-btn-label'>Updated {creditBalanceUpdatedAt}</span>
      </div>
    </div>
  )
}

CreditBalance.propTypes = {
  balance: PropTypes.string.isRequired,
  creditBalanceUpdatedAt: PropTypes.string.isRequired
}

export default CreditBalance