import React from 'react'
import PropTypes from "prop-types"
import { displayServiceProvider } from '../../util/helpers/reusableFunctions'

const PaymentProcessingBar = ({provider, reduceMargin,...props}) => {
  
  return (
    <>
      <div className='payment-processing-bar-wrapper' style={{marginTop: reduceMargin ? "-25px" : "", marginBottom: 10}}>
        <div className='left-wrapper'>
          <div className='top-wrapper'>
            <span className='payment-processing-label'>PAYMENT PROCESSING ONGOING</span>
          </div>
          <div className='top-wrapper'>
            <span className='payment-processing-value'>ooo We are still processing your payment with {displayServiceProvider(provider)}</span>
          </div>
        </div>
        <div className='right-wrapper'>
          <img src="/images/loader-40.svg" alt='loader' />
        </div>
      </div>
    </>
  )
}

PaymentProcessingBar.propTypes = {
  provider: PropTypes.string
}

export default PaymentProcessingBar;