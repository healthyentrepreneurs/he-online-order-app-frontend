import React from 'react'
import PropTypes from "prop-types"
import { Button } from 'antd'
import { FormattedMessage } from 'react-intl'

const PaymentTimeup = (props) => {
  const {
    onWaitPayment,
    onSetPaymentProcessing,
    // onCheckOrderPaymentStatus
  } = props
  return (
    <div style={{width: "100%", height: "400px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "flex-start", padding: 20}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='70px' height="70px" src="/images/loader-40.svg" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", justifyContent: "space-around", alignItems: "center", 
        flexGrow: 1}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <h4 className='generic-modal-title'>
            <FormattedMessage id="PAYMENT_NOT_YET_PROCESSED" />
          </h4>
        </div>
        <p className='generic-modal-text'>
          <FormattedMessage id="WOULD_YOU_LIKE_TO_TAKE_FEW_MINUTES_TO_PROCESS" />
        </p>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center", marginBottom: 5}}>
          <Button size="large" style={{width: 140}} onClick={e => onWaitPayment()} type="default"><FormattedMessage id="YES" /></Button>
        </div>
        {/* <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end"}}>
          <Button size="large" className='grey-btn' style={{width: 140}} onClick={e => onCheckOrderPaymentStatus()}><FormattedMessage id="CHECK_STATUS" /></Button>
        </div> */}
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end"}}>
          <Button size="large" className='yellow-btn' style={{width: 140}} onClick={e => onSetPaymentProcessing()}><FormattedMessage id="CHECK_LATER" /></Button>
        </div>
      </div>
    </div>
  )
}

PaymentTimeup.propTypes = {
  timeRemaining: PropTypes.string,
  onWaitPayment: PropTypes.func,
  onSetPaymentProcessing: PropTypes.func
}

export default PaymentTimeup