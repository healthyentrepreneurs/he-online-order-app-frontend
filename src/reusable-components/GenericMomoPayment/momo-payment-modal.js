import React from 'react'
import PropTypes from 'prop-types'
import { Button, Col, Input, Row } from 'antd'
import { FormattedMessage } from 'react-intl'

const MomoPaymentModal = (props) => {
  
  return (
    <div id='my-payment-modal' className={props.visible ? 'my-payment-modal-show': 'my-payment-modal-hidden'}>
      <div className='my-payment-modal-content'>
        <p className='generic-modal-text'>
         <FormattedMessage id="SENDING_MONEY" />
        </p>
        <p className='generic-modal-text'>
          <FormattedMessage id="YOU_ARE_PAYING" />{` Healthy Entrepreneurs ${props.amount}`}
        </p>
        <p className='generic-modal-text'>
          <FormattedMessage id="ENTER_MOBILE_MONEY_PIN_TO_CONFIRM" />:
        </p>
        <p className='generic-modal-text'>
          <FormattedMessage id="NOTE" />: <FormattedMessage id="THIS_IS_A_TEST_PAYMENT" />.
        </p>
        <Row style={{marginBottom: 20}}> 
          <Col span={24}>
            <Input
              type='number'
              value={props.pin}
              onChange={e => {
                props.onChange(e.target.value)
              }}
            />
          </Col>
        </Row>
        <Row>
          <Col span={12} style={{textAlign: 'center'}}>
            <Button 
              type='text'
              onClick={() => {
                props.onCancel()
              }}
            >
              <FormattedMessage id="CANCEL" />
            </Button>
          </Col>
          <Col span={12} style={{textAlign: 'center'}}>
            <Button 
              type='text'
              onClick={() => props.onSubmit()}
            >
              <FormattedMessage id="SEND" />
            </Button>
          </Col>
        </Row>
      </div>
    </div>
  )
}

MomoPaymentModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  amount: PropTypes.string,
  pin: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
}

export default MomoPaymentModal