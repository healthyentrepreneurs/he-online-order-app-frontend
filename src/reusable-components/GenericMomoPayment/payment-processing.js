import React from 'react'
import PropTypes from "prop-types"
import { FormattedMessage } from 'react-intl'

const PaymentProcessing = (props) => {
  const {
    timeRemaining,
  } = props
  return (
    <div style={{width: "100%", height: "400px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "flex-start", padding: 20}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='90px' height="90px" src="/images/loader-40.svg" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", justifyContent: "flex-start", alignItems: "center", flexGrow: 1}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <h4 className='generic-modal-title'>
            <FormattedMessage id="PROCESSING_PAYMENT" />
          </h4>
        </div>
        <p className='generic-modal-text'>
          <FormattedMessage id="WAIT_PAYMENT_IS_PROCESSING" />
        </p>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end", flexGrow: 1}}>
          <p style={{fontWeight: "bold", fontSize: "16px"}}>{timeRemaining}</p>
        </div>
      </div>
    </div>
  )
}

PaymentProcessing.propTypes = {
  timeRemaining: PropTypes.any
}

export default PaymentProcessing