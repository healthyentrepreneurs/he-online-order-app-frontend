import { Row, Col, Button } from 'antd'
import React from 'react'
import PropType from "prop-types"
import { FormattedMessage } from 'react-intl'

const AddPhoneNumber = (props) => {
  const {
    onAccept,
    onCancel,
    phoneNumber,
    savingDefaultPhoneNumber,
    validatingPhoneNumber
  } = props
  return (
    <>
      <Row style={{height: "auto"}}>
        <Col span={24} className="add-new-phone-number-wrapper">
          <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span><FormattedMessage id="THE_PHONE_NUMBER_IS_NEW" values={{ phoneNumber }} /></span>
              {/* <span>The phone number <span style={{fontWeight: "bold"}}>{phoneNumber}</span> is a new</span> */}
            </Col>
          </Row>
          <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span><FormattedMessage id="PAYMENT_NUMBER" /></span>
              {/* <span>payment number.</span> */}
            </Col>
          </Row>
          <Row>
            <Col span={24} style={{textAlign: "center"}}>
              <span><FormattedMessage id="YOU_WANT_TO_USER_THIS_NUMBER_FOR_FUTURE_ORDERS" /></span>
              {/* <span>You want to use this number for future order?</span> */}
            </Col>
          </Row>
          {/* <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span>orders?</span>
            </Col>
          </Row> */}
          <Row>
            <Col span={12} style={{textAlign: "center"}}>
              <Button loading={savingDefaultPhoneNumber || validatingPhoneNumber} onClick={e => onAccept()} style={{width: 100}} type="default">
                <FormattedMessage id="YES" />
              </Button>
            </Col>
            <Col span={12} style={{textAlign: "center"}}>
              <Button 
                onClick={e => onCancel()} style={{width: 100}} 
                type="default"
              >
                <FormattedMessage id="NO_THANKS" />
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  )
}

AddPhoneNumber.propTypes = {
  onAccept: PropType.func,
  onCancel: PropType.func,
  verifyingPhoneNumber: PropType.bool
}

export default AddPhoneNumber