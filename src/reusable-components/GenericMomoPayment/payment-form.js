import { Form, Row, Col } from 'antd'
import Steppers from "../../components/Steppers";
import PropTypes from "prop-types"
import React from 'react'
import MyButton from "../../components/Button"
import { SyncOutlined } from "@ant-design/icons"
import { InputField } from "../../components/InputField"
import { displayServiceProvider, isServiceProviderConfiguredForCountry, isServiceProviderSupported, prefixPhoneNumber } from '../../util/helpers/reusableFunctions'
import AddPhoneNumber from './add-phone-number'
import { useWakeLock } from 'react-screen-wake-lock';
import { FormattedMessage, useIntl } from 'react-intl';

const PaymentForm = (props) => {
  const intl = useIntl()

  const {
    onSubmit,
    showAddNewNumber,
    service,
    verifyingPhoneNumber,
    savingDefaultPhoneNumber,
    setShowAddNewNumber,
    handleSaveNewPhoneNumber,
    balance,
    validatingPhoneNumber,
    user,
    onPhoneNumberChange,
    handleVerifyPhoneNumber,
    initialValues,
    paymentType,
    maxAmount,
    isTestPayment,
    disableAmount=false,
  } = props

  const { isSupported, request } = useWakeLock({
    onRequest: () => console.log('Screen Wake Lock: requested!'),
    onError: () => console.log('An error happened'),
    onRelease: () => console.log('Screen Wake Lock: released!'),
  })

  const [form] = Form.useForm()

  const serviceProviderText = () => {
    if(service) {
      if(!isServiceProviderSupported(service)) {
        return "Unknown service provider"
      } else if(!isServiceProviderConfiguredForCountry(service)) {
        return `${displayServiceProvider(service)} not allowed`
      } else {
        return displayServiceProvider(service)
      }
    } else {
      return "Unknown"
    }
  }

  const getPaymentTitleText = () => {
    if(isTestPayment) {
      return <FormattedMessage id="TEST_PAYMENT" />
    } else {
      if(paymentType === "order") {
        return <FormattedMessage id="PAY_WITH_MOBILE_MONEY" />
      } else {
        return <FormattedMessage id="ADD_PAYMENT" />
      }
    }
  } 
  
  return (
    <div style={{width: "auto", minHeight: "400px", display: "flex", flexDirection: "column"}}>
      {paymentType === "order" && (
        <div className="stepper-wrapper">
          <Steppers current={2} />
        </div>
      )}
      <div style={{width: "100%", flexGrow: 3, padding: "10px 40px 20px 40px"}}>
        <Row style={{marginBottom: 10}}>
          <Col span={24} style={{textAlign: "center", textDecoration: "underline"}}>
            <h3 className='generic-modal-title'>{getPaymentTitleText()}</h3>
          </Col>
        </Row>
        {paymentType === "invoice" && (
          <Row style={{marginBottom: 5}}>
            <Col span={24} style={{textAlign: "left"}}>
              <span style={{marginRight: 3}}><FormattedMessage id="BALANCE" />: </span><span>{balance}</span>
            </Col>
          </Row>
        )}
        {paymentType === "order" && (
          <Row style={{marginBottom: 5}}>
            <Col span={24} style={{textAlign: "left"}}>
              <span style={{marginRight: 3}}><FormattedMessage id="AMOUNT_TO_PAY" />: </span><span style={{fontWeight: "bold"}}>{Math.ceil(props.total)} {user?.country?.currency}</span>
            </Col>
          </Row>
        )}
        <Row>
          <Col span={24}>
            <Form
              form={form}
              onFinish={values => {
                console.log('values', values)
                //lets lock screen awake
                if(isSupported) {
                  request()
                }

                const payload = {
                  ...values,
                  isTestPayment: isTestPayment,
                }
                onSubmit(payload)
              }}
              initialValues={initialValues}
              autoComplete="off"
            >
              {paymentType === "invoice" && (
                <InputField
                  label={<FormattedMessage id="FILL_PAYMENT_AMOUNT" />}
                  disabled={disableAmount}
                  name="amount"
                  type="number"
                  rule={[
                    {required: true, message: "Amount required"},
                  ]}
                  onChange={e => {
                    e.persist()
                    if(e.target.value > maxAmount && paymentType === "invoice") {
                      form.setFieldsValue({"amount": maxAmount})
                    }
                  }}
                />
              )}
              <InputField
                type="number"
                label={intl.formatMessage({ id:"PHONE_NUMBER" })}
                name="phone_number"
                onChange={(e) => {
                  e.persist()
                  onPhoneNumberChange(e.target.value)
                }}
              />
              <Form.Item>
                {verifyingPhoneNumber ? (<><FormattedMessage id="PAYMENT_SERVICE_LOADING" />...</>) : (
                  <>
                    <span>
                      <FormattedMessage id="PAYMENT_PROVIDER" />: {serviceProviderText()}
                      {!service && (
                        <SyncOutlined
                          onClick={() => {
                            const phoneNumber = form.getFieldValue("phone_number")
                            handleVerifyPhoneNumber(prefixPhoneNumber(phoneNumber, user?.country?.phone_number_prefix))
                          }}
                          style={{marginLeft: 5}}
                          spin={validatingPhoneNumber}
                        />
                      )}
                    </span>
                  </>
                )}
              </Form.Item>
              <MyButton
                text={<FormattedMessage id="PAY" />}
                htmlType="submit"
                disabled={verifyingPhoneNumber || !isServiceProviderSupported(service) 
                    || !isServiceProviderConfiguredForCountry(service) || !service}
              />
              {isTestPayment && (
                <Form.Item>
                  <p className='generic-modal-text'>
                    <FormattedMessage id="NOTE" />: <FormattedMessage id="YOU_ARE_DOING_TEST_ORDER" />
                  </p>
                  <p className='generic-modal-text'>
                    <FormattedMessage id="ORDER_WILL_NOT_BE_PAID" />
                  </p>
                </Form.Item>
              )}
            </Form>
          </Col>
        </Row>
      </div>
      {showAddNewNumber && (
        <div style={{width: "100%", height: "100%", flexGrow: 1}}>
          <AddPhoneNumber
            phoneNumber={form.getFieldValue("phone_number")}
            onAccept={() => {
              const inputPhoneNumber = form.getFieldValue("phone_number")
              if(inputPhoneNumber) {
                const prefixed = prefixPhoneNumber(inputPhoneNumber, user?.country?.phone_number_prefix)
                handleSaveNewPhoneNumber(user?.id, {default_phone_number: prefixed})
              }
            }}
            onCancel={() => {
              setShowAddNewNumber(false)
              //form.submit()
            }}
            savingDefaultPhoneNumber={savingDefaultPhoneNumber}
            validatingPhoneNumber={validatingPhoneNumber}
          />
        </div>
      )}
    </div>
  )
}

PaymentForm.propTypes = {
  paymentType: PropTypes.oneOf(["order", "invoice"]),
  currency: PropTypes.string,
  onSubmit: PropTypes.func,
  showAddPhoneNumber: PropTypes.bool,
  onPhoneNumberChange: PropTypes.func,
  service: PropTypes.string,
  verifyingPhoneNumber: PropTypes.bool,
  savingDefaultPhoneNumber: PropTypes.bool,
  saveDefaultPhoneNumber: PropTypes.func,
  setCheckNumber: PropTypes.func,
  invoice: PropTypes.object,
  order: PropTypes.any,
  initialValues: PropTypes.object,
  amountToPay: PropTypes.string
}

export default PaymentForm