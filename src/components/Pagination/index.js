import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox, Row, Col } from 'antd';
import { withFormik, Field, FormikProps } from 'formik';
import { Pagination as RNPagination } from 'antd';
import moment from 'moment';

const FormItem = Form.Item;

export default class Pagination extends Component {
  state = {
    limit: 10
  }

  onChange = page => {
    const { searchQuery, handleFetchIncidents, modifyPagination } = this.props;
    const offset = this.state.limit * (page - 1);
    const limit = this.state.limit
    modifyPagination({ currentPage: page });
    handleFetchIncidents({...searchQuery, limit, offset, currentPage: page});
  };

  render() {
    const { totalCount, currentPage } = this.props;
    const itemsPerPage = this.state.limit
    const totalPages = Math.ceil(totalCount / itemsPerPage)
    return ( 
      <>
        <RNPagination 
          size="small" 
          total={parseInt(totalCount)} 
          onChange={this.onChange} 
          defaultCurrent={1}
          current={currentPage}
          showTotal={(total, range) => `${range[0]}-${range[1]} of ${total} items`} />
      </>
    );
  }
}


