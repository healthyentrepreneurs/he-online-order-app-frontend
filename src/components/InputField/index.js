import React from 'react';
import { Form, Input } from 'antd';

import '../../styles/forms.less';

const FormItem = Form.Item;

export const InputField = (props) => {
	const { label, myStyle, onChange, max="", type="text", suffix = null } = props;

	let rules = [
		{
			required: true,
			message: 'This field is required'
		}
	]

	if (props.rules){
		rules.push(props.rules);
		rules.push({
			pattern: /^[0-9]*$/,
			message: 'Please enter only numeric values'
		})
	}
	return (
		<React.Fragment>
			{label && <p className="label">{label}</p>}
			<FormItem
				style={myStyle ? myStyle : {}}
				className="he-form"
				name={props.name}
				rules={rules}
			>
				<Input
					onChange={value => {
						if(onChange) {
							onChange(value)
						}
					}}
					max={max}
					placeholder={props.placeholder}
					className="input"
					type={type}
					inputMode={props.inputmode ? props.inputmode : null}
					pattern={props.pattern}
					disabled={props.disabled}
          suffix={suffix}
				/>
			</FormItem>
	  </React.Fragment>
	);
}

