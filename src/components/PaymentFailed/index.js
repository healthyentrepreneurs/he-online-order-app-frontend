import React from 'react'
import PropTypes from "prop-types"
import HomeButton from "../../containers/PrivateRoutes/CreditOverview/home-button"

const PaymentFailed = (props) => {
  const {
    errorMessage,
  } = props
  return (
    <div style={{width: "100%", height: "100%", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "space-around", padding: 20}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='100px' height="100px" src="/images/error.png" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", justifyContent: "flex-start", alignItems: "center", flexGrow: 1}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <span style={{fontWeight: "bold", textDecoration: "underline", fontSize: "16px"}}>
            Payment Failed
          </span>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>Your payment has failed</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>Error: {errorMessage}</p>
        </div>
        <div style={{display: "flex", flexGrow: 1, flexDirection:"row", justifyContent: "row", alignItems: "flex-end"}}>
          <HomeButton />
        </div>
      </div>
    </div>
  )
}

PaymentFailed.propTypes = {
  orderId: PropTypes.string,
  errorMessage: PropTypes.string
}

export default PaymentFailed