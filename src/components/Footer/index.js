import React from "react";
import { Layout } from "antd";
import  "../../styles/footer.less";

const { Footer } = Layout

const CustomFooter = props => {
  const currentDate = new Date()

  return (
    <Footer className="footer">
      <p>{`Copyright © ${currentDate.getFullYear()}. Healthy Entrepreneurs`}</p>
    </Footer>
  )
}

export default CustomFooter;