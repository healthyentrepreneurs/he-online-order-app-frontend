import React from 'react'
import PropTypes from "prop-types"
import HomeButton from "../../containers/PrivateRoutes/CreditOverview/home-button"
import { Button } from 'antd'

const PaymentNotYetProcessed = (props) => {
  const {
    timeRemaining="196",
    orderId="112223",
  } = props
  return (
    <div style={{width: "100%", height: "100%", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "flex-start"}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='70px' height="70px" src="/images/loader-40.svg" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", justifyContent: "flex-start", alignItems: "center", flexGrow: 1}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <span style={{fontWeight: "bold", textDecoration: "underline", fontSize: "16px"}}>
            Payment not yet
          </span>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <span style={{fontWeight: "bold", textDecoration: "underline", fontSize: "16px"}}>
            processed
          </span>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>Would you like to keep waiting?</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>It might still take a few more</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>minutes</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end", flexGrow: 1}}>
          <Button color="#ffffff" style={{width: 100}}>
            Yes
          </Button>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end", flexGrow: 1}}>
          <Button className='he-button-yellow' style={{width: 100}}>
            Check later
          </Button>
        </div>
      </div>
    </div>
  )
}

PaymentNotYetProcessed.propTypes = {
  
}

export default PaymentNotYetProcessed