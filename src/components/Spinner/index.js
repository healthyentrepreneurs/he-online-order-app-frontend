import React from "react";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

const Spinner = ({ color = "#30994b", indicator }) => {
	let antIcon;
	if (indicator) antIcon = indicator;
	else antIcon = <LoadingOutlined type="loading" style={{ fontSize: 24 }} spin />;

	return <Spin indicator={antIcon} style={{ color }} />;
};

export default Spinner;
