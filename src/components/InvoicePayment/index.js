import React from 'react'
import PropTypes from "prop-types"
import InvoicePaymentForm from './components/invoice-payment-form'
import PaymentProcessing from './components/payment-processing'
import PaymentFailed from './components/payment-failed'
import PaymentSuccess from './components/payment-success'
import PaymentTimeup from './components/payment-timeup'

const InvoicePayment = props => {
  //##################### Props #####################
  const {
    invoice,
    verifyingPhoneNumber,
    paymentSuccess,
    paymentTimeup,
    makingPayment,
    paymentErrorMessage,
    onSkipPayment,
    onWaitPayment,
    timeRemaining,
    makePayment,
    user,
    updateDefaultPaymentPhoneNumber,
    showUpdateDefaultPaymentPhoneNumber,
    setShowUpdateDefaultPaymentPhoneNumber,
    retry,
  } = props

  //#################### Local State ####################


  //###################### Helper Functions ######################



  return (
    <>
      {(!makingPayment && !paymentSuccess && !paymentErrorMessage && !paymentTimeup) ? (
        <InvoicePaymentForm
          onSubmit={makePayment}
          invoice={invoice}
          currency={user?.country?.currency}
          user={user}
          updateDefaultPaymentPhoneNumber={updateDefaultPaymentPhoneNumber}
          verifyingPhoneNumber={verifyingPhoneNumber}
          showUpdateDefaultPaymentPhoneNumber={showUpdateDefaultPaymentPhoneNumber}
          setShowUpdateDefaultPaymentPhoneNumber={setShowUpdateDefaultPaymentPhoneNumber}
          {...props}
        />
      ) : null}
      {(makingPayment && !paymentSuccess && !paymentErrorMessage && !paymentTimeup) ? (
        <PaymentProcessing 
          timeRemaining={timeRemaining}
        />
      ) : null}
      {(!makingPayment && !paymentSuccess && paymentErrorMessage) ? (
        <PaymentFailed 
          errorMessage={paymentErrorMessage}
          retry={retry}
        />
      ) : null}
      {(!makingPayment && paymentSuccess && !paymentErrorMessage) ? (
        <PaymentSuccess 
          currency={user?.country?.currency}
        />
      ) : null}
      {(makingPayment && !paymentSuccess && !paymentErrorMessage && paymentTimeup) ? (
        <PaymentTimeup 
          onSkipPayment={onSkipPayment}
          onWaitPayment={onWaitPayment}
        />
      ) : null}
    </>
  )

}

InvoicePayment.propTypes = {
  makingPayment: PropTypes.bool,
  paymentSuccess: PropTypes.bool,
  paymentTimeup: PropTypes.bool,
  invoice: PropTypes.object,
  user: PropTypes.object,
  updateDefaultPaymentPhoneNumber: PropTypes.func,
  showUpdateDefaultPaymentPhoneNumber: PropTypes.bool,
  paymentErrorMessage: PropTypes.string,
  timeRemaining: PropTypes.string,
  verifyingPhoneNumber: PropTypes.bool,
  onSkipPayment: PropTypes.func,
  onWaitPayment: PropTypes.func,
  setShowUpdateDefaultPaymentPhoneNumber: PropTypes.func,
  retry: PropTypes.func
}

export default InvoicePayment