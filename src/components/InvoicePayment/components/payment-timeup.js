import React from 'react'
import PropTypes from "prop-types"
import { Button } from 'antd'

const PaymentTimeup = (props) => {
  const {
    onSkipPayment,
    onWaitPayment
  } = props
  return (
    <div style={{width: "100%", height: "400px", display: "flex", flexDirection: "column", justifyContent: "center", 
      alignItems: "flex-start", padding: 20}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='70px' height="70px" src="/images/loader-40.svg" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", justifyContent: "space-around", alignItems: "center", 
        flexGrow: 1}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <h3 style={{textDecoration: "underline"}}>
            Payment not yet
          </h3>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <h3 style={{textDecoration: "underline"}}>
            processed
          </h3>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>Would you like to keep waiting?</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>It might still take  a few more</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>minutes.</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center", marginBottom: 5}}>
          <Button style={{width: 100}} onClick={e => onWaitPayment()} type="default">Yes</Button>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end"}}>
          <Button className='yellow-btn' style={{width: 100}} onClick={e => onSkipPayment()}>Check later</Button>
        </div>
      </div>
    </div>
  )
}

PaymentTimeup.propTypes = {
  timeRemaining: PropTypes.string,
  onSkipPayment: PropTypes.func,
  onWaitPayment: PropTypes.func
}

export default PaymentTimeup