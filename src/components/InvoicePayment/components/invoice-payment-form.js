import { Form, Row, Col } from 'antd'
import React from 'react'
import MyButton from "../../../components/Button"
import { InputField } from "../../../components/InputField"
import { getPhoneNumberWithoutPrefix, numberWithCommas } from '../../../util/helpers/reusableFunctions'
import PropTypes from "prop-types"
import AddPhoneNumber from './add-phone-number'
import { FormattedMessage } from 'react-intl'

const InvoicePaymentForm = (props) => {
  const {
    onSubmit,
    showUpdateDefaultPaymentPhoneNumber,
    setShowUpdateDefaultPaymentPhoneNumber,
    updateDefaultPaymentPhoneNumber,
    verifyingPhoneNumber,
    savingDefaultPhoneNumber,
    invoice,
    currency,
    user,
  } = props

  const [form] = Form.useForm()
  
  return (
    <div style={{width: "auto", height: "100%", display: "flex", flexDirection: "column"}}>
      <div style={{width: "100%", flexGrow: 3, padding: "10px 20px 10px 20px"}}>
        <Row style={{marginBottom: 10}}>
          <Col span={24} style={{textAlign: "center", textDecoration: "underline", fontWeight: "bold", fontSize: "16px"}}>
            <span>Add payment</span>
          </Col>
        </Row>
        <Row style={{marginBottom: 5}}>
          <Col span={24} style={{textAlign: "left"}}>
            <span style={{marginRight: 3}}>Balance: </span><span>{numberWithCommas(invoice?.balance || 0)} {currency}</span>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form
              form={form}
              onFinish={values => {
                onSubmit({...values, check_phone_number: true})
              }}
              initialValues={{
                phone_number: getPhoneNumberWithoutPrefix(user?.default_phone_number
                    || user?.phone_number || "", user?.country?.phone_number_prefix),
                amount: ""
              }}
              autoComplete="off"
            >
              <InputField
                // myStyle={{marginBottom: 3}}
                label={<FormattedMessage id="FILL_PAYMENT_AMOUNT"/>}
                name="amount"
                type="number"
                rule={[
                  {required: true, message: "Amount required"}
                ]}
              />
              <InputField 
                // myStyle={{marginBottom: 3}}
                label="Phone number"
                name="phone_number"
              />
              <MyButton
                text="Pay"
                htmlType="submit"
                disabled={verifyingPhoneNumber}
                loading={verifyingPhoneNumber}
              />
            </Form>
          </Col>
        </Row>
      </div>
      {showUpdateDefaultPaymentPhoneNumber && (
        <div style={{width: "100%", height: "100%", flexGrow: 1}}>
          <AddPhoneNumber
            phoneNumber={form.getFieldValue("phone_number")}
            onAccept={() => updateDefaultPaymentPhoneNumber(form.getFieldValue("phone_number"))}
            onCancel={() => {
              setShowUpdateDefaultPaymentPhoneNumber(false)
              const payload = {
                ...form.getFieldsValue(),
                check_phone_number: false
              }
              onSubmit(payload)
            }}
            savingDefaultPhoneNumber={savingDefaultPhoneNumber}
            setShowUpdateDefaultPaymentPhoneNumber={setShowUpdateDefaultPaymentPhoneNumber}
          />
        </div>
      )}
    </div>
  )
}

InvoicePaymentForm.propTypes = {
  onSubmit: PropTypes.func,
  initialValues: PropTypes.object,
  timeRemaining: PropTypes.string,
  currency: PropTypes.string,
  loading: PropTypes.bool,
  invoice: PropTypes.object,
  user: PropTypes.object,
  savingDefaultPhoneNumber: PropTypes.bool,
  showUpdateDefaultPaymentPhoneNumber: PropTypes.bool,
  updateDefaultPaymentPhoneNumber: PropTypes.func
}

export default InvoicePaymentForm