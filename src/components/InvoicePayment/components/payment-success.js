import React from 'react'
import PropTypes from "prop-types"
import HomeButton from "../../../containers/PrivateRoutes/CreditOverview/home-button"

const PaymentSuccess = (props) => {
  const {
    amount,
    currency,
  } = props
  return (
    <div style={{width: "100%", height: "400px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "flex-start"}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='100px' height="100px" src="/images/check-mark.png" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", 
        justifyContent: "flex-start", alignItems: "center", flexGrow: 1, paddingBottom: 10}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <h2 style={{textDecoration: "underline"}}>
            Payment successful
          </h2>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p><span style={{fontWeight: "bold"}}>{amount} {currency}</span> has been</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>deducted from your</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>outstanding balance</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end", flexGrow: 1}}>
          <HomeButton />
        </div>
      </div>
    </div>
  )
}

PaymentSuccess.propTypes = {
  timeRemaining: PropTypes.string,
  amount: PropTypes.string.isRequired,
  currency: PropTypes.string.isRequired
}

export default PaymentSuccess