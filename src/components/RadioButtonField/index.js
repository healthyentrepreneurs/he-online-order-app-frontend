import React from 'react';
import { Form } from 'antd';
import { Radio } from "@jbuschke/formik-antd";

const FormItem = Form.Item;

export const RadioButtonField = ({ label, field, form: { touched, errors }, ...props }) => {
  const errorMsg =  errors[field.name];

	return (
	  <FormItem
	    validateStatus={ errorMsg ? 'error': ''}
	    help={errorMsg}>
    <Radio.Group 
      onChange = {e => {
         props.setFieldValue(field.name, e.target.value)
      }}
      value={props.value}>
    	{ props.options.map((option, index) => (
          <Radio key={index} value={option.value} style={radioStyle}>
              {option.label}
          </Radio>		  			
	  		))
	  	}
      </Radio.Group>	   
	  </FormItem>
	);
}

const radioStyle = {
  display: 'block',
  height: '30px',
  lineHeight: '30px',
};
