import React, { useState } from 'react';
import { Form } from 'antd';
import { DatePicker } from "@jbuschke/formik-antd";
import { Formik, Field } from 'formik';
import moment from 'moment';

const FormItem = Form.Item;

export const DateField = ({ label, field, form: { touched, errors }, ...props }) => {
	const errorMsg =  errors[field.name];
	  
	return (
	  <FormItem
	    label={label}
	    validateStatus={ errorMsg ? 'error': ''}
	    help={errorMsg}>
	    <DatePicker
				{...props}
			  name={field.name}
			  disabledDate= {(current) => {
				  // disable future dates
					const today = moment();
					return current && current > moment(today, "YYYY-MM-DD");
			  }}
	      defaultValue={moment('2015-01-01', 'YYYY-MM-DD')}
	      format="YYYY-MM-DD"
	      showTime={false}
	      size={props.size}
	    />
	  </FormItem>
	);
}