import Icon from '@ant-design/icons/lib/components/Icon'
import React from 'react'
import PropTypes from "prop-types"

const CustomIcon = (props) => {
  return (
    <Icon component={() => (<img alt='edit_btn' width={20} height={20} src={props.imageUrl} />)} onClick={e => {
      if(props.onClick) {
        props.onClick()
      }
    }} />
  )
}

CustomIcon.propTypes = {
  onClick: PropTypes.func,
  imageUrl: PropTypes.string.isRequired
}

export default CustomIcon