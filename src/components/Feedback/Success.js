import React from 'react';

import '../../styles/change-pin.less';
import Button from '../Button';

const SuccessFeedback = (props) => {
    return (
        <div className="change-pin-container">
            <div className="info">
                <div className="content">
                    <img src={props.icon} className="icon" alt="icon" />
                    <p className="title">{props.title}</p>

                    <Button
                        text={props.btnText}
                        onClick={props.onClick}
                    />
                </div>
            </div>
        </div>
    )
}

export default SuccessFeedback;
