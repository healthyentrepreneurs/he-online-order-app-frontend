import React from "react";
import { Steps } from "antd";

const { Step } = Steps;

const steps = [
  {
    step: 0,
  },
  {
    step: 1,
  },
  {
    step: 2,
  },
  {
    step: 3,
  },
];

const Steppers = ({ current, ...props }) => {
  return (
    <>
      <Steps direction="horizontal" current={current} size="small">
        {steps.map((item) => (
          <Step
            key={item.step}
            onClick={() => {
              props.onPress(item.step);
            }}
          />
        ))}
      </Steps>
    </>
  );
};

export default Steppers;
