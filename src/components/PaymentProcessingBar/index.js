import React from 'react'

const PaymentProcessingBar = ({message, ...props}) => {
  return (
    <>
      <div className='payment-processing-bar-wrapper'>
        <div className='left-wrapper'>
          <div className='top-wrapper'>
            <span className='payment-processing-label'>PAYMENT PROCESSING ONGOING</span>
          </div>
          <div className='top-wrapper'>
            <span className='payment-processing-value'>{message}</span>
          </div>
        </div>
        <div className='right-wrapper'>
          <img src="/images/loader-40.svg" alt='loader' />
        </div>
      </div>
    </>
  )
}

export default PaymentProcessingBar;