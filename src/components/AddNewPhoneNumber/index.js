import { Row, Col, Button } from 'antd'
import React from 'react'
import PropTypes from "prop-types"

const AddNewPhoneNumber = (props) => {
  const {
    onAccept,
    onCancel,
    phoneNumber,
    addingNewPhoneNumber,
    showAddPhoneNumber
  } = props

  return (
    <>
      <Row>
        <Col span={24} className="add-new-phone-number-wrapper">
          <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span>The phone number <span>{phoneNumber}</span> is new</span>
            </Col>
          </Row>
          <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span>payment number.</span>
            </Col>
          </Row>
          <Row>
            <Col span={24} style={{textAlign: "center"}}>
              <span>You want to use this number for future</span>
            </Col>
          </Row>
          <Row>
            <Col span={24}  style={{textAlign: "center"}}>
              <span>orders?</span>
            </Col>
          </Row>
          <Row>
            <Col span={12} style={{textAlign: "center"}}>
              <Button loading={addingNewPhoneNumber} onClick={e => onAccept()} style={{width: 100}} type="default">Yes</Button>
            </Col>
            <Col span={12} style={{textAlign: "center"}}>
              <Button onClick={e => onCancel()} style={{width: 100}} type="default">No, thanks</Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  )
}

AddNewPhoneNumber.propTypes = {
  showAddPhoneNumber: PropTypes.bool,
  addingNewPhoneNumber: PropTypes.bool
}

export default AddNewPhoneNumber