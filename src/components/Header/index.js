import React from "react";
import { Image, Menu, Dropdown, Col, Row, Button, message } from "antd";
import { DownOutlined, SyncOutlined } from "@ant-design/icons";
import HELogo from "../../assets/he-full-logo.png";
import HESmallLogo from "../../assets/he-small-logo.png";
import ShoppingCart from "../../assets/shopping-cart-small.svg";
import { history } from "../../util/helpers/browserHistory";
import "../../styles/header.less";
import { getUserProfileFromStorage, numberWithCommas } from "../../util/helpers/reusableFunctions";
import { Creators as AuthCreators } from "../../services/redux/auth/actions"
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";


const getCurrency = () => {
  let user = getUserProfileFromStorage() || {};
  let currency = user && user.country && user.country.currency;
  return currency;
};

const PageHeader = (props) => {
  return props.authHeader ? (
    <AuthHeader {...props} />
  ) : (
    <DashboardHeader {...props} />
  );
};

const AuthHeader = (props) => {
  return (
    <div className="auth-header-wrapper">
      <Image
        width={120}
        height={35}
        style={{ marginBottom: 30 }}
        preview={false}
        src={HELogo}
        onClick={() => history.push("/dashboard")}
        className="auth-logo"
      />
    </div>
  );
};


// const items = [
//   {
//     label: "All Orders",
//     route: "/orders",
//   },
//   {
//     label: "Change Pin",
//     route: "/change-pin",
//   },
//   {
//     label: "Logout",
//     route: "/logout",
//   },
// ];

const items = [
  {
    key: "1",
    label: <FormattedMessage id="ALL_ORDERS" />,
    route: "/orders",
  },
  {
    key: "2",
    label: <FormattedMessage id="CHANGE_PIN" />,
    route: "/change-pin",
  },
  {
    key: "3",
    label: <FormattedMessage id="LOGOUT" />,
    route: "/logout",
  },
];

const Menus = () => (
  <Menu>
    {items.map((item) => (
      <Menu.Item
        key={item.key}
        className="menu-item-list"
        onClick={() => history.push(item.route)}
      >
        {item.label}
      </Menu.Item>
    ))}
  </Menu>
);

const DashboardHeader = (props) => {
  const [ reSetting, setReSetting ] = React.useState(false)

  React.useEffect(()=> {
    const userId = JSON.parse(localStorage.getItem("he-che-user"))?.id
    if(navigator.onLine) {
      props.getUserDetailsWithSalt(userId)
    }
    const checkOnline = window.addEventListener("online", async () => {
      props.getUserDetailsWithSalt(userId)
    })

    return () => {
      window.removeEventListener("online", checkOnline)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  React.useEffect(()=> {
    const userId = JSON.parse(localStorage.getItem("he-che-user"))?.id
    let periodicUpdate = setInterval(() => {
      if(navigator.onLine) {
        props.getUserDetailsWithSalt(userId)
    }
    }, 1800000);

    //}, 1800000);

    return () => {
      clearInterval(periodicUpdate)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  

  const clearCache = () => {
    const { getUserDetails } = props
    const authUser = getUserProfileFromStorage()
    setReSetting(true)
    setTimeout(async ()=> {
      if(navigator.onLine) {
        if('caches' in window) {
          await caches.delete('endpoints-cache')
          await caches.delete('images-cache')
          caches.keys().then(names => {
            names.forEach(name => {
              caches.delete(name)
            })
          })
          const userId = authUser?.id
          getUserDetails(userId)
          setReSetting(false)
          window.location.href = "/dashboard"
          //window.location.reload(true)
        } else {
          setReSetting(false)
          message.error('Browser does not support cache storage!')
        }
      } else {
        setReSetting(false)
        message.error("Failed! Browser offline")
      }
    }, 500)
  }

  React.useEffect(()=> {
    if(getUserProfileFromStorage()) {
      props.getUserDetails(getUserProfileFromStorage()?.id)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <div className="dashboard-header-wrapper">
      <Row className="main-header">
        <Col span={4}>
          <Image
            src={HESmallLogo}
            width={45}
            height={35}
            className="logo"
            onClick={() => history.push("/dashboard")}
            preview={false}
            style={{ marginBottom: 15 }}
          />
        </Col>
        <Col span={3} style={{display: 'flex', alignItems: 'center', flexDirection: 'row', justifyContent: 'center'}}>
          <SyncOutlined spin={reSetting} className="sync-button" onClick={()=> {clearCache()}}/>
        </Col>
        <Col span={7} style={{ marginTop: "-10px" }}>
          <Dropdown overlay={Menus} trigger={["click"]}>
            <Button
              type="text"
              className="drop-down-title"
              onClick={(e) => e.preventDefault()}
            >
              <FormattedMessage id="ACCOUNT" />
              <DownOutlined />
            </Button>
          </Dropdown>
        </Col>
        <Col span={10} style={{ background: "#F5FAF6" }}>
          <div className="shopping-cart">
            <img src={ShoppingCart} alt="shopping logo" />
            <span className="cart-total-price" onClick={() => history.push("/cart/details/default/")}>
              {getCurrency()}{" "}
              {props.total && numberWithCommas(Math.ceil(props.total))}
            </span>
          </div>
        </Col>
      </Row>
      {props.category}
      <div className="info-wrapper">
        {props.title && <p className="info-text">{props.title}</p>}
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
      isLoading: state.auth.isFetching,
      user: state.auth.user,
  }
}

const mapDispatchToProps = dispatch => {
  return {
      getUserDetails: (id) => {
          dispatch(AuthCreators.getUserDetails(id))
      },
      getUserDetailsWithSalt: (id) => {
        dispatch(AuthCreators.getUserDetailsWithSalt(id))
      }
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (PageHeader);
