import React from "react";
import { Card } from "antd";
import Modal from "react-modal";

import Steppers from "../../components/Steppers";
import "../../../styles/cart.less";
import Button from "../../components/Button";
import CancelIcon from "../../assets/cancel.svg";
import SuccessIcon from "../../assets/success.svg";
import ErrorIcon from "../../assets/error.svg";
import { useIntl } from "react-intl";

const CashPaymentModal = (props) => {
  const intl = useIntl()

  const {
    visible,
    closeModal,
    title,
    text,
    isSuccess = true,
    showReturnHomeButton = true,
    buttonText = intl.formatMessage({ id: "RETURN_TO_HOME" }),
    showSteppers = true,
  } = props

  const customStyles = {
    content: {
      transform: "translate(-50%, -50%)",
      position: "absolute",
      top: "54%",
      left: "50%",
      padding: "0px",
      width: "340px",
      height: "fit-content",
      zIndex: 50000,
    },
  };
  return (
    <Modal isOpen={visible} onRequestClose={() => closeModal()} style={customStyles}>
      <Card className="generic-modal">
        <img
          src={CancelIcon}
          alt="Cancel"
          onClick={() => closeModal()}
          className="close-icon"
        />
        {showSteppers && (
          <div className="stepper-wrapper">
            <Steppers current={4} />
          </div>
        )}
        
        <div className="text-content">
          {isSuccess ? (
            <img src={SuccessIcon} alt="success" className="status-logo" />
          ) : (
            <img src={ErrorIcon} alt="error" className="status-logo" />
          )}
          <h4 className="title">{title}</h4>
          <p className="text">{text}</p>
          <Button
            className="action-btn"
            text={buttonText ? buttonText : intl.formatMessage({ id: "TRY_AGAIN" })}
            onClick={(e) => {
              e.preventDefault();
              props.executeAction();
            }}
          />
        </div>
      </Card>
    </Modal>
  );
};

export default CashPaymentModal;
