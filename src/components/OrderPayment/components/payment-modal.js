import { Modal } from 'antd';
import React from 'react'

const PaymentModal = ({
  visible,
  closeModal,
  makePayment,
  order_id,
  total,
  loading,
}) => {

  const user = JSON.parse(localStorage.getItem("he-che-user"));
  const [paymentValue, setPaymentValue] = useState(null);
  const [paymentChoiceError, setPaymentChoiceError] = useState("");
  const onPaymentChange = (e) => {
    let selectedOption = e.target.value;
    setPaymentValue(selectedOption);
  };
  // const customStyles = {
  //   content: {
  //     transform: "translate(-50%, -50%)",
  //     position: "absolute",
  //     top: "54%",
  //     left: "50%",
  //     padding: "0px",
  //     width: "340px",
  //     height: "fit-content",
  //     zIndex: 50000,
  //   },
  // };
  return (
    <Modal 
      visible={visible} 
      onCancel={() => {
        closeModal()
      }}
    >
        <div className="stepper-wrapper">
          <Steppers current={2} />
        </div>
        <div>
          <h4 className="title">Pay with Mobile Money</h4>
          <p className="select-text">Select Payment Method</p>
          <Radio.Group onChange={onPaymentChange} value={paymentValue}>
            {getPaymentMethods().includes("momo") ? (
              <Radio className="payment-modal-radio" value={"momo"}>
                MTN
              </Radio>
            ) : null}
            {getPaymentMethods().includes("airtel") ? (
              <Radio className="payment-modal-radio" value={"airtel"}>
                Airtel
              </Radio>
            ) : null}
            {getPaymentMethods().includes("mpesa") ? (
              <Radio className="payment-modal-radio" value={"mpesa"}>
                Mpesa
              </Radio>
            ) : null}
            {getPaymentMethods().includes("ihela") ? (
              <Radio className="payment-modal-radio" value={"ihela"}>
                EcoCash
              </Radio>
            ) : null}
          </Radio.Group>
        </div>
        <Form
          initialValues={{
            phone_number: formatPhoneNumber(user.phone_number, user?.country?.phone_number_prefix),
          }}
          onFinish={(values) => {
            if (paymentValue) {
              setPaymentChoiceError(null);
              if (values?.phone_number.includes("@")){
              
                const payload = {
                  email: values?.phone_number,
                  order_id,
                  amount: total,
                  payment_method: paymentValue,
                  partial: false
                }
                makePayment(payload);

               } else {
                const phoneNumber = addPrefixToPhoneNumber(values?.phone_number, user?.country?.phone_number_prefix, paymentValue === 'ihela')
              
                const payload = {
                  phone_number: phoneNumber,
                  order_id,
                  amount: total,
                  payment_method: paymentValue,
                }
                // console.log('payment type: ', paymentValue)
                // console.log('pay paload: ', payload)
                makePayment(payload);
               }
            } else {
              setPaymentChoiceError("Please select a mobile money carrier");
            }
          }}
        >
          <InputField
            label="Phone Number"
            placeholder="Enter the phone number"
            inputmode="numeric"
            name="phone_number"
          />
          {paymentChoiceError && (
            <p style={{ color: "red" }}>{paymentChoiceError}</p>
          )}
          <Form.Item>
            <Button text="Pay Now" htmlType="submit" loading={loading} />
          </Form.Item>
        </Form>
    </Modal>
  );
};