import React from "react";
import { Layout } from "antd";
import { connect } from "react-redux";

import PageHeader from "../Header";
import Footer from "../Footer";
import "../../styles/layout.less";

const { Content } = Layout;

const AppLayout = (props) => {
	return (
		<div className="app-layout">
			<PageHeader
				authHeader={props.authHeader}
				title={props.title}
				currency={props.currency}
				total={props.total}
				category={props.category}
				message={props.message}
				showNotificationBar={props.showNotificationBar}
			/>
			<Content>{props.children}</Content>
			{!props.noFooter && <Footer />}
		</div>
	);
};

const mapStateToProps = (state) => {
	const { currency, total } = state.cart;
	return {
		currency,
		total,
	};
};

export default connect(mapStateToProps, null)(AppLayout);
