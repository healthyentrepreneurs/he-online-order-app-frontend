import React from 'react'
import PropTypes from "prop-types"

const ProcessingPayment = (props) => {
  const {
    timeRemaining="196",
  } = props
  return (
    <div style={{width: "100%", height: "400px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "flex-start", padding: 20}}>
      <div style={{width: "100%", display: "flex", justifyContent: "center", alignItems: "center",flexGrow: 3}}>
        <img width='70px' height="70px" src="/images/loader-40.svg" alt='loading' />
      </div>
      <div style={{width: "100%", display: "flex", flexDirection: "column", justifyContent: "flex-start", alignItems: "center", flexGrow: 1}}>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <span style={{fontWeight: "bold", textDecoration: "underline", fontSize: "16px"}}>
            Processing Payment
          </span>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>Please wait</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>Your payment is being</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>processed.</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "center"}}>
          <p>It might take a few minutes.</p>
        </div>
        <div style={{display: "flex", flexDirection:"row", justifyContent: "row", alignItems: "flex-end", flexGrow: 1}}>
          <p style={{fontWeight: "bold", fontSize: "16px"}}>{timeRemaining}</p>
        </div>
      </div>
    </div>
  )
}

ProcessingPayment.propTypes = {
  timeRemaining: PropTypes.string
}

export default ProcessingPayment