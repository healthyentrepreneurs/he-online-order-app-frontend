import React from 'react'
import PropTypes from "prop-types"
import { Button, Row, Col } from 'antd'
import { history } from '../../util/helpers/browserHistory'
import { FormattedMessage } from 'react-intl'

const HomeButton = (props) => {
  const {
    onClick
  } = props
  return (
    <>
      <Row>
        <Col span={24}>
          <Button
            size='large'
            style={{border: "solid 1px #82817e"}}
            block
            onClick={e => {
              if(onClick) {
                onClick()
              } else {
                history.push("/dashboard")
              }
            }}
          >
            <FormattedMessage id="BACK_TO_HOME_SCREEN" />
          </Button>
        </Col>
      </Row>
    </>
  )
}

HomeButton.propTypes = {
  onClick: PropTypes.func
}

export default HomeButton;