import React from 'react';
import { Button as AntButton } from 'antd';
import PropTypes from "prop-types"

//import '../../styles/buttons.less';

const Button = props => {
    return (
        <AntButton
            htmlType={props.htmlType}
            className="he-button"
            onClick={props.onClick}
            loading={props.loading}
            {...props}
        >
            <span className="btn-text">{props.text}</span>
        </AntButton>
    )
}

Button.propTypes = {
    onClick: PropTypes.func,
    loading: PropTypes.bool,
    text: PropTypes.any.isRequired,
    htmlType: PropTypes.string
}

export default Button;
