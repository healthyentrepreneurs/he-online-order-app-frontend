import { createActions } from "reduxsauce";

export const { Types, Creators } = createActions(
	{
		getPhoneNumberDetails: ["phoneNumber"],
		getPhoneNumberDetailsSuccess: ["responseData"],
		getPhoneNumberDetailsFailure: ["error"],

		resetPhoneNumberDetails: null
	},
	{}
);
