import { apiRequest } from "../apiRequest";
import { ORDERS_ENDPOINT } from "../urls";

export const sendOrder = (data) => {
	return apiRequest("POST", `${ORDERS_ENDPOINT}`, data);
};
