importScripts(
	"https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js"
);
//importScripts('swenv.js');
//console.log("process.env is ", process.env && process.env.REACT_APP_BASE_URL);

if (workbox) {
	console.log(`Yay! Workbox is loaded`);
	try {
		workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);

		workbox.routing.registerRoute(
			// /https:\/\/he-staging\.herokuapp\.com\/api/, //handle url more dynamically.
			({url}) => url.pathname.startsWith('/api/') && url.href.indexOf('/profile/?salt') === -1,
			new workbox.strategies.CacheFirst({
				cacheName: "endpoints-cache",
				plugins: [
					new workbox.expiration.ExpirationPlugin({
						maxEntries: 50,
						maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
					}),
				],
			})
		);
		

		//cache images
		workbox.routing.registerRoute(
			//({url}) => url.startsWith('https://he-products-images.s3-us-west-2.amazonaws.com'),
			// new RegExp('^https://he-products-images.s3-us-west-2.amazonaws.com'),
			/.*\.(?:png|jpg|jpeg|svg|gif)/,
			new workbox.strategies.StaleWhileRevalidate({
				cacheName: 'images-cache',
				plugins: [
					new workbox.expiration.ExpirationPlugin({
						maxEntries: 400,
						maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
						purgeOnQuotaError: true // Automatically cleanup if quota is exceeded
					})
				]
			})
		)

		// Cache the Google Fonts stylesheets with a stale-while-revalidate strategy.
		workbox.routing.registerRoute(
			({ url }) => url.origin === "https://fonts.googleapis.com",
			new workbox.strategies.StaleWhileRevalidate({
				cacheName: "google-fonts-stylesheets",
			})
		);

		// Cache the underlying font files with a cache-first strategy for 1 year.
		workbox.routing.registerRoute(
			({ url }) => url.origin === "https://fonts.gstatic.com",
			new workbox.strategies.CacheFirst({
				cacheName: "google-fonts-webfonts",
				plugins: [
					new workbox.cacheableResponse.CacheableResponsePlugin({
						statuses: [0, 200],
					}),
					new workbox.expiration.ExpirationPlugin({
						maxAgeSeconds: 60 * 60 * 24 * 365,
						maxEntries: 30,
					}),
				],
			})
		);

		// for SPA navigation routes to work offline on page refresh...
		const handler = workbox.precaching.createHandlerBoundToURL("/index.html");
		const navigationRoute = new workbox.routing.NavigationRoute(handler, {
			// allowlist: [new RegExp("/blog/")],
			// denylist: [new RegExp("/blog/restricted/")],
		});
		workbox.routing.registerRoute(navigationRoute);
	} catch (err) {
		console.log("An error occurred on workbox ", err);
	}
} else {
	console.log(`Boo! Workbox didn't load`);
}

self.addEventListener("activate", event => {
	clients.claim();
})

self.addEventListener("message", (event) => {
	if (event.data && event.data === "SKIP_WAITING") {
		console.log('SKIP_WAITING MESSAGE EVENT TRIGGERED!')
		self.skipWaiting();
	}
});
