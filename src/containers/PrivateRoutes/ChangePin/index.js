import { connect } from 'react-redux';

import { Creators } from '../../../services/redux/auth/actions';
import ChangePin from './components/';

const mapStateToProps = state => {
  return {
    changingPin: state.auth.isChangingPin,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changePin: (old_pin, new_pin) => {
      dispatch(Creators.changePinRequest(old_pin, new_pin));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePin);
