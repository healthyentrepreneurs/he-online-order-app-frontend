import React from 'react';

import SuccessFeedback from '../../../../components/Feedback/Success';
import CheckMarkIcon from '../../../../assets/check-mark.svg';
import AppLayout from '../../../../components/Layout/Layout';
import { history } from '../../../../util/helpers/browserHistory';


const PinSucessFully = props => {
    return (
        <AppLayout title="Change PIN">
            <SuccessFeedback
                icon={CheckMarkIcon}
                title="Your PIN has been changed succesfully"
                btnText="Go to Product Page"
                onClick={() => history.push('/dashboard')}
            />
        </AppLayout>
    )
}

export default PinSucessFully;