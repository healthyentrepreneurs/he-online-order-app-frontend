import React from "react";
import { Form, Button as AntButton } from "antd";
import { ArrowLeftOutlined } from "@ant-design/icons";

import Button from "../../../../components/Button";
import { InputField } from "../../../../components/InputField";
import { history } from "../../../../util/helpers/browserHistory";
import AppLayout from "../../../../components/Layout/Layout";

import "../../../../styles/change-pin.less";
import { FormattedMessage, useIntl } from "react-intl";

const ChangePin = (props) => {
  const intl = useIntl()
  const onFinish = (values) => {
    props.changePin(values.old_pin, values.new_pin);
  };

  const onFinishFailed = (error) => {
  };

  const comparePins = ({ getFieldValue }) => ({
    validator(rule, value) {
      if (!value || getFieldValue("new_pin") === value) {
        return Promise.resolve();
      }
      return Promise.reject("The two pins that you entered do not match!");
    },
  });
  return (
    <AppLayout title="Change Pin">
      <div className="change-pin-container">
        <AntButton type="text" className="go-back" onClick={() => history.goBack()}>
          <ArrowLeftOutlined style={{ fontWeight: "bolder", fontSize: "16px" }} />
          <span className="text">
            <FormattedMessage id="GO_BACK" />
          </span>
        </AntButton>
        <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <InputField
            label={<FormattedMessage id="OLD_PIN" />}
            placeholder={ intl.formatMessage({ id: "ENTER_YOUR_OLD_PIN" }) }
            name="old_pin"
            inputmode="numeric"
            pattern="[0-9]*"
            rules={{
              max: 4,
              message: "Pin cannot be more than 4 characters",
            }}
          />
          <InputField
            label={ intl.formatMessage({ id: "ENTER_YOUR_NEW_PIN" }) }
            placeholder={ intl.formatMessage({ id: "ENTER_YOUR_NEW_PIN" }) }
            name="new_pin"
            inputmode="numeric"
            pattern="[0-9]*"
            rules={{
              max: 4,
              message: "Pin cannot be more than 4 characters",
            }}
          />
          <InputField
            label={ intl.formatMessage({ id: "CONFIRM_YOUR_NEW_PIN" }) }
            placeholder={ intl.formatMessage({ id: "CONFIRM_YOUR_NEW_PIN" }) }
            name="confirm_pin"
            inputmode="numeric"
            pattern="[0-9]*"
            rules={comparePins}
          />
          <Button text={ intl.formatMessage({ id: "CONFIRM_YOUR_NEW_PIN" }) } htmlType="submit" loading={props.changingPin} />
        </Form>
      </div>
    </AppLayout>
  );
};

export default ChangePin;
