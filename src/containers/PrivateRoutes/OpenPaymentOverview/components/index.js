import { SyncOutlined } from '@ant-design/icons'
import { Row, Col, message, Button } from 'antd'
import { debounce } from 'lodash'
import moment from 'moment'
import React, { useState, useEffect } from 'react'
import AppLayout from '../../../../components/Layout/Layout'
import GenericMomoPayment from '../../../../reusable-components/GenericMomoPayment'
import { appStyleConfig, rollbar } from '../../../../util/configs'
import { history } from '../../../../util/helpers/browserHistory'
import { getUserProfileFromStorage, hasMomoPaymentConfig, numberWithCommas } from '../../../../util/helpers/reusableFunctions'
import useQuery from '../../../../util/helpers/useQuery'
import OpenPaymentList from './open-payment-list'
import PaymentStatusModal from '../../../../reusable-components/PaymentStatusModal';
import { FormattedMessage } from 'react-intl'



const OpenPaymentOverview = (props) => {
  const {
    fetchOpenPayments,
    payments, 
    loading,
    userDetailsLoading,
    resetOpenPayments,
    resetPayment,
    completePayment,
    getUserDetailsWithSalt,
    setTab,
    user
  } = props

  //################### Local states ##########################
  const [selectedInvoice, setSelectedInvoice] = useState(null);
  const [paymentFormVisible, setPaymentFormVisible] = useState(false)
  const [isOnline, setIsOnline] = useState(navigator.onLine);
  const [paymentStatusModal, setPaymentStatusModal] = useState({
    isVisible: false,
    title: null,
    text: null,
    showReturnHomeButton: true,
    isSuccess: true,
    callBack: () => {},
  });

  const query = useQuery()
  

  
  //##################### Events ##########################
  const handleScroll = debounce(() => {
    const { error, loading, hasMore, nextPage } = props;
    if (error || loading || !hasMore) {
      return;
    } else  {
      const node = document.documentElement;
      if(node.scrollHeight - node.scrollTop - 140 < node.clientHeight) {
        const next = (nextPage.split("?") || [])[1]
        fetchOpenPayments(next)
    }
    }
  }, 100)

  const handleSetOnline = () => {
    setIsOnline(true)
  }

  const handleSetOffline = () => {
    setIsOnline(false)
  }


  //############################## Helper Functions ############################
  
  const clearCacheAndReload = async () => {
    const authUser = getUserProfileFromStorage()
    rollbar.info(`Sync credit balance manually: ${authUser}`)
    await clearCache()
    window.location.reload(true)
  }

  const clearCache = async() => {
    if(navigator.onLine) {
      if('caches' in window) {
        await caches.delete('endpoints-cache')
        const userId = JSON.parse(localStorage.getItem("he-che-user"))?.id
        getUserDetailsWithSalt(userId)
      } else {
        message.error('Browser does not support cache storage!')
      }
    } else {
      message.error("Failed! Browser offline")
    }
  }

  //########################## Effects ###############################
  useEffect(() => {
    const userId = JSON.parse(localStorage.getItem("he-che-user"))?.id
    resetPayment()
    resetOpenPayments()
    fetchOpenPayments("page=1&limit=10")
    getUserDetailsWithSalt(userId)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    window.addEventListener("scroll", handleScroll)
    window.addEventListener("online", handleSetOnline)
    window.addEventListener("offline", handleSetOffline)
    return () => {
      window.removeEventListener("scroll", handleScroll)
      window.removeEventListener("online", handleSetOnline)
      window.removeEventListener("offline", handleSetOffline)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  })

  useEffect(() => {
    const status = query.get("status") || null
    const invoiceId = query.get("invoice_id") || null
    const msg = query.get("msg")
    const messageAmount = query.get("amount")
    const userId = JSON.parse(localStorage.getItem("he-che-user"))?.id
    if(status && invoiceId) {
      const isSuccess = status !== "failure";
      completePayment()
      if(isSuccess) {
        getUserDetailsWithSalt(userId)
      }
      setPaymentStatusModal({
        ...paymentStatusModal,
        isSuccess: isSuccess,
        isVisible: true,
        buttonText: isSuccess ? "Back to Home" : "Try Again",
        amountPaid: `${numberWithCommas(messageAmount)} ${user?.country?.currency}`,
        paymentType: "invoice",
        title: isSuccess ? "Payment successful" : "Payment Unsuccessful",
        text: isSuccess ?
          `${numberWithCommas(messageAmount)} ${user?.country?.currency} has been deducted from your outstanding balance`
          : `${msg}`
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <AppLayout
        title={<FormattedMessage id="OPEN_PAYMENTS_LOWER_CASE" />}
      >
        <Row style={{padding: "0px 10px 0px 10px", marginTop: "-15px"}}>
          <Col span={24} style={{position: "relative"}}>
            <Row style={{ border: '#f5faf5 3px solid', marginBottom: "10px" }}>
              <Col span={12}
                onClick={() => {
                  setTab(1)
                  history.push('/credit-invoices')
                }}
                style={{ backgroundColor: appStyleConfig.he_white, display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10 }}
              >
                <FormattedMessage id="CREDIT_OVERVIEW" />
              </Col>
              <Col span={12}
                // onClick={() => history.push('/open-payments')}
                style={{ backgroundColor: appStyleConfig.he_green, color: appStyleConfig.he_white, display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10 }}
              >
                <FormattedMessage id="OPEN_PAYMENTS_LOWER_CASE" />
              </Col>
            </Row>
            <Row style={{marginBottom: 30}}>
              <Col span={15}>
                <Button
                  disabled={!isOnline}
                  size="large"
                  style={{background: "#dfeff0", borderRadius: "10px", maxWidth: '100%'}}
                  onClick={e => {
                    //const userDetails = getUserProfileFromStorage()
                    clearCacheAndReload()
                  }}
                >
                  <SyncOutlined spin={userDetailsLoading} />
                  <span><FormattedMessage id="UPDATE_BALANCE_UPPER_CASE"/></span>
                </Button>
              </Col>
              <Col span={8} offset={1}>
                <Row>
                  <Col span={24}><FormattedMessage id="UPDATED_LOWER_CASE"/></Col>
                </Row>
                <Row>
                  <Col span={24}>{props.lastUpdated && moment(props.lastUpdated).format("DD-MM-YY HH:mm")}</Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <OpenPaymentList 
                  payments={payments}
                  loading={loading}
                  setSelectedInvoice={(invoice) => {
                    setSelectedInvoice(invoice)
                  }}
                  setPaymentFormVisible={setPaymentFormVisible}
                  hasMomoPaymentConfig={hasMomoPaymentConfig()}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <GenericMomoPayment
          disableAmount={true}
          visible={paymentFormVisible}
          closeModal={()=> {
            window.history.replaceState({}, document.title, "/credit-invoices")
            setPaymentFormVisible(false)
          }}
          paymentType="invoice"
          invoice={selectedInvoice}
          {...props}
        />
        <PaymentStatusModal
          buttonText={paymentStatusModal.buttonText}
          showSteppers={false}
          visible={paymentStatusModal.isVisible}
          title={paymentStatusModal.title}
          isSuccess={paymentStatusModal.isSuccess}
          text={paymentStatusModal.text}
          paymentType={paymentStatusModal.paymentType}
          amountPaid={paymentStatusModal.amountPaid}
          closeModal={() => {
            window.history.replaceState({}, document.title, "/credit-invoices")
            setPaymentStatusModal({...paymentStatusModal, isVisible: false})
            if(paymentStatusModal.isSuccess) {
              window.scrollTo(0, 0);
              clearCache()
              resetOpenPayments()
              fetchOpenPayments("page=1&limit=40")
            } 
          }}
          executeAction={() => {
            if(paymentStatusModal.isSuccess) {
              completePayment()
              clearCache()
              history.push("/dashboard")
            } else {
              window.history.replaceState({}, document.title, "/credit-invoices")
              setPaymentStatusModal({...paymentStatusModal, isVisible: false})
              completePayment()
            }
          }}
        />
      </AppLayout>
    </>
  )
}

export default OpenPaymentOverview