import { RightOutlined } from '@ant-design/icons'
import { Divider, Row, Col, Button } from 'antd'
import React from 'react'
import { numberWithCommas } from '../../../../util/helpers/reusableFunctions'
import PropTypes from "prop-types"
import moment from 'moment'
import { FormattedMessage } from 'react-intl'
// import Button from "../../../../../../components/Button"

const PaymentItem = (props) => {
  const {
    payment,
    currency,
    showPaymentButton,
    onMakePayment,
    index
  } = props

  const {amount_total_signed, balance, created_at, due_date, paid, status, order, sales_person} = payment
  return (
    <div className='invoice-item-wrapper'>
      <Row>
        <Col span={24}>
          <Row>
            <Col span={2}>

            </Col>
            <Col span={7}>
              <span className='invoice-item-top-label'><FormattedMessage id="TOTAL_UPPER_CASE"/></span>
            </Col>
            <Col span={7}>
              <span><FormattedMessage id="PAID"/></span>
            </Col>
            <Col span={8}>
              <span><FormattedMessage id="BALANCE_UPPER_CASE" /></span>
            </Col>
          </Row>
          <Divider style={{marginTop: 1, marginBottom: 1, borderTop: "1px solid #999894"}} />
          <Row className='invoice-item-row'>
            <Col span={2}>
              <span className=''>{index}</span>
            </Col>
            <Col span={7}>
              <span className='invoice-item-top-amount'>{numberWithCommas(amount_total_signed)} {currency}</span>
            </Col>
            <Col span={7}>
              <span className='invoice-item-top-amount'>{numberWithCommas(paid)} {currency}</span>
            </Col>
            <Col span={8}>
              <span className='invoice-item-top-amount'>{numberWithCommas(balance)} {currency}</span>
            </Col>
          </Row>
          <Row className='invoice-item-row'>
            <Col span={2}>

            </Col>
            <Col span={7}>
              <span className='invoice-item-top-amount'><FormattedMessage id="SALES_PERSON"/>:</span>
            </Col>
            <Col span={15}>
              <span className='invoice-item-top-amount'>{sales_person || 'N/A'}</span>
            </Col>
          </Row>
          <Row className='invoice-item-row'>
            <Col span={2}>

            </Col>
            <Col span={7}>
              <span className='invoice-item-top-amount'><FormattedMessage id="ORDER_NUMBER" />:</span>
            </Col>
            <Col span={15}>
              <span className='invoice-item-top-amount'>{order}</span>
            </Col>
          </Row>
          <Row className='invoice-item-row'>
            <Col span={2}>

            </Col>
            <Col span={7}>
              <span className='invoice-item-top-amount'><FormattedMessage id="INVOICE_DATE"/>:</span>
            </Col>
            <Col span={15}>
              <span className='invoice-item-top-amount'>{moment(created_at).format("DD-MM-YYYY")}</span>
            </Col>
          </Row>
          <Row className='invoice-item-row'>
            <Col span={2}></Col>
            <Col span={7}>
              <span className='invoice-item-top-amount'><FormattedMessage id="DUE_DATE"/>:</span>
            </Col>
            <Col span={15}>
              <span className='invoice-item-top-amount'>{moment(due_date).format("DD-MM-YYYY")}</span>
            </Col>
          </Row>
          <Row  className='invoice-item-row'>
            <Col span={2}></Col>
            <Col span={11}>
              {status === "posted" && (
                <Button
                  style={{border: "#30994B solid 1px", background: "#fff", borderWidth: "1px"}}
                >
                  <img src="/images/time.png" width="16px" height="16px" alt='check-logo' />
                  <span style={{fontWeight: "bolder", marginLeft: 5}}><FormattedMessage id="OPEN" /></span>
                </Button>
              )}
              {status === "paid" && (
                <Button
                  style={{border: "#30994B solid 1px", background: "#fff", borderWidth: "1px"}}
                >
                  <img src="/images/check.png" width="16px" height="16px" alt='check-logo' />
                  <span style={{fontWeight: "bolder", marginLeft: 5}}><FormattedMessage id="PAID"/></span>
                </Button>
              )}
            </Col>
            <Col span={11}>
              {(status === "posted" && showPaymentButton) ? (
                <Button
                  style={{background: "#30994B", color: "#fff"}}
                  block
                  onClick={onMakePayment}
                >
                  <span style={{fontWeight: "bold"}}><FormattedMessage id="I_WANT_TO_PAY" /></span>
                  <span style={{marginLeft: 8}}>
                    <RightOutlined style={{margin: 0}} />
                    <RightOutlined style={{margin: 0}}/>
                  </span>
                </Button>
              ) : null}
            </Col>
          </Row>
        </Col>
      </Row>
      
    </div>
  )
}

PaymentItem.propTypes = {
  payment: PropTypes.object.isRequired,
  showPaymentButton: PropTypes.bool.isRequired
}

export default PaymentItem