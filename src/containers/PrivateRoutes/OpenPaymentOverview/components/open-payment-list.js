import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Spin, Empty } from 'antd'
import PaymentItem from './payment-item'
import HomeButton from '../../../../components/HomeButton'
import { FormattedMessage } from 'react-intl'

const OpenPaymentList = (props) => {
  const {
    payments,
    user,
    loading,
    setSelectedInvoice,
    setPaymentFormVisible,
    hasMomoPaymentConfig
  } = props
  return (
    <>
      <Row style={{padding: 10}}>
        <Col span={24} style={{position: "relative"}}>
          {payments && payments.map((payment, index) => (
            <PaymentItem
              index={index + 1}
              key={`${payment?.created_at}${payment?.display_name}${payment?.amount_total_signed}`}
              payment={payment}
              showPaymentButton={hasMomoPaymentConfig}
              currency={user?.country?.currency}
              onMakePayment={(e) => {
                setSelectedInvoice(payment)
                setPaymentFormVisible(true)
              }}
            />
          ))}
          <Row style={{padding: 10}}>
            <Col span={24}></Col>
          </Row>
          {loading ? (
            <div className="load-more">
              <Spin size="large" tip="Loading..." />
            </div>
          ) : (
            <Row>
              {!payments.length ? (
                <div className="center-container">
                  <Empty description={<FormattedMessage id="NO_DATA" />} />
                </div>
              ) : null}
            </Row>
          )}
          <div style={{width: "100%", position: "absolute", bottom: "5px", marginTop: 10}}>
            <HomeButton />
          </div>
        </Col>
      </Row>
    </>
  )
}

OpenPaymentList.propTypes = {
  payments: PropTypes.array.isRequired,
  user: PropTypes.object,
  loading: PropTypes.bool,
  setSelectedInvoice: PropTypes.func,
  setPaymentFormVisible: PropTypes.func,
  hasMomoPaymentConfig: PropTypes.bool
}

export default OpenPaymentList