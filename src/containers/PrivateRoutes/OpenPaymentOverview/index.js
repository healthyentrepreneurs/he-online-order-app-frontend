
import { connect } from 'react-redux'
import { Creators as AccountingCreators } from "../../../services/redux/accounting/actions"
import {Creators as AuthCreators } from "../../../services/redux/auth/actions"
import { Creators as PaymentCreators } from "../../../services/redux/payments/actions"
import OpenPaymentsList from './components'

const mapStateToProps = state => ({
  credit_balance: state.accounting.credit_balance,
  payments: state.accounting.openPayments,
  loading: state.accounting.loadingOpenPayments,
  error: state.accounting.openPaymentsError,
  hasMore: state.accounting.openPaymentsHasMore,
  nextPage: state.accounting.openPaymentsNextPage,
  totalCount: state.accounting.openPaymentsTotalCount,
  lastUpdated: state.auth.lastUpdated,
  user: state.auth.user,
  userDetailsLoading: state.auth.isFetching,
  payment: state.payment,
  isMakingPayment: state.payment.isMakingPayment,
  paymentProcessing: state.payment.paymentProcessing,
  paymentSuccess: state.payment.paymentSuccess,
  paymentTimeup: state.payment.paymentTimeup,
  paymentError: state.payment.error,
  paymentAmount: state.payment.amount,
  savingDefaultPhoneNumber: state.auth.savingDefaultPhoneNumber,
  saveDefaultNumberSuccess: state.auth.saveDefaultNumberSuccess,
  amount: state.payment.amount
})


const mapActionToProps = dispatch => ({
  fetchCreditBalance: () => dispatch(AccountingCreators.fetchCreditBalance()),
  fetchOpenPayments: (query) => dispatch(AccountingCreators.fetchOpenPayments(query)),
  getUserDetailsWithSalt: id => dispatch(AuthCreators.getUserDetailsWithSalt(id)),
  makePaymentRequest: (data) => dispatch(PaymentCreators.makePaymentRequest(data)),
  resetOpenPayments: () => dispatch(AccountingCreators.resetOpenPayments()),
  setPaymentProcessing: () => dispatch(PaymentCreators.setPaymentProcessing()),
  setPaymentTimeup: (status) => dispatch(PaymentCreators.setPaymentTimeup(status)),
  waitForPayment: () => dispatch(PaymentCreators.waitForPayment()),
  resetPayment: () => dispatch(PaymentCreators.resetPayment()),
  saveDefaultPhoneNumber: (userId, payload) => dispatch(AuthCreators.saveDefaultPhoneNumber(userId, payload)),
  getUserDetails: (id) => dispatch(AuthCreators.getUserDetails(id)),
  completePayment: () => dispatch(PaymentCreators.completePayment()),
  setTab: (tab) => dispatch(AccountingCreators.setTab(tab))
})


export default connect(mapStateToProps, mapActionToProps)(OpenPaymentsList);