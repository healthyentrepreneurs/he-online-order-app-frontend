import { Row, Col, Spin, Empty } from 'antd'
import React from 'react'
import HomeButton from '../../../../components/HomeButton'
import InvoiceItem from './invoice-item'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

const InvoiceList = (props) => {
  const {
    invoices,
    user,
    loading,
    setSelectedInvoice,
    setPaymentFormVisible,
    hasMomoPaymentConfig
  } = props

  return (
    <>
      <Row>
        <Col span={24}>
          {invoices && invoices.map((invoice, index) => (
            <InvoiceItem
              index={index + 1}
              key={`${invoice.created_at}${invoice?.display_name}${invoice?.amount_total_signed}`}
              invoice={invoice}
              showPaymentButton={hasMomoPaymentConfig}
              currency={user?.country?.currency}
              onMakePayment={(e) => {
                setSelectedInvoice(invoice)
                setPaymentFormVisible(true)
              }}
            />
          ))}
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Row style={{padding: 10}}>
            <Col span={24}></Col>
          </Row>
          {loading ? (
            <div className="load-more">
              <Spin size="large" tip={<><FormattedMessage id="LOADING" />...</>} />
            </div>
          ) : (
            <Row>
              {!invoices.length ? (
                <div className="center-container">
                  <Empty description={<FormattedMessage id="NO_DATA" />} />
                </div>
              ) : null}
            </Row>
          )}
          <div style={{width: "100%", position: "absolute", bottom: "5px", marginTop: 10}}>
            <HomeButton />
          </div>
        </Col>
      </Row>
    </>
  )
}

InvoiceList.propTypes = {
  invoices: PropTypes.array.isRequired,
  user: PropTypes.object,
  loading: PropTypes.bool,
  setSelectedInvoice: PropTypes.func,
  setPaymentFormVisible: PropTypes.func,
  hasMomoPaymentConfig: PropTypes.bool
}

export default InvoiceList