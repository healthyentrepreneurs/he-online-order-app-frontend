import { SyncOutlined } from '@ant-design/icons';
import { Row, Col, Button, Spin, Empty, message } from 'antd';
import { debounce } from 'lodash';
import moment from 'moment';
import React, { useEffect, useState } from 'react'
import GenericMomoPayment from '../../../../../reusable-components/GenericMomoPayment';
// import PaymentStatusModal from '../../../../../reusable-components/PaymentStatusModal';
import { getUserProfileFromStorage, hasMomoPaymentConfig, numberWithCommas } from '../../../../../util/helpers/reusableFunctions';
import useQuery from '../../../../../util/helpers/useQuery';
import InvoiceItem from '../../components/invoice-item';
// import { history } from '../../../../../util/helpers/browserHistory'
import { rollbar } from '../../../../../util/configs';
import HomeButton from '../../../../../components/HomeButton';
import { FormattedMessage } from 'react-intl';

const CreditInvoiceList = (props) => {

  //############### PROPS ####################
  const {
    fetchInvoices,
    invoices,
    userDetailsLoading,
    resetInvoices,
    resetPayment,
    completePayment,
    getUserDetailsWithSalt,
    user
  } = props

  //################### Local states ##########################
  const [selectedInvoice, setSelectedInvoice] = useState(null);
  const [paymentFormVisible, setPaymentFormVisible] = useState(false)
  const [isOnline, setIsOnline] = useState(navigator.onLine);
  const [paymentStatusModal, setPaymentStatusModal] = useState({
    isVisible: false,
    title: null,
    text: null,
    showReturnHomeButton: true,
    isSuccess: true,
    callBack: () => {},
  });

  const query = useQuery()
  

  
  //##################### Events ##########################
  const handleScroll = debounce(() => {
    const { error, loading, hasMore, nextPage } = props;
    if (error || loading || !hasMore) {
      return;
    } else  {
      const node = document.documentElement;
      if(node.scrollHeight - node.scrollTop - 140 < node.clientHeight) {
        const next = (nextPage.split("?") || [])[1]
        fetchInvoices(next)
    }
    }
  }, 100)

  const handleSetOnline = () => {
    setIsOnline(true)
  }

  const handleSetOffline = () => {
    setIsOnline(false)
  }


  //############################## Helper Functions ############################
  
  const clearCacheAndReload = async () => {
    const authUser = getUserProfileFromStorage()
    rollbar.info(`Sync credit balance manually: ${authUser}`)
    await clearCache()
    window.location.reload(true)
  }

  const clearCache = async() => {
    if(navigator.onLine) {
      if('caches' in window) {
        await caches.delete('endpoints-cache')
        const userId = JSON.parse(localStorage.getItem("he-che-user"))?.id
        getUserDetailsWithSalt(userId)
      } else {
        message.error('Browser does not support cache storage!')
      }
    } else {
      message.error("Failed! Browser offline")
    }
  }

  //########################## Effects ###############################
  useEffect(() => {
    const userId = JSON.parse(localStorage.getItem("he-che-user"))?.id
    resetPayment()
    resetInvoices()
    fetchInvoices("page=1&limit=10")
    getUserDetailsWithSalt(userId)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    window.addEventListener("scroll", handleScroll)
    window.addEventListener("online", handleSetOnline)
    window.addEventListener("offline", handleSetOffline)
    return () => {
      window.removeEventListener("scroll", handleScroll)
      window.removeEventListener("online", handleSetOnline)
      window.removeEventListener("offline", handleSetOffline)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  })

  useEffect(() => {
    const status = query.get("status") || null
    const invoiceId = query.get("invoice_id") || null
    const msg = query.get("msg")
    const messageAmount = query.get("amount")
    const userId = JSON.parse(localStorage.getItem("he-che-user"))?.id
    if(status && invoiceId) {
      const isSuccess = status !== "failure";
      completePayment()
      if(isSuccess) {
        getUserDetailsWithSalt(userId)
      }
      setPaymentStatusModal({
        ...paymentStatusModal,
        isSuccess: isSuccess,
        isVisible: true,
        buttonText: isSuccess ? "Back to Home" : "Try Again",
        amountPaid: `${numberWithCommas(messageAmount)} ${user?.country?.currency}`,
        paymentType: "invoice",
        title: isSuccess ? "Payment successful" : "Payment Unsuccessful",
        text: isSuccess ?
          `${numberWithCommas(messageAmount)} ${user?.country?.currency} has been deducted from your outstanding balance`
          : `${msg}`
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <Row style={{padding: 10}}>
        <Col span={24} style={{position: "relative"}}>
          <Row style={{marginBottom: 30}}>
            <Col span={14}>
              <Button
                disabled={!isOnline}
                size="large"
                style={{background: "#dfeff0", borderRadius: "10px"}}
                onClick={e => {
                  //const userDetails = getUserProfileFromStorage()
                  clearCacheAndReload()
                }}
              >
                <SyncOutlined spin={userDetailsLoading} />
                <span>UPDATE BALANCE</span>
              </Button>
            </Col>
            <Col span={9} offset={1}>
              <Row>
                <Col span={24}>Updated</Col>
              </Row>
              <Row>
                <Col span={24}>{props.lastUpdated && moment(props.lastUpdated).format("DD-MM-YY HH:mm")}</Col>
              </Row>
            </Col>
          </Row>
          {invoices && invoices.map((invoice, index) => (
            <InvoiceItem
              index={index + 1}
              key={`${invoice.created_at}${invoice?.display_name}${invoice?.amount_total_signed}`}
              invoice={invoice}
              showPaymentButton={hasMomoPaymentConfig()}
              currency={user?.country?.currency}
              onMakePayment={(e) => {
                setSelectedInvoice(invoice)
                setPaymentFormVisible(true)
              }}
            />
          ))}
          <Row style={{padding: 10}}>
            <Col span={24}></Col>
          </Row>
          {props.loading ? (
            <div className="load-more">
              <Spin size="large" tip="Loading..." />
            </div>
          ) : (
            <Row>
              {!invoices.length ? (
                <div className="center-container">
                  <Empty description={<FormattedMessage id="NO_DATA" />} />
                </div>
              ) : null}
            </Row>
          )}
          <div style={{width: "100%", position: "absolute", bottom: "5px", marginTop: 10}}>
            <HomeButton />
          </div>
        </Col>
      </Row>
      <GenericMomoPayment
        visible={paymentFormVisible}
        closeModal={()=> {
          window.history.replaceState({}, document.title, "/credit-invoices")
          setPaymentFormVisible(false)
        }}
        paymentType="invoice"
        invoice={selectedInvoice}
        {...props}
      />
      <PaymentStatusModal
        buttonText={paymentStatusModal.buttonText}
        showSteppers={false}
        visible={paymentStatusModal.isVisible}
        title={paymentStatusModal.title}
        isSuccess={paymentStatusModal.isSuccess}
        text={paymentStatusModal.text}
        paymentType={paymentStatusModal.paymentType}
        amountPaid={paymentStatusModal.amountPaid}
        closeModal={() => {
          window.history.replaceState({}, document.title, "/credit-invoices")
          setPaymentStatusModal({...paymentStatusModal, isVisible: false})
          if(paymentStatusModal.isSuccess) {
            window.scrollTo(0, 0);
            clearCache()
            resetInvoices()
            fetchInvoices("page=1&limit=40")
          } 
        }}
        executeAction={() => {
          if(paymentStatusModal.isSuccess) {
            completePayment()
            clearCache()
            history.push("/dashboard")
          } else {
            window.history.replaceState({}, document.title, "/credit-invoices")
            setPaymentStatusModal({...paymentStatusModal, isVisible: false})
            completePayment()
          }
        }}
      />
    </>
  )
}

export default CreditInvoiceList