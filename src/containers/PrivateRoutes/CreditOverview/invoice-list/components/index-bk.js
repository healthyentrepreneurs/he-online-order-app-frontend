import { Row, Col } from 'antd'
import React, { useState } from 'react'
import AppLayout from "../../../../../components/Layout/Layout";
import CreditInvoiceList from './credit-invoice-list';
import PaymentInvoiceList from './payment-invoice-list';


const CreditPaymaentPage = (props) => {
  const [ pageIndex, setPageIndex ] = useState(1)
  return (
    <>
      <AppLayout 
        title="Credit overview"
      >
        <Row style={{ border: '#f5faf5 3px solid' }}>
          <Col span={12} 
            onClick={() => setPageIndex(1)}
            style={{ backgroundColor: pageIndex ===1 ? '#f5faf5' : '#ffffff', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10 }}
          >
            Credit Overview
          </Col>
          <Col span={12}
            onClick={() => setPageIndex(2)}
            style={{ backgroundColor: pageIndex ===2 ? '#f5faf5' : '#ffffff', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10 }}
          >
            Open Payments
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            {pageIndex === 1 ? (
              <CreditInvoiceList 
                {...props}
              />
            ) : (
              <PaymentInvoiceList 
                { ...props }
              />
            )}
          </Col>
        </Row>
      </AppLayout>
    </>
  )
}

export default CreditPaymaentPage