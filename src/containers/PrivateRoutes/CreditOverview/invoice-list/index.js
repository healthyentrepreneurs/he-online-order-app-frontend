import { connect } from 'react-redux'
import { Creators as AccountingCreators } from "../../../../services/redux/accounting/actions"
import {Creators as AuthCreators } from "../../../../services/redux/auth/actions"
import { Creators as PaymentCreators } from "../../../../services/redux/payments/actions"
import CreditOverView from './components/credit-invoice-list'

const mapStateToProps = state => ({
  credit_balance: state.accounting.credit_balance,
  invoices: state.accounting.invoices,
  loading: state.accounting.loading,
  error: state.accounting.error,
  hasMore: state.accounting.hasMore,
  nextPage: state.accounting.nextPage,
  totalCount: state.accounting.totalCount,
  lastUpdated: state.auth.lastUpdated,
  user: state.auth.user,
  userDetailsLoading: state.auth.isFetching,
  payment: state.payment,
  isMakingPayment: state.payment.isMakingPayment,
  paymentProcessing: state.payment.paymentProcessing,
  paymentSuccess: state.payment.paymentSuccess,
  paymentTimeup: state.payment.paymentTimeup,
  paymentError: state.payment.error,
  paymentAmount: state.payment.amount,
  savingDefaultPhoneNumber: state.auth.savingDefaultPhoneNumber,
  saveDefaultNumberSuccess: state.auth.saveDefaultNumberSuccess,
  amount: state.payment.amount
})


const mapActionToProps = dispatch => ({
  fetchCreditBalance: () => dispatch(AccountingCreators.fetchCreditBalance()),
  fetchInvoices: (query) => dispatch(AccountingCreators.fetchInvoices(query)),
  getUserDetailsWithSalt: id => dispatch(AuthCreators.getUserDetailsWithSalt(id)),
  makePaymentRequest: (data) => dispatch(PaymentCreators.makePaymentRequest(data)),
  resetInvoices: () => dispatch(AccountingCreators.resetInvoices()),
  setPaymentProcessing: () => dispatch(PaymentCreators.setPaymentProcessing()),
  setPaymentTimeup: (status) => dispatch(PaymentCreators.setPaymentTimeup(status)),
  waitForPayment: () => dispatch(PaymentCreators.waitForPayment()),
  resetPayment: () => dispatch(PaymentCreators.resetPayment()),
  saveDefaultPhoneNumber: (userId, payload) => dispatch(AuthCreators.saveDefaultPhoneNumber(userId, payload)),
  getUserDetails: (id) => dispatch(AuthCreators.getUserDetails(id)),
  completePayment: () => dispatch(PaymentCreators.completePayment())
})


export default connect(mapStateToProps, mapActionToProps)(CreditOverView);