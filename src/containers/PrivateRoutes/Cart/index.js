import { connect } from "react-redux";
import { Creators } from "../../../services/redux/cart/actions";
import { Creators as PaymentCreators } from "../../../services/redux/payments/actions";
import { Creators as AuthCreators } from "../../../services/redux/auth/actions";
import { Creators as UsersCreators } from "../../../services/redux/users/actions"
import { Creators as ProductCreators } from "../../../services/redux/products/actions"
import Cart from "./components";

const mapStateToProps = (state) => {
  const {
    ordering_for,
    order,
    currency,
    tax,
    sub_total,
    total,
    order_id,
    order_name,
    order_changed,
    order_item_price_changed,
    order_item_missing,
    order_less_stock,
    order_out_of_stock,
    order_item_blacklisted,
    order_item_unaccepted_products,
    order_sale_not_ok,
    is_sending_order,
    order_created,
    order_payment_method,
    errors_found,
    no_connection,
    free_products,
    free_product_out_of_stock,
    free_product_not_ok,
    checkingOrderPaymentStatus,
    orderPaymentStatusSuccess,
    paymentStatusData,
    posting_ordering_for,
  } = state.cart;

  const { user, savingDefaultPhoneNumber, saveDefaultNumberSuccess, ordering_for_data, fetching_ordering_for } = state.auth
  return {
    ordering_for,
    order,
    currency,
    sub_total,
    total,
    tax,
    order_id,
    order_name,
    order_changed,
    order_item_price_changed,
    order_item_missing,
    order_less_stock,
    order_out_of_stock,
    order_item_blacklisted,
    order_item_unaccepted_products,
    order_sale_not_ok,
    is_sending_order,
    order_created,
    order_payment_method,
    errors_found,
    no_connection,
    isMakingPayment: state.payment.isMakingPayment,
    paymentSuccess: state.payment.paymentSuccess,
    pymentError: state.payment.error,
    paymentTimeup: state.payment.paymentTimeup,
    paymentProcessing: state.payment.paymentProcessing,
    fakeMomoPaymentModalVisible: state.payment.fakeMomoPaymentModalVisible,
    fakeMomoPaymentPin: state.payment.fakeMomoPaymentPin,
    cart_free_products: free_products,
    user,
    free_products: user.free_products,
    free_product_out_of_stock,
    free_product_not_ok,
    savingDefaultPhoneNumber,
    saveDefaultNumberSuccess,
    ches_query_list: state.users.users,
    loadind_ches_query_list: state.users.loading,
    posting_user_update: state.users.posting,
    post_user_update_success: state.users.post_success,
    checkingOrderPaymentStatus,
    orderPaymentStatusSuccess,
    paymentStatusData,
    posting_ordering_for,
    ordering_for_data,
    fetching_ordering_for,
    ordering_for_id: state.auth.user.ordering_for
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    decrementItem: (id) => {
      dispatch(Creators.decrementItem(id));
    },
    incrementItem: (id) => {
      dispatch(Creators.incrementItem(id));
    },
    addItem: (item, action, quantity = null) => {
      dispatch(Creators.addItem(item, action, quantity));
    },
    removeItem: (id) => {
      dispatch(Creators.removeItem(id));
    },
    setCurrency: () => {
      dispatch(Creators.setCurrency());
    },
    sendOrder: (order) => {
      dispatch(Creators.sendOrder(order));
    },
    resetOrder: () => {
      dispatch(Creators.resetOrder());
    },
    resetOrderError: () => {
      dispatch(Creators.resetOrderError());
    },
    makePayment: (data) => {
      dispatch(PaymentCreators.makePaymentRequest(data));
    },
    makePaymentRequest: (data) => {
      dispatch(PaymentCreators.makePaymentRequest(data));
    },
    setFakeMomoPaymentModalVisible: (visible) => {
      dispatch(PaymentCreators.setFakeMomoPaymentModalVisible(visible))
    },
    setFakeMomoPaymentPin: (pin) => dispatch(PaymentCreators.setFakeMomoPaymentPin(pin)),
    completePayment: () => {
      dispatch(PaymentCreators.completePayment());
    },
    addFreeProducts: (products) => {
      dispatch(Creators.addFreeProducts(products))
    },
    waitForPayment: () => dispatch(PaymentCreators.waitForPayment()),
    setPaymentProcessing: (telco) => dispatch(PaymentCreators.setPaymentProcessing(telco)),
    setPaymentTimeup: () => {
      dispatch(PaymentCreators.setPaymentTimeup())
    },
    saveDefaultPhoneNumber: (userId, payload) => dispatch(AuthCreators.saveDefaultPhoneNumber(userId, payload)),
    getUserDetailsWithSalt: (id) => dispatch(AuthCreators.getUserDetailsWithSalt(id)),
    makeFakePayment: (data) => dispatch(PaymentCreators.makeFakePayment(data)),
    cancelFakePayment: () => dispatch(PaymentCreators.cancelFakePayment()),
    fetchUsers: query => dispatch(UsersCreators.fetchUsers(query)),
    updateUser: (id, payload, che) => dispatch(UsersCreators.updateUser(id, payload, che)),
    setOrderingFor: (employee, che) => dispatch(Creators.setOrderingFor(employee, che)),
    checkOrderPaymentStatus: (order_or_invoice, payload) => dispatch(PaymentCreators.checkOrderPaymentStatus(order_or_invoice, payload)),
    refreshProductListCache: () => dispatch(ProductCreators.refreshProductListCache()),
    resetProduct: () => dispatch(ProductCreators.refresh),
    cleanProduct: () => {
      dispatch(ProductCreators.cleanProducts());
    },
    checkCartProductPrice: (order) => dispatch(Creators.checkCartProductPrice(order)),
    fetchOrderingFor: query => dispatch(AuthCreators.fetchOrderingFor(query))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
