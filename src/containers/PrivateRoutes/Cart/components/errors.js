import React from "react";
import PriceArrowUpLogo from "../../../../assets/price-arrow-up.svg";
import PriceArrowDownLogo from "../../../../assets/price-arrow-down.svg";
import blackDot from "../../../../assets/black-dot.svg";
import redDot from "../../../../assets/red-dot.svg";
import { FormattedMessage } from "react-intl";

const Errors = ({
  order,
  order_item_price_changed,
  order_item_missing,
  order_less_stock,
  order_sale_not_ok,
  order_out_of_stock,
  order_item_blacklisted,
  order_item_unaccepted_products,
  free_product_out_of_stock,
  free_product_blacklisted,
  free_product_not_ok,
  cart_free_products,
  no_connection,
}) => {
  return (
    <>
      {order.length > 0 ? (
        <>
          <div className="order-errors-container">
            {order_item_price_changed ? (
              <div className="price-changed-container">
                <p><FormattedMessage id="PRICES_CHANGED_FOR" />:</p>
                {order
                  .filter((item) => item.price_changed)
                  .map((item) => (
                    <span
                      key={item.id}
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginBottom: "8px",
                      }}
                    >
                      <span
                        style={{
                          display: "flex",
                          justifyContent: "flex-start",
                          flexDirection: "row",
                          width: "90%",
                        }}
                      >
                        <img
                          src={blackDot}
                          style={{ marginRight: "8px" }}
                          alt="black-dot"
                        />{" "}
                        {item.name}
                      </span>
                      <img
                        src={
                          parseInt(item.new_price) > parseInt(item.old_price)
                            ? PriceArrowUpLogo
                            : PriceArrowDownLogo
                        }
                        alt="Price changed"
                      />
                    </span>
                  ))}
              </div>
            ) : null}
            {order_item_unaccepted_products ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}>
                  <FormattedMessage id="WE_ARE_UNAVLE_SELL_YOU_THE_FOLLOWING_PRODUCTS" />
                </p>
                {order
                  .filter((item) => item.unaccepted_products)
                  .map((item) => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name}
                    </span>
                  ))}
              </div>
            ) : null}
            {order_item_missing ? (
              <div
                className="product-missing-container"
                style={{ marginBottom: "20px" }}
              >
                <p style={{ fontWeight: "bold" }}><FormattedMessage id="FOLLOWING_PRODUCTS_ARE_UNAVAILABLE" /></p>
                {order
                  .filter((item) => !item.exists)
                  .map((item) => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name}
                    </span>
                  ))}
              </div>
            ) : null}
            {order_less_stock ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}><FormattedMessage id="FOLLOWING_PRODUCTS_HAVE_LESS_STOCK" />:</p>
                {order
                  .filter((item) => item.new_quantity)
                  .map((item) => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name} -
                      <span style={{ marginLeft: "5px" }}>
                        <FormattedMessage id="CHANGED_FROM" /> {item.old_quantity} <FormattedMessage id="TO" /> {item.new_quantity}
                      </span>
                    </span>
                  ))}
              </div>
            ) : null}
            {order_sale_not_ok ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}><FormattedMessage id="FOLLOWING_PRODUCTS_CANNOT_BE_SOLD" />:</p>
                {order
                  .filter((item) => !item.sale_ok)
                  .map((item) => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name} -
                      <span style={{ marginLeft: "5px" }}>
                        <FormattedMessage id="ITEM_REMOVED_CANNOT_BE_SOLD" />
                      </span>
                    </span>
                  ))}
              </div>
            ) : null}
            {order_out_of_stock ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}>
                  <FormattedMessage id="FOLLOWING_PRODUCTS_OUT_OF_STOCK" />:
                </p>
                {order
                  .filter((item) => item.out_of_stock)
                  .map((item) => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name} -
                      <span style={{ marginLeft: "5px" }}>
                        <FormattedMessage id="ITEM_REMOVED_OUT_OF_STOCK" />
                      </span>
                    </span>
                  ))}
              </div>
            ) : null}
            {order_item_blacklisted ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}><FormattedMessage id="FOLLOWING_PRODUCTS_BLACKLISTED" />:</p>
                {order
                  .filter((item) => item.blacklisted)
                  .map((item) => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name} -
                      <span style={{ marginLeft: "5px" }}>
                        <FormattedMessage id="ITEM_REMOVED_UNAVAILABLE" />
                      </span>
                    </span>
                  ))}
              </div>
            ) : null}
            {free_product_out_of_stock ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}><FormattedMessage id="FOLLOWING_FREE_PRODUCTS_OUT_OF_STOCK" />:</p>
                {cart_free_products
                  .filter(it => it.out_of_stock)
                  .map(item => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name} -
                      <span style={{ marginLeft: "5px" }}>
                        <FormattedMessage id="ITEM_REMOVED_OUT_OF_STOCK" />
                      </span>
                    </span>))}
              </div>
            ) : null}
            {free_product_not_ok ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}><FormattedMessage id="FOLLOWING_FREE_PRODUCTS_NOT_AVAILABLE" />:</p>
                {cart_free_products
                  .filter(it => !it.sale_ok)
                  .map(item => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name} -
                      <span style={{ marginLeft: "5px" }}>
                        <FormattedMessage id="ITEM_REMOVED_UNAVAILABLE" />
                      </span>
                    </span>))}
              </div>
            ) : null}
            {free_product_blacklisted ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}><FormattedMessage id="FOLLOWING_FREE_PRODUCTS_ARE_BLACKLISTED" /></p>
                {cart_free_products
                  .filter(it => it.blacklisted)
                  .map(item => (
                    <span key={item.id} style={{ display: "block" }}>
                      <img src={redDot} style={{ marginRight: "5px" }} alt="red-dot" />{" "}
                      {item.name} -
                      <span style={{ marginLeft: "5px" }}>
                        <FormattedMessage id="ITEM_REMOVED_BLACKLISTED" />
                      </span>
                    </span>))}
              </div>
            ) : null}
            {no_connection ? (
              <div className="product-missing-container">
                <p style={{ fontWeight: "bold" }}>
                  <FormattedMessage id="NO_INTERNET_CONNECTION" />
                </p>
              </div>
            ) : null}
          </div>
        </>
      ) : null}
    </>
  );
};

export default Errors;
