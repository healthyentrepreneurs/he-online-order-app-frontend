import React from "react";
import EmptyCartLogo from "../../../../assets/empty-cart.svg";
import { history } from "../../../../util/helpers/browserHistory";
import { FormattedMessage } from "react-intl";

const EmptyCart = (props) => {
	return (
		<>
			<div className="empty-cart-container">
				<img src={EmptyCartLogo} alt="Empty Cart" />
				<div className="text-content">
					<p style={{ textAlign: "center" }}>
						<strong style={{ fontSize: "18px" }}><FormattedMessage id="YOUR_CART_IS_EMPTY" /></strong>
					</p>
					<p style={{ textAlign: "center", fontSize: "16px" }}>
						<FormattedMessage id="YOU_CURRENTLY_HAVE_NO_ITEM_ADDED" />
					</p>
				</div>{" "}
				<button type="button" onClick={() => history.push('/dashboard')} className="he-btn" style={{ marginTop: "20px" }}>
					<FormattedMessage id="GO_TO_PRODUCT_PAGE"/>
				</button>
			</div>
		</>
	);
};

export default EmptyCart;
