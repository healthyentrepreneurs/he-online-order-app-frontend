import React from 'react'
import PropTypes from "prop-types"
import { Row, Col, AutoComplete, Input } from 'antd'
import { useIntl } from 'react-intl'

const CheFilter = (props) => {
  const intl = useIntl()

  const renderItem = (che) => ({
    odoo_id: che?.odoo_id,
    row: che,
    value: `${che?.name} - ${che?.phone_number}`,
    label: (
      <div
        key={che?.phone_number}
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          minWidth: "100%",
        }}
      >
        <Row style={{ width: "100%" }}>
          <Col span={13} style={{ width: "100%", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis" }}>
             <span>{che?.name}</span>
          </Col>
          <Col span={10} offset={1}>
            {che?.phone_number}
          </Col>
        </Row>
        {/* {che?.name}
        <span>
          {che?.phone_number}
        </span> */}
      </div>
    ),
  });

  return (
    <Row>
      <Col span={24}>
        <AutoComplete 
          style={{ minWidth: '100%' }}
          dropdownMatchSelectWidth="100%"
          options = {props.ches.map(it => renderItem(it))}
          onSelect={(value, options) => {
            props.onSelect(value, options?.row)
          }}
        >
          <Input.Search 
            onChange={e => {
              const val = e.target.value
              props.onChange(val)
            }} 
            size="large"
            placeholder={intl.formatMessage({ id: "SEARCH_FOR_NAME_OR_PHONE_NUMBER" })}
            loading={props.loading}
          />
        </AutoComplete>
      </Col>
    </Row>
  )
}

CheFilter.propTypes = {
  ches: PropTypes.array,
  onChange: PropTypes.func,
  onSelect: PropTypes.func,
  loading: PropTypes.bool
}

export default CheFilter