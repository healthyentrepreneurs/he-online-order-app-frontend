import { PlusOutlined } from '@ant-design/icons'
import { Button, Col, Row } from 'antd'
import React from 'react'


const FreeProduct = (props) => {
  const { product } = props
  return (
    <>
      <Row
        className="add-free-product-bar" style={{minHeight:60, width: "100%", padding: 10, marginBottom: 5}}>
        <Col span={4} style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start"
        }}>
          <Button 
            shape="circle" 
            style={{backgroundColor: "#5be6f0"}}
          >
            <PlusOutlined style={{color: "#ffffff"}} />
          </Button>
        </Col>
        <Col span={20} style={{display: "flex", flexDirection: "column"}}>
            <div style={{display: "flex", flexGrow: 1,flexDirection: "row", alignItems: "center", justifyContent: "flex-start"}}>
              <span>{product?.name}</span>
            </div>
            <div style={{display: "flex", flexGrow: 1, flexDirection: "row", alignItems: "center", justifyContent: "flex-start"}}>
              <span>FREE</span>
            </div>
        </Col>
      </Row>
    </>
  )
}

export default FreeProduct