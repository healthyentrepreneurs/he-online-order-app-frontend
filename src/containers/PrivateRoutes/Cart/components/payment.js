import React, { useState, useEffect } from "react";
import { Col, Row, Radio, Card, Form } from "antd";
import Modal from "react-modal";

import Steppers from "../../../../components/Steppers";
import "../../../../styles/cart.less";
import { InputField } from "../../../../components/InputField";
import Button from "../../../../components/Button";
import Spinner from "../../../../components/Spinner";
import CancelIcon from "../../../../assets/cancel.svg";
import GenericModal from "./generic-modal";
import { history } from "../../../../util/helpers/browserHistory";
import { getPaymentMethods } from "../../../../util/helpers/reusableFunctions";
import { FormattedMessage } from "react-intl";

const formatPhoneNumber = (phoneNumber, phoneNumberPrefix) => {
  let formatedNumber = phoneNumber;
  if(phoneNumber.startsWith('+257')) {
    formatedNumber = phoneNumber.replace('+257', '')
  } else if(phoneNumber.startsWith("+") ) {
    formatedNumber = phoneNumber.replace(/^.{4}/g, '0')
  }
  return formatedNumber
}

const cleanOrderObj = (order) => {
  let modifiedOrder = order
    .filter((item) => !item.out_of_stock)
    .filter((item) => !item.blacklisted)
    .filter((item) => !item.unaccepted_products)
    .filter(item => item.sale_ok)
    .reduce((acc, cur) => {
      let newObj = {
        product_id: cur.id,
        product_uom_qty: cur.quantity,
        last_price: cur.lst_price,
      };
      return (acc = [...acc, newObj]);
    }, []);
  return modifiedOrder;
};

const cleanFreeProductsOrderObj = (freeProducts) => {
  return freeProducts
    .map(item => {
      return {
        sale_ok: item.sale_ok,
        out_of_stock: item.out_of_stock,
        product_id: item.odoo_id,
        product_uom_qty: 1,
        last_price: 0.0,
        free_product: true,
      }
    })
    .filter(item => item.sale_ok)
    .filter(item => !item.out_of_stock)
    .filter(item => !item.blacklisted)
    .filter((item) => !item.unaccepted_products)
}

const addPrefixToPhoneNumber = (phoneNumber, prefix, isBurundi=false) => {
  if(phoneNumber.indexOf("+") === 0) {
    return phoneNumber
  } else if(isBurundi) {
    return `${prefix}${phoneNumber}`
  } else {
    const phoneNumberArr = phoneNumber.split("")
    phoneNumberArr[0] = prefix;
    const formatedNumber = phoneNumberArr.join("")
    return formatedNumber;
  }
}

const PaymentModal = ({
  visible,
  closeModal,
  makePayment,
  order_id,
  total,
  loading,
}) => {
  const user = JSON.parse(localStorage.getItem("he-che-user"));
  const [paymentValue, setPaymentValue] = useState(null);
  const [paymentChoiceError, setPaymentChoiceError] = useState("");
  const onPaymentChange = (e) => {
    let selectedOption = e.target.value;
    setPaymentValue(selectedOption);
  };
  const customStyles = {
    content: {
      transform: "translate(-50%, -50%)",
      position: "absolute",
      top: "54%",
      left: "50%",
      padding: "0px",
      width: "340px",
      height: "fit-content",
      zIndex: 50000,
    },
  };
  return (
    <Modal isOpen={visible} onRequestClose={() => closeModal()} style={customStyles}>
      <Card className="payment-modal">
        <img
          src={CancelIcon}
          alt="Cancel"
          onClick={() => closeModal()}
          className="close-icon"
        />
        <div className="stepper-wrapper">
          <Steppers current={2} />
        </div>
        <div>
          <h4 className="title">Pay with Mobile Money</h4>
          <p className="select-text">Select Payment Method</p>
          <Radio.Group onChange={onPaymentChange} value={paymentValue}>
            {getPaymentMethods().includes("momo") ? (
              <Radio className="payment-modal-radio" value={"momo"}>
                MTN
              </Radio>
            ) : null}
            {getPaymentMethods().includes("airtel") ? (
              <Radio className="payment-modal-radio" value={"airtel"}>
                Airtel
              </Radio>
            ) : null}
            {getPaymentMethods().includes("mpesa") ? (
              <Radio className="payment-modal-radio" value={"mpesa"}>
                Mpesa
              </Radio>
            ) : null}
            {getPaymentMethods().includes("ihela") ? (
              <Radio className="payment-modal-radio" value={"ihela"}>
                EcoCash
              </Radio>
            ) : null}
          </Radio.Group>
        </div>
        <Form
          initialValues={{
            phone_number: formatPhoneNumber(user.phone_number, user?.country?.phone_number_prefix),
          }}
          onFinish={(values) => {
            if (paymentValue) {
              setPaymentChoiceError(null);
              if (values?.phone_number.includes("@")){
              
                const payload = {
                  email: values?.phone_number,
                  order_id,
                  amount: total,
                  payment_method: paymentValue,
                  partial: false
                }
                makePayment(payload);

               } else {
                const phoneNumber = addPrefixToPhoneNumber(values?.phone_number, user?.country?.phone_number_prefix, paymentValue === 'ihela')
              
                const payload = {
                  phone_number: phoneNumber,
                  order_id,
                  amount: total,
                  payment_method: paymentValue,
                }
                // console.log('payment type: ', paymentValue)
                // console.log('pay paload: ', payload)
                makePayment(payload);
               }
            } else {
              setPaymentChoiceError("Please select a mobile money carrier");
            }
          }}
        >
          <InputField
            label="Phone Number"
            placeholder="Enter the phone number"
            inputmode="numeric"
            name="phone_number"
          />
          {paymentChoiceError && (
            <p style={{ color: "red" }}>{paymentChoiceError}</p>
          )}
          <Form.Item>
            <Button text="Pay Now" htmlType="submit" loading={loading} />
          </Form.Item>
        </Form>
      </Card>
    </Modal>
  );
};

const Payment = ({
  order,
  total,
  order_id,
  order_name,
  is_sending_order,
  order_created,
  order_payment_method,
  errors_found,
  isMakingPayment,
  completePayment,
  cart_free_products,
  ...props
}) => {
  const [value, setValue] = useState();
  const [visible, setModalVisibiltiy] = useState(false);
  const [payment_method_errors, setPaymentMethodErrors] = useState();
  const [paymentModalVisibility, setPaymentModalVisibility] = useState(false);
  const [modalState, setGenericModalVisibility] = useState({
    isVisible: false,
    title: null,
    text: null,
    showReturnHomeButton: true,
    isSuccess: true,
    callBack: () => {},
  });
  const onChange = (e) => {
    let selectedOption = e.target.value;
    setValue(selectedOption);
    setPaymentMethodErrors("");
  };
  const radioStyle = {
    display: "block",
    height: "30px",
    lineHeight: "30px",
  };

  let buttonState = is_sending_order ? <Spinner color="#ffffff" /> : "Pay";

  useEffect(() => {
    Modal.setAppElement("#root")
    if (order_created) {
      if (["momo", "ihela"].includes(value)) {
        setModalVisibiltiy(true);
      } else {
        if (order_payment_method === "cash" || order_payment_method === "test") {
          setGenericModalVisibility({
            isVisible: true,
            title:
              value === "test"
                ? <FormattedMessage id="TEST_ORDER_CREATED_SUCCCESSFULLY" />
                : <FormattedMessage id="ORDER_CREATED_SUCCESSFULLY" />,
            text:
              value === "test"
                ? `This is a test order used for practice/training. This order is not received by HE.`
                : value === "cash"
                ? `${<FormattedMessage id="ORDER_CREATED_SUCCESSFULLY_AND_ORDER_ID" />} #${order_name}.`
                : `${<FormattedMessage id="YOUR_ORDER_CREATED_SUCCESSFULLY" />} #${order_name}`,
            showReturnHomeButton: true,
            isSuccess: true,
            callBack: () => {
              props.resetOrder();
              history.push("/dashboard");
            },
          });
        }
      }
    } else {
      if (errors_found) {
        if (!is_sending_order) window.scrollTo(0, 0);
      }
    }
    setPaymentModalVisibility(
      ["successful", "failure"].includes(props.match.params.type)
    );
    return () => {
      if (
        order_created &&
        (order_payment_method === "cash" || order_payment_method === "test")
      )
        props.resetOrder();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [is_sending_order, order_created, errors_found]);

  let paymentModalOptions = {
    orderId: props.match.params.orderId,
  };
  if (props.match.params.type === "successful") {
    completePayment();
    paymentModalOptions.isSuccess = true;
    paymentModalOptions.title = "Payment Successful";
    paymentModalOptions.text = `Your payment is successful and your order ID is #${paymentModalOptions.orderId}`;
    paymentModalOptions.executeAction = () => {
      props.resetOrder();
      setPaymentModalVisibility(false);
      history.push("/dashboard");
    };
    paymentModalOptions.showReturnHomeButton = true;
  } else if (props.match.params.type === "failure") {
    completePayment();
    paymentModalOptions.isSuccess = false;
    paymentModalOptions.title = "Payment Unsuccessful";
    paymentModalOptions.text = `Your payment is unsuccessful because ${decodeURI(
      props.location.search.slice(5)
    )}`;
    paymentModalOptions.executeAction = () => {
      setPaymentModalVisibility(false);
      history.push("/cart/details/default/");
    };
    paymentModalOptions.showReturnHomeButton = false;
    paymentModalOptions.buttonText = "Try Again";
    paymentModalOptions.showSteppers = false;
  }

  return (
    <>
      <Row>
        <Col span={24}>
          <div className="payment-container">
            <p>Pay</p>
            <p
              style={{
                color: "#be090a",
                fontWeight: "bold",
                marginBottom: "0",
              }}
            >
              {payment_method_errors ? payment_method_errors : ""}
            </p>
            <Radio.Group onChange={onChange} value={value}>
              {getPaymentMethods().includes("cash") ? (
                <Radio style={radioStyle} value={"cash"}>
                  Pay with cash
                </Radio>
              ) : null}
              {getPaymentMethods().includes("momo") ||
              getPaymentMethods().includes("airtel") ||
              getPaymentMethods().includes("mpesa") ||
              getPaymentMethods().includes("ihela") ? (
                <Radio style={radioStyle} value={"ihela"}>
                  Pay with mobile money
                </Radio>
              ) : null}
              {getPaymentMethods().includes("test") ? (
                <Radio style={radioStyle} value={"test"}>
                  Test payment
                </Radio>
              ) : null}
            </Radio.Group>

            <button
              className="he-btn"
              style={{ marginTop: "20px" }}
              onClick={(e) => {
                e.preventDefault();
                props.nextStep(2);
                let cleanedOrder = cleanOrderObj(order);
                let cleanedFreeOrder = cleanFreeProductsOrderObj(cart_free_products)
                let orders = cleanedOrder.concat(cleanedFreeOrder || [])
                if (value) {
                  if (cleanedOrder && cleanedOrder.length > 0) {
                    props.sendOrder({
                      order_line: orders,
                      total,
                      sub_total: total,
                      payment_method: value,
                    });
                  } else {
                    // clear the cart
                    props.resetOrder();
                  }
                } else {
                  setPaymentMethodErrors("Please select a payment method");
                }
              }}
            >
              {buttonState}
            </button>
          </div>
          <PaymentModal
            visible={visible}
            closeModal={() => setModalVisibiltiy(false)}
            makePayment={(data) => props.makePayment(data)}
            order_id={order_id}
            total={total}
            loading={isMakingPayment}
            user={props.user}
          />
          <GenericModal
            visible={modalState.isVisible}
            closeModal={() => {
              if (modalState.isSuccess) props.resetOrder();
              setGenericModalVisibility({ ...modalState, isVisible: false });
            }}
            isSuccess={modalState.isSuccess}
            showReturnHomeButton={modalState.showReturnHomeButton}
            title={modalState.title}
            text={modalState.text}
            executeAction={modalState.callBack}
          />
          <GenericModal
            visible={paymentModalVisibility}
            closeModal={paymentModalOptions.executeAction}
            isSuccess={paymentModalOptions.isSuccess}
            showReturnHomeButton={paymentModalOptions.showReturnHomeButton}
            buttonText={paymentModalOptions.buttonText}
            title={paymentModalOptions.title}
            text={paymentModalOptions.text}
            executeAction={paymentModalOptions.executeAction}
            showSteppers={paymentModalOptions.showSteppers}
          />
        </Col>
      </Row>
    </>
  );
};

export default Payment;
