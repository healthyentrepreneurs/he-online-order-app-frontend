import React, { useState, useEffect } from "react";
import Steppers from "../../../../components/Steppers";
import OrderSummary from "./order-summary";
import Errors from "./errors";
import AppLayout from "../../../../components/Layout/Layout";
import { Row, Col, Alert, Divider, Modal, Spin } from "antd"
import { checkExpiryDateTime } from "../../../../util/helpers/commonHelper"
import "../../../../styles/cart.less";
import { numberWithCommas } from "../../../../util/helpers/reusableFunctions";
import OrderPayment from "./order-payment";
import PaymentProcessingBar from "../../../../reusable-components/PaymentProcessingBar"
import CheFilter from "./CheFilter";
import { useCallback } from "react";
import _debounce from "lodash/debounce"
import { FormattedMessage, useIntl } from "react-intl";
import CustomerDetails from "./customer-details";
import { cleanCartProductsObject } from "../../../../util/helpers/commonHelper"


const Cart = (props) => {
  const intl = useIntl()
  
  const {
    ches_query_list,
    loadind_ches_query_list,
    posting_user_update,
    post_user_update_success,
    setOrderingFor,
    posting_ordering_for,
    user,
    checkCartProductPrice,
    ordering_for_data,
    // ordering_for_id,
    fetching_ordering_for,
    // fetchOrderingFor
  } = props

  
  const [current, setCurrent] = useState(1);
  const [userSearchVisible, setUserSeachVisible] = useState(false)
  const [ selectedOrderingFor, setSelectedOrderingFor ] = useState(null)
  //const [orderingForChe, setOrderingForChe] = useState(null)
  //const [che, setChe] = useState(null)
  //const userProfile = JSON.parse(localStorage.getItem("he-che-user")); lets fetch from redux store instead

  // const getCartCustomer = (userData) => {
  //   if(userData?.contact_type === "employee") {
  //     if(localStorage.getItem("employee_ordering_for")) {
  //       return JSON.parse(localStorage.getItem("employee_ordering_for"))
  //     } else {
  //       return userData
  //     }
  //   } else {
  //     return userData
  //   }
  // }

  const customerDetailsData = () => {
    return ordering_for_data?.odoo_id ? ordering_for_data : user
  }

  const handleEditCheClick = () => {
    setUserSeachVisible(true)
  }

  const checkCartPriceAgainstPricelist = () => {
    if(props.order.length > 0) {
      checkCartProductPrice({order_line: cleanCartProductsObject(props.order)})
    }
  }

  const onChangeDebounceFn = useCallback(_debounce(handleCheFilterOnChange, 1000), [])

  function handleCheFilterOnChange(value) {
    props.fetchUsers({ search_term: value, company_id: user?.country?.id })
  }

  // useEffect(() => {
  //   if(ordering_for_id && user.contact_type === 'employee') {
  //     fetchOrderingFor({ search_term: ordering_for_id })
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [ordering_for_id])

  useEffect(() => {
    props.resetOrderError()
    props.setCurrency();
    window.scrollTo(0, 0);

    //console.log('<=========== lets check for price change discount =========>', props.order)
    // if(props.order.length > 0) {
    //   checkCartProductPrice({order_line: cleanCartProductsObject(props.order)})
    // }
    checkCartPriceAgainstPricelist()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(()=> {
    if(!posting_user_update && post_user_update_success) {
      localStorage.setItem("employee_ordering_for", JSON.stringify(selectedOrderingFor))
    }
  }, [post_user_update_success, posting_user_update, selectedOrderingFor])

  useEffect(() => {
    const products = (props.free_products || [])
      .map(it=> {
        let data = it
        data['sale_ok'] = true
        data['out_of_stock'] = false
        data["blacklisted"] = false
        data["unaccepted_products"] = false
        return data
      })
      .filter(it => checkExpiryDateTime(it.expiry_date))
      .filter(it => {
        if(!it.min_order_value) {
          return true
        } else {
          if(Number(props?.total) >= Number(it?.min_order_value)) {
            return true
          } else {
            return false;
          }
        }
      })
    props.addFreeProducts(products)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props?.user?.free_products, props.total]);

  return (
    <>
      <AppLayout title={<><FormattedMessage id="IN_YOUR_CART" /></>}>
        {props.paymentWaiting && (
          <PaymentProcessingBar reduceMargin={true} provider="MTN" />
        )}

        <div className="cart-container" style={{marginTop: props.paymentWaiting ? 50 : ""}}>
          {props.order && props.order.length > 0 ? (
            <div style={{ margin: "-36px 0 23px 0" }}>
              <Row>
                <Col span={24}>
                  <Steppers current={current} /*onPress={(step) => setCurrent(step)}*/ />
                </Col>
              </Row>
              {fetching_ordering_for ? <Spin/> : (
                <CustomerDetails
                name={customerDetailsData()?.name}
                phoneNumber={customerDetailsData()?.phone_number || customerDetailsData()?.default_phone_number}
                //username={getCartCustomer(user)?.username}
                //phoneNumber={getCartCustomer(user)?.default_phone_number || getCartCustomer(user)?.phone_number}
                showEditButton={props?.user?.contact_type === "employee"}
                loading={posting_ordering_for}
                onEdit={handleEditCheClick}
                />
              )}
              <Divider />
              {(props.total < props.user?.country.minimum_order_value) && (
                <Row style={{marginTop: 10}}>
                  <Col span={24}>
                    <Alert 
                      message={intl.formatMessage({ id: "ORDER_VALUE_TOO_LOW" })}
                      description={`${intl.formatMessage({ id: "MINIMUM_ORDER_VALUE_IS" })} ${numberWithCommas(props.user?.country?.minimum_order_value)} ${props?.currency}`}
                      type="warning"
                      showIcon
                    />
                  </Col>
                </Row>
              )}
            </div>
          ) : null}
          {props.order_changed || props.free_product_out_of_stock || props.no_connection ? <Errors {...props} /> : null}
          <OrderSummary {...props} checkCartPriceAgainstPricelist = {checkCartPriceAgainstPricelist} />
          {props.order && props.order.length > 0 ? (
            <OrderPayment
              nextStep={(step) => setCurrent(step)} 
              {...props}
            />
          ) : null}
        </div>
        <Modal
          visible={userSearchVisible}
          footer={null}
          onCancel={() => { 
            setUserSeachVisible(false)
          }}
          style={{  }}
          bodyStyle={{ paddingTop: 50 }}
        >
          <CheFilter
            loading ={ loadind_ches_query_list } 
            ches={ches_query_list} 
            onChange={filter => {
              onChangeDebounceFn(filter)
            }} 
            onSelect={(value, row) => {
              setUserSeachVisible(false)
              setSelectedOrderingFor(row)
              setOrderingFor(user, row)
            }}
          />
        </Modal>
      </AppLayout>
    </>
  );
};

export default Cart;
