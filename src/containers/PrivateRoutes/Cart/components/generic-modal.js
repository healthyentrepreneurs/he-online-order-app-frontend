import React from "react";
import { Card } from "antd";
import Modal from "react-modal";

import Steppers from "../../../../components/Steppers";
import "../../../../styles/cart.less";
import Button from "../../../../components/Button";
import CancelIcon from "../../../../assets/cancel.svg";
import SuccessIcon from "../../../../assets/success.svg";
import ErrorIcon from "../../../../assets/error.svg";
import { useWakeLock } from "react-screen-wake-lock"
import { FormattedMessage, useIntl } from "react-intl";

const GenericModal = (props) => {
  const intl = useIntl()
  const {
    visible,
    closeModal,
    title,
    text,
    deliveryStart,
    deliveryEnd,
    transactionId,
    isSuccess = true,
    // showReturnHomeButton = true,
    buttonText = intl.formatMessage({ id: "RETURN_TO_HOME" }),
    showSteppers = true,
  } = props

  const customStyles = {
    content: {
      transform: "translate(-50%, -50%)",
      position: "absolute",
      top: "54%",
      left: "50%",
      padding: "0px",
      width: "340px",
      height: "fit-content",
      zIndex: 50000,
    },
  };
  const { isSupported, released, release } = useWakeLock({
    onRequest: () => console.log('Screen Wake Lock: requested!'),
    onError: () => console.log('An error happened'),
    onRelease: () => console.log('Screen Wake Lock: released!'),
  })
  return (
    <Modal 
      isOpen={visible} 
      onRequestClose={() => closeModal()} 
      style={customStyles} 
      shouldCloseOnOverlayClick={false}
      shouldCloseOnEsc={false}
    >
      <Card className="generic-modal">
        <img
          src={CancelIcon}
          alt="Cancel"
          onClick={() => closeModal()}
          className="close-icon"
        />
        {showSteppers && (
          <div className="stepper-wrapper">
            <Steppers current={3} />
          </div>
        )}
        
        <div className="text-content">
          {isSuccess ? (
            <img src={SuccessIcon} alt="success" className="status-logo" />
          ) : (
            <img src={ErrorIcon} alt="error" className="status-logo" />
          )}
          <h4 className="title">{title}</h4>
          <p className="text">{text}</p>
          {isSuccess ? (
            <>
              {transactionId && (<p>{ <FormattedMessage id="TRANSACTION_ID_IS" />} {transactionId}</p>)}
              {deliveryStart && deliveryEnd && (<p style={{ textAlign: "center" }}><FormattedMessage id="DELIVERY_DATE_TXT" values={{ deliveryStart, deliveryEnd }} /></p>)}
            </>
          ) : null}
          <Button
            className="action-btn"
            text={buttonText ? buttonText : "Try again"}
            onClick={(e) => {
              e.preventDefault();
              console.log('lock status', released)
              if(isSupported) {
                release()
              }
              props.executeAction();
            }}
          />
        </div>
      </Card>
    </Modal>
  );
};

export default GenericModal;
