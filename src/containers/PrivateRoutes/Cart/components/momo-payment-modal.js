import React, {useState} from 'react'
import { Modal } from "antd";
// import Modal from "react-modal";
import Steppers from "../../../../components/Steppers";
import "../../../../styles/cart.less";
import { getPhoneNumberWithoutPrefix } from "../../../../util/helpers/reusableFunctions";
import PaymentSuccessful from './payment-successful';
import MomoPaymentForm from './momo-payment-form';
import PaymentProcessing from '../../../../components/InvoicePayment/components/payment-processing';
import { PAYMENT_WAIT_TIME } from '../../../../util/configs';
import PaymentTimeup from './payment-timeup';
import { history } from '../../../../util/helpers/browserHistory';


const addPrefixToPhoneNumber = (phoneNumber, prefix, isBurundi=false) => {
  if(phoneNumber.indexOf("+") === 0) {
    return phoneNumber
  } else if(isBurundi) {
    return `${prefix}${phoneNumber}`
  } else {
    const phoneNumberArr = phoneNumber.split("")
    phoneNumberArr[0] = prefix;
    const formatedNumber = phoneNumberArr.join("")
    return formatedNumber;
  }
}

const MomoPaymentModal = (props) => {
  const {
    visible,
    closeModal,
    makePayment,
    order_id,
    total,
    loading,
    serviceProvider,
    onPhoneNumberChange,
    waitTime,
    setWaitTime,
    paymentSuccess,
    paymentTimeup,
  } = props
  const user = JSON.parse(localStorage.getItem("he-che-user"));
  const [paymentChoiceError, setPaymentChoiceError] = useState("");
  
  return (
    <Modal visible={visible} onCancel={() => closeModal()} footer={false} bodyStyle={{padding: "20px 0px 0px 0px", minHeight: 400}}>
      <div className="stepper-wrapper" style={{marginTop: 20, padding: "0px 20px 0px 20px"}}>
        <Steppers current={2} />
      </div>
      {(!loading) ? (
        <MomoPaymentForm
          serviceProvider={serviceProvider}
          paymentChoiceError={paymentChoiceError}
          onPhoneNumberChange={onPhoneNumberChange}
          {...props}
          initialValues={{
            phone_number: getPhoneNumberWithoutPrefix(user.default_phone_number || user.phone_number, user?.country?.phone_number_prefix),
          }}
          onFinish={(values) => {
            if (serviceProvider) {
              setPaymentChoiceError(null);
              setWaitTime(PAYMENT_WAIT_TIME)
              setInterval(() => {
                if(waitTime > 0) {
                  setWaitTime(waitTime => waitTime - 1)
                }
              }, 1000)
              if (values?.phone_number.includes("@")){
                const payload = {
                  email: values?.phone_number,
                  order_id,
                  amount: total,
                  payment_method: serviceProvider,
                  partial: false
                }
                makePayment(payload);
  
                } else {
                const phoneNumber = addPrefixToPhoneNumber(values?.phone_number, user?.country?.phone_number_prefix)
              
                const payload = {
                  phone_number: phoneNumber,
                  order_id,
                  amount: total,
                  payment_method: serviceProvider,
                  partial: false
                }
                makePayment(payload);
                }
            } else {
              setPaymentChoiceError("Mobile money carrier invalid!");
            }
          }}
        />
      ) : null}
      {(loading && !paymentTimeup) ? (
        <PaymentProcessing 
          timeRemaining={waitTime}
        />
      ) : null}
      {(!loading && paymentSuccess) ? (
        <PaymentSuccessful 
          orderId={order_id}
        />
      ) : null}
      {(paymentTimeup && loading) ? (
        <PaymentTimeup
          timeRemaining={waitTime}
          onCheckPaymentLater={() => {
            props.checkPaymentLater(true, serviceProvider)
            history.push("/dashboard")
          }}
          onWaitPayment={() => {
            props.checkPaymentLater(false, serviceProvider)
            props.setPaymentTimeup(false)
          }}
        />
      ) : null}
    </Modal>
  );
};

export default MomoPaymentModal;