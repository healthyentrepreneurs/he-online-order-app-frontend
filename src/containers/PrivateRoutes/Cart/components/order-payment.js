import React, { useState, useEffect } from "react";
import { Col, Row, Radio } from "antd";
import Modal from "react-modal";
import Spinner from "../../../../components/Spinner";
import GenericModal from "./generic-modal";
import { history } from "../../../../util/helpers/browserHistory";
import { getPaymentMethods } from "../../../../util/helpers/reusableFunctions";
import GenericMomoPayment from "../../../../reusable-components/GenericMomoPayment";
import MomoPaymentModal from "../../../../reusable-components/GenericMomoPayment/momo-payment-modal";
import { useWakeLock } from 'react-screen-wake-lock';
import { FormattedMessage, useIntl } from "react-intl";
import { roundUpFloat } from "../../../../util/helpers/commonHelper";

const cleanOrderObj = (order) => {
  let modifiedOrder = order
    .filter((item) => !item.out_of_stock)
    .filter((item) => !item.blacklisted)
    .filter(item => item.sale_ok)
    .filter(item => !item.unaccepted_products)
    .reduce((acc, cur) => {
      let newObj = {
        product_id: cur.id,
        product_uom_qty: cur.quantity,
        last_price: roundUpFloat(cur.lst_price, 2),
        price_list_rule: cur.price_list_rule
      };
      return (acc = [...acc, newObj]);
    }, []);
  return modifiedOrder;
};

const cleanFreeProductsOrderObj = (freeProducts) => {
  return freeProducts 
    .map(item => {
      return {
        sale_ok: item.sale_ok,
        out_of_stock: item.out_of_stock,
        product_id: item.odoo_id,
        product_uom_qty: 1,
        last_price: 0.0,
        free_product: true,
      }
    })
    .filter(item => item.sale_ok)
    .filter(item => !item.out_of_stock)
    .filter(item => !item.blacklisted)
    .filter(item => !item.unaccepted_products)
}


const OrderPayment = (props) => {
  const {
    order,
    total,
    order_name,
    is_sending_order,
    order_created,
    order_payment_method,
    errors_found,
    completePayment,
    cart_free_products,
  } = props
  const intl = useIntl()
  const query = new URLSearchParams(props.location.search);
  const [value, setValue] = useState();
  const [visible, setModalVisibiltiy] = useState(false);
  const [payment_method_errors, setPaymentMethodErrors] = useState();
  const [paymentModalVisibility, setPaymentModalVisibility] = useState(false);
  const [modalState, setGenericModalVisibility] = useState({
    isVisible: false,
    title: null,
    text: null,
    showReturnHomeButton: true,
    isSuccess: true,
    callBack: () => {},
  });
  const { release } = useWakeLock({
    onRequest: () => console.log('Screen Wake Lock: requested!'),
    onError: () => console.log('An error happened'),
    onRelease: () => console.log('Screen Wake Lock: released!'),
  })


  const onChange = (e) => {
    let selectedOption = e.target.value;
    setValue(selectedOption);
    setPaymentMethodErrors("");
  };
  const radioStyle = {
    display: "block",
    height: "30px",
    lineHeight: "30px",
  };

  let buttonState = is_sending_order ? <Spinner color="#ffffff" /> : (<FormattedMessage id="PAY" />);

  useEffect(() => {
    Modal.setAppElement("#root")
    if (order_created) {
      if (["momo", "ihela"].includes(value) || order_payment_method === "test") {
        setModalVisibiltiy(true);
      } else {
        if (order_payment_method === "cash") {
          setGenericModalVisibility({
            isVisible: true,
            title: <FormattedMessage id="ORDER_CREATED_SUCCESSFULLY" />,
            text: `${intl.formatMessage({ id: "ORDER_CREATED_SUCCESSFULLY_AND_ORDER_ID" })} #${order_name}.`,
            showReturnHomeButton: true,
            isSuccess: true,
            callBack: () => {
              props.resetOrder();
              history.push("/dashboard");
            },
          });
        }
      }
    } else {
      if (errors_found) {
        if (!is_sending_order) window.scrollTo(0, 0);
      }
    }
    setPaymentModalVisibility(
      ["successful", "failure"].includes(props.match.params.type)
    );
    return () => {
      if (
        order_created &&
        (order_payment_method === "cash" || order_payment_method === "test")
      )
        props.resetOrder();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [is_sending_order, order_created, errors_found]);

  let paymentModalOptions = {
    orderId: props.match.params.orderId,
  };
  
  
  
  
  if (props.match.params.type === "successful") {    
    completePayment();
    paymentModalOptions.isSuccess = true;
    paymentModalOptions.title = (<FormattedMessage id="PAYMENT_SUCCESSFUL" />);
    paymentModalOptions.text = `${intl.formatMessage({ id: "PAYMENT_SUCCESSFUL_YOUR_ORDER_ID" })} #${paymentModalOptions.orderId}`;
    paymentModalOptions.deliveryStart = query.get("delivery_start")
    paymentModalOptions.deliveryEnd= query.get("delivery_end")
    paymentModalOptions.transactionId = query.get("transaction_id")
    paymentModalOptions.executeAction = () => {
      props.resetOrder();
      setPaymentModalVisibility(false);
      history.push("/dashboard");
    };
    paymentModalOptions.showReturnHomeButton = true;
  } else if (props.match.params.type === "failure") {
    completePayment();
    paymentModalOptions.isSuccess = false;
    paymentModalOptions.title = <FormattedMessage id="PAYMENT_UNSUCCESSFUL" />;
    // paymentModalOptions.text = `${intl.formatMessage({ id: "PAYMENT_UNSUCCESSFUL_BECAUSE" })} ${decodeURI(
    //   props.location.search.slice(5)
    // )}`;
    paymentModalOptions.text  = `${intl.formatMessage({ id: "PAYMENT_UNSUCCESSFUL_BECAUSE" })} ${query.get("msg")}`
    paymentModalOptions.deliveryStart = query.get("delivery_start")
    paymentModalOptions.deliveryEnd = query.get("delivery_end")
    paymentModalOptions.transactionId = query.get("transaction_id")
    paymentModalOptions.executeAction = () => {
      setPaymentModalVisibility(false);
      history.push("/cart/details/default/");
    };
    paymentModalOptions.showReturnHomeButton = false;
    paymentModalOptions.buttonText = intl.formatMessage({ id: "TRY_AGAIN" });
    paymentModalOptions.showSteppers = false;
  }

  return (
    <>
      <Row id="order-payment-id">
        <Col span={24}>
          <div className="payment-container">
            <p><FormattedMessage id="PAY" /></p>
            <p
              style={{
                color: "#be090a",
                fontWeight: "bold",
                marginBottom: "0",
              }}
            >
              {payment_method_errors ? payment_method_errors : ""}
            </p>
            <Radio.Group onChange={onChange} value={value}>
              {getPaymentMethods().includes("cash") ? (
                <Radio style={radioStyle} value={"cash"}>
                  <FormattedMessage id="PAY_WITH_CASH" />
                </Radio>
              ) : null}
              {getPaymentMethods().includes("momo") ||
              getPaymentMethods().includes("airtel") ||
              getPaymentMethods().includes("mpesa") ||
              getPaymentMethods().includes("ihela") ? (
                <Radio style={radioStyle} value={"ihela"}>
                  <FormattedMessage id="PAY_WITH_MOBILE_MONEY" />
                </Radio>
              ) : null}
              {getPaymentMethods().includes("test") ? (
                <Radio style={radioStyle} value={"test"}>
                  <FormattedMessage id="TEST_PAYMENT" />
                </Radio>
              ) : null}
            </Radio.Group>

            <button
              className="he-btn"
              style={{ marginTop: "20px" }}
              onClick={(e) => {
                e.preventDefault();
                props.nextStep(2);
                let cleanedOrder = cleanOrderObj(order);
                let cleanedFreeOrder = cleanFreeProductsOrderObj(cart_free_products)
                let orders = cleanedOrder.concat(cleanedFreeOrder || [])
                if (value) {
                  if (cleanedOrder && cleanedOrder.length > 0) {
                    props.sendOrder({
                      order_line: orders,
                      total: Math.ceil(total),
                      sub_total: Math.ceil(total),
                      payment_method: value,
                    });
                  } else {
                    // clear the cart
                    props.resetOrder();
                  }
                } else {
                  setPaymentMethodErrors(<FormattedMessage id="PLEASE_SELECT_PAYMENT_METHOD" />);
                }
              }}
            >
              {buttonState}
            </button>
          </div>
          <GenericMomoPayment
            {...props}
            visible={visible}
            paymentType="order"
            closeModal={()=> setModalVisibiltiy(false)}
            order={order}
            isTestPayment={order_payment_method === "test"}
          />
          <GenericModal
            visible={modalState.isVisible}
            closeModal={() => {
              release()
              if (modalState.isSuccess) props.resetOrder();
              setGenericModalVisibility({ ...modalState, isVisible: false });
            }}
            isSuccess={modalState.isSuccess}
            showReturnHomeButton={modalState.showReturnHomeButton}
            title={modalState.title}
            text={modalState.text}
            executeAction={modalState.callBack}
          />
          <GenericModal
            visible={paymentModalVisibility}
            closeModal={paymentModalOptions.executeAction}
            isSuccess={paymentModalOptions.isSuccess}
            showReturnHomeButton={paymentModalOptions.showReturnHomeButton}
            buttonText={paymentModalOptions.buttonText}
            title={paymentModalOptions.title}
            text={paymentModalOptions.text}
            deliveryStart={paymentModalOptions?.deliveryStart}
            deliveryEnd={paymentModalOptions?.deliveryEnd}
            transactionId={paymentModalOptions?.transactionId}
            executeAction={paymentModalOptions.executeAction}
            showSteppers={paymentModalOptions.showSteppers}
            {...props}
          />
          {(props.isMakingPayment && props.fakeMomoPaymentModalVisible && order_payment_method === "test") ? (
          <MomoPaymentModal
            visible={props?.fakeMomoPaymentModalVisible && order_payment_method === "test"}
            onChange={value => {
              props.setFakeMomoPaymentPin(value)
            }}
            pin={props.fakeMomoPaymentPin}
            onCancel={() => {
              props.cancelFakePayment()
            }}
            onSubmit={() => {
              props.makeFakePayment({})
            }}
          />
        ): null}
          {/* <GenericTestMomoPayment
            visible={testModalVisible}
            {...props}
            closeModal={()=> setTestModalVisible(false)}
            order={order}
          /> */}
        </Col>
      </Row>
    </>
  );
};

export default OrderPayment;
