import React from 'react'
import PropTypes from "prop-types"
import { FormattedMessage } from 'react-intl'
import { Row, Col, Spin } from 'antd'
import CustomIcon from "../../../../components/CustomIcon";

const CustomerDetails = ({
  name,
  phoneNumber,
  showEditButton,
  loading,
  onEdit
}) => {
  return (
    <Row>
      <Col span={24} style={{ paddingTop: 15 }}>
        <Row>
          <Col span={24}>
            <span style={{ fontWeight: "bolder", fontSize: 18 }}><FormattedMessage id="CUSTOMER" />: </span>
          </Col>
        </Row>
        <Row style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <Col span={20} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
            <span style={{ marginRight: 10 }}>{name}</span> -  <span style={{ marginLeft: 10 }}>{phoneNumber}</span>
          </Col>
          <Col span={4} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            {showEditButton ? (loading ? <Spin /> : <CustomIcon imageUrl="/images/edit_image.png" onClick={() => onEdit()} />) : null}
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

CustomerDetails.propTypes = {
  phoneNumber: PropTypes.string,
  onEdit: PropTypes.func,
  loading: PropTypes.bool,
  showEditButton: PropTypes.bool,
  name: PropTypes.string
}

export default CustomerDetails