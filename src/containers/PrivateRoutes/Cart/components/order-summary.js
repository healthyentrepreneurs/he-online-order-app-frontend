import React, { Fragment } from "react";
import { Col, Row, Button, Input } from "antd";
import CartDeleteButton from "../../../../assets/cart-delete-btn.svg";
import PriceIncreasedLogo from "../../../../assets/price-increased.svg";
import PriceDecreasedLogo from "../../../../assets/price-decreased.svg";
import { numberWithCommas } from "../../../../util/helpers/reusableFunctions";
import EmptyCart from "./empty-cart";
import FreeProduct from "./free-product";
import { FormattedMessage } from "react-intl";
import { roundUpFloat } from "../../../../util/helpers/commonHelper";

const OrderSummary = ({
	order,
	currency,
	tax,
	sub_total,
	total,
	order_changed,
	cart_free_products,
  order_item_unaccepted_products,
	...props
}) => {

	return (
		<>
			<Row gutter={32}>
				{order && order.length > 0 ? (
					<>
						{order
							.filter((item) => !item.out_of_stock)
							.filter((item) => !item.blacklisted)
							.filter(item => item.sale_ok)
              .filter(item => !item.unaccepted_products)
							.map((item) => (
								<Col span={24} key={item.id}>
									<div className="item-container">
										<p className="label">{item.name}</p>
										<p className="cost">
											{item.price_changed ? (
												<>
													<span
														style={{ float: "left", textDecoration: "line-through" }}
													>
														{currency}{" "}
														{numberWithCommas(roundUpFloat(item.old_price, 2))}
													</span>
													<span style={{ float: "left", marginLeft: "10px" }}>
														{currency}{" "}
														{numberWithCommas(roundUpFloat(item.new_price, 2))}
													</span>
													<span style={{ marginLeft: "10px" }}>
														<img
															src={
																parseInt(item.new_price) > parseInt(item.old_price)
																	? PriceIncreasedLogo
																	: PriceDecreasedLogo
															}
															alt="price_change_img"
														/>
													</span>
												</>
											) : (
												<>
													{currency}{" "}
													{numberWithCommas(roundUpFloat(item.lst_price, 2))}
												</>
											)}
										</p>
										<div className="controls">
											<div className="input-group">
												<Button
													className="left-btn-controls"
													onClick={() => {
                            props.decrementItem(item.id)
                            props.checkCartPriceAgainstPricelist()
                          }}
												>
													-
												</Button>
												<Input
													name="quantity"
													className="input-qty"
													inputMode="numeric"
													pattern="[0-9]*"
													value={item.quantity}
													onChange={(e) => {
														let qty = parseInt(e.target.value) || 1;

														if (qty <= 0) qty = 1;

														props.addItem(item, "change_quantity", qty);
                            props.checkCartPriceAgainstPricelist()
													}}
												/>
												<Button
													className="right-btn-controls"
													onClick={() => {
                            props.incrementItem(item.id)
                            props.checkCartPriceAgainstPricelist()
                          }}
												>
													+
												</Button>
											</div>
											<img
												src={CartDeleteButton}
												className="delete-btn"
												alt="delete icon"
												onClick={() => props.removeItem(item.id)}
											/>
										</div>
									</div>
								</Col>
							))}
						{/* <AddFreeProduct 
							product={{name: "Coca cola"}}
						/> */}
						{(cart_free_products || [])
							.filter(it => it.sale_ok)
							.filter(it => !it.out_of_stock)
							.filter(it => !it.blacklisted)
              .filter(item => !item.unaccepted_products)
							.map(free_product => (
							<Fragment key={free_product?.odoo_id}>
								<FreeProduct
									product={free_product}
								/>
							</Fragment>
						))}
						<Col span={24}>
							<p className="totals">
								<strong><FormattedMessage id="TOTAL" /></strong>{" "}
								<span>
									{currency} { numberWithCommas(Math.ceil(total || 0.00))}
								</span>
							</p>
						</Col>
					</>
				) : (
					<Col span={24}>
						<EmptyCart />
					</Col>
				)}
			</Row>
		</>
	);
};

export default OrderSummary;
