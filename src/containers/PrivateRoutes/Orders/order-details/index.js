import { connect } from "react-redux";
import { Creators } from "../../../../services/redux/orders/actions";
import OrderDetails from "./components";

const mapStateToProps = (state) => {
  const { fetchingOrderDetails, details, orderDetailsError  } = state.orders;
  return {
    fetchingOrderDetails,
    details,
    orderDetailsError
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchOrderDetails: (id) => {
      dispatch(Creators.fetchOrderDetails(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetails);
