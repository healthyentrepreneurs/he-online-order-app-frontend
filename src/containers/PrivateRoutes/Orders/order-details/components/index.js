import React, { useEffect } from 'react';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { Button, Row, Col, Card, Spin } from 'antd';
import moment from 'moment';

import AppLayout from "../../../../../components/Layout/Layout";
import { history } from '../../../../../util/helpers/browserHistory';
import { getStatus } from '../../list-orders/components/list-item';
import "../../../../../styles/orders.less";


const getSpanValue = (index, dataLength) => {
    if (dataLength === 1) {
        return 24
    }

    if (dataLength === 2) {
        return 12
    }

    if (dataLength > 2 && dataLength-index === 1) {
        return 12;
    }
    return 12
}

const OrderDetails = props => {
    const orderId = props.match.params.id
    const data = props.details;
    const orderName = props.location.state.name;

    useEffect(() => {
        props.fetchOrderDetails(orderId)
        // eslint-disable-next-line
    }, [])

    const renderOrderDetails = () => {
        return (
            <React.Fragment>
                <Card className="he-order-details">
                <Row justify="end" gutter={2}>
                    <Col span={12} className="info">
                        <p className="detail-title">Order Name</p>
                        <p className="detail-info">{data.name}</p>
                    </Col>
                    <Col span={12} className="info">
                        <p className="detail-title">Order Date</p>
                        <p className="detail-info">{moment(data.created_at).format('Do MMMM, YYYY')}</p>
                    </Col>
                </Row>
                <Row justify="end" gutter={2}>
                    <Col span={12} className="info">
                        <p className="detail-title">Total Amount</p>
                        <p className="detail-info">{data.currency} {Number(data.total).toFixed(1).toLocaleString()}</p>
                    </Col>
                    <Col span={12} className="info">
                        <p className="detail-title">Order Status</p>
                        <p className="detail-info">{getStatus(data.order_status)}</p>
                    </Col>
                </Row>
                {data.payment_details ? (
                        <Row>
                            <Col span={12} align="left" className="info">
                                <p className="detail-title">Transaction Id</p>
                                <p className="detail-info">{data.payment_details.transaction_id}</p>
                            </Col>
                            <Col span={12} align="left" className="info">
                                <p className="detail-title">Payment Provider</p>
                                <p className="detail-info">{data.payment_details.payment_method?.carrier}</p>
                            </Col>
                            <Col span={12} align="left" className="info">
                                <p className="detail-title">Phone Number</p>
                                <p className="detail-info">{data.payment_details.phone_number}</p>
                            </Col>
                        </Row>
                ) : null}
            </Card>
            <div className="qty-divider" />
            <Row className="product-list-wrapper">
                {data.order_line_items && data.order_line_items.length && data.order_line_items.map((qty, index) => (
                    <Col span={getSpanValue(index, data.order_line_items.length)} key={qty.id} className="product-list-col">
                        <p className="product-name">{qty.name}</p>
                        <p className="product-price">{qty.currency_id[1]} {Number((qty.price_total).toFixed(1)).toLocaleString()}</p>
                        <p className="product-name">Quantity: {qty.product_uom_qty}</p>
                    </Col>
                ))}
            </Row>
            <Row className="product-list-wrapper" >
                <Col span={12} className="product-list-col">
                    <p style={{ paddingTop: "10px", fontWeight: "900" }}>Total</p>
                </Col>
                <Col span={12} className="product-list-col">
                    <p className="product-price" style={{ float: 'right', paddingTop: "10px" }}>
                        {data.currency} {Number((data.order_line_items.reduce((a, { price_total }) => a + price_total, 0)).toFixed(1)).toLocaleString()}
                    </p>
                </Col>
            </Row>
            <div className="qty-divider" />
            </React.Fragment>
        )
    }

    const { fetchingOrderDetails } = props
    return (
        <AppLayout title={`Order ID ${orderName}`}>
            <div className="order-details">
                <Button type="text" className="go-back" onClick={() => history.goBack()}>
                    <ArrowLeftOutlined style={{ fontWeight: "bolder", fontSize: "16px" }} />
                    <span className="text">Go back</span>
                </Button>
                {fetchingOrderDetails ? (
                    <div className="center-container">
                        <Spin size="large" tip="Loading..." />
                    </div>
                ) : data ? renderOrderDetails() : null}
                
            </div>
        </AppLayout>
    )
}

export default OrderDetails;
