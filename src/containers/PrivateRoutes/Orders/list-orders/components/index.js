import React, { Component } from "react";
import { Row, Empty, Spin } from "antd";
import { debounce } from "lodash";

import AppLayout from "../../../../../components/Layout/Layout";
import Filters from "./filters";
import ListItem from "./list-item";

import "../../../../../styles/orders.less";
import { FormattedMessage } from "react-intl";

class Orders extends Component {
  constructor(props) {
    super(props);

    // Sets up our initial state
    this.state = {
      hasMore: true,
      searchTerm: null,
      status: undefined,
      fromDate: undefined,
      endDate: undefined,
    };

    // Binds our scroll event handler
    window.onscroll = debounce(() => {
      const { errors, isloading, hasMore, nextPage, fetchOrders } = this.props;

      // Bails early if:
      // * there's an error
      // * it's already loading
      // * there's nothing left to load
      if (errors || isloading || !hasMore) return;

      // Checks that the page has scrolled to the bottom
      // if (
      //   window.innerHeight + document.documentElement.scrollTop ===
      //   document.scrollingElement.scrollHeight
      // ) {
      const node = document.documentElement;
      if (node.scrollHeight - node.scrollTop - 140 < node.clientHeight) {
        fetchOrders({
          page: nextPage,
          search_term: this.state.searchTerm,
          from_date: this.state.fromDate,
          end_date: this.state.endDate,
          status: this.state.status,
        });
      }
    }, 100);
  }

  componentDidMount() {
    const { fetchOrders } = this.props;
    fetchOrders();
  }

  render() {
    const { isloading, orders, hasMore, fetchOrders, resetPagination } = this.props;
    return (
      <AppLayout title={<FormattedMessage id="ALL_ORDERS" />} noFooter>
        <Filters
          handleChange={(fromDate, endDate, status, searchTerm) => {
            resetPagination();
            this.setState({ searchTerm, status, fromDate, endDate });
            fetchOrders({
              page: 1,
              search_term: searchTerm,
              from_date: fromDate,
              end_date: endDate,
              status,
            });
          }}
          {...this.props}
        />
        {isloading && !hasMore ? (
          <div className="center-container">
            <Spin size="large" tip={<><FormattedMessage id="LOADING" />...</>} />
          </div>
        ) : (
          <Row>
            {orders && orders.length > 0 ? (
              orders.map((order) => <ListItem key={order.order_id} {...order} />)
            ) : (
              <div className="center-container">
                <Empty description={<FormattedMessage id="NO_DATA" />} />
              </div>
            )}
          </Row>
        )}
        {isloading && hasMore ? (
          <div className="load-more">
            <Spin size="medium" tip="Loading More..." />
          </div>
        ) : null}
      </AppLayout>
    );
  }
}

export default Orders;
