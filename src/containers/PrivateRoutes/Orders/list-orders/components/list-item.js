import React from "react";
import { Col, Card } from "antd";
import { numberWithCommas } from "../../../../../util/helpers/reusableFunctions";
import Button from "../../../../../components/Button";

import ConfirmedIcon from "../../../../../assets/confirmed.svg";
import ToBeConfirmedIcon from "../../../../../assets/to-be-confirmed.svg";
import LeftWarehouseIcon from "../../../../../assets/left-warehouse.svg";
import moment from "moment";
import { history } from "../../../../../util/helpers/browserHistory";

export const getStatus = (status) => {
	let statusText = null;
	switch (status) {
		case "not confirmed":
			statusText = (
				<span style={{ color: "red", fontWeight: "bold" }}>
					<img
						src={ToBeConfirmedIcon}
						alt="to be confirmed"
						style={{ marginRight: "4px", marginTop: "-2px" }}
					/>
					To be confirmed
				</span>
			);
			break;
		case "confirmed":
			statusText = (
				<span style={{ color: "orange", fontWeight: "bold" }}>
					<img
						src={ConfirmedIcon}
						alt="confirmed"
						style={{ marginRight: "4px", marginTop: "-2px" }}
					/>
					Confirmed
				</span>
			);
			break;
		case "cancelled":
			statusText = (
				<span style={{ color: "red", fontWeight: "bold" }}>Cancelled</span>
			);
			break;
		case "delivered":
			statusText = (
				<span style={{ color: "#30994B", fontWeight: "bold" }}>
					<img
						src={LeftWarehouseIcon}
						alt="left warehouse"
						style={{ marginRight: "5px", marginTop: "-2px" }}
					/>
					Left warehouse
				</span>
			);
			break;
		case "not delivered":
			statusText = (
				<span style={{ color: "#BE090A", fontWeight: "bold" }}>Not Delivered</span>
			);
			break;
		default:
	}
	return statusText;
};

const ListItem = ({
	name,
	total = 0,
	currency,
	order_status,
	created_at,
	order_id,
}) => {
	return (
		<Col span={24} key={order_id}>
			<Card className="order-container">
				<div className="upper-content">
					<div>
						<span className="label">ORDER ID</span>
						<span className="text">{name}</span>
					</div>
					<div className="align-text-right">
						<span className="label">ORDER DATE</span>
						<span className="text">
							{moment(created_at).format("Do MMMM, YYYY")}
						</span>
					</div>
				</div>
				<div className="lower-content">
					<div>
						<span className="label">TOTAL AMOUNT</span>
						<span className="text">
							{currency} {numberWithCommas(parseInt(total).toFixed(0))}
						</span>
					</div>
					<div className="align-text-right">
						<span className="label">ORDER STATUS</span>
						<span className="text">{getStatus(order_status)}</span>
					</div>
				</div>
				<Button
					className="action-btn"
					text="View Order"
					onClick={() =>
						history.push(`/order/${order_id}`, {
							name,
						})
					}
				/>
			</Card>
		</Col>
	);
};

export default ListItem;
