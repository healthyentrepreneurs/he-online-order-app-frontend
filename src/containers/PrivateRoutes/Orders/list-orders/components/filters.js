import React, { useState } from "react";
import { Input, Select, Row, Col, DatePicker } from "antd";
import moment from "moment";

import SearchIcon from "../../../../../assets/search.svg";
import { FormattedMessage, useIntl } from "react-intl";

const { Option } = Select;

const Filters = ({ isloading, handleChange, ...props }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [start_date, setStartDate] = useState(undefined);
  const [end_date, setEndDate] = useState(undefined);
  const [status, setStatus] = useState(undefined);

  const intl = useIntl(); 

  const onChange = (e) => {
    const val = e.target.value;
    setSearchTerm(val);
    handleChange(start_date, end_date, status, val);
  };

  return (
    <>
      <div style={{ margin: "0 14px 0 14px" }} className="che-order-filters">
        <Row
          gutter={5}
          style={{ marginBottom: "10px", display: "flex", justifyContent: "center" }}
        >
          <Col>
            <button
              className="he-btn"
              onClick={() => {
                setSearchTerm("");
                setStartDate(undefined);
                setEndDate(undefined);
                setStatus(undefined);
                props.resetPagination();
                props.fetchOrders({ salt: Math.floor(Date.now() / 1000) });
              }}
            >
              <FormattedMessage id="REFRESH_ALL_ORDERS" />
            </button>
          </Col>
        </Row>
        <Row gutter={5} style={{ marginBottom: "10px" }}>
          <Col
            sm={3}
            xs={3}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h4 style={{ padding: "0", margin: "0" }}>Filter</h4>
          </Col>
          <Col sm={7} xs={7}>
            <Select
              allowClear={true}
              name="status"
              size="large"
              style={{ width: "100%" }}
              loading={isloading}
              showSearch
              className="dropdown"
              placeholder={(<><FormattedMessage id="STATUS"/></>)}
              optionFilterProp="children"
              value={status}
              onChange={(value) => {
                setStatus(value);
                handleChange(start_date, end_date, value, searchTerm);
              }}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              <Option value="confirmed"><FormattedMessage id="CONFIRMED" /></Option>
              <Option value="not confirmed"><FormattedMessage id="TO_BE_CONFIRMED" /></Option>
              <Option value="delivered"><FormattedMessage id="LEFT_WAREHOUSE" /></Option>
              <Option value="not delivered"><FormattedMessage id="NOT_DELIVERED" /></Option>
              <Option value="canceled"><FormattedMessage id="CANCELLED" /></Option>
            </Select>
          </Col>
          <Col sm={14} xs={14}>
            <Input
              name="search_term"
              size="large"
              style={{ width: "100%" }}
              placeholder={intl.formatMessage({ id: 'SEARCH' }) + "..."}
              onChange={onChange}
              value={searchTerm}
              autoComplete="off"
              allowClear={true}
              suffix={<img src={SearchIcon} alt="search" />}
            />
          </Col>
        </Row>
        <Row gutter={5} style={{ display: "flex", justifyContent: "space-between" }}>
          <Col sm={12} xs={12}>
            <DatePicker
              name="from_date"
              format="YYYY-MM-DD"
              size="large"
              className="filters-date-picker"
              style={{ width: "100%" }}
              value={start_date ? moment(start_date) : ""}
              onChange={(date, dateString) => {
                setStartDate(dateString);
                handleChange(dateString, end_date, status, searchTerm);
              }}
              placeholder={intl.formatMessage({ id: 'START_DATE' })}
              disabledDate={(current) => {
                // disable future dates
                const today = moment();
                return (
                  (current && current > moment(today, "YYYY-MM-DD")) ||
                  (current && current > moment(end_date, "YYYY-MM-DD"))
                );
              }}
            />
          </Col>
          <Col sm={12} xs={12}>
            <DatePicker
              name="from_date"
              format="YYYY-MM-DD"
              size="large"
              className="filters-date-picker"
              style={{ width: "100%" }}
              value={end_date ? moment(end_date) : ""}
              onChange={(date, dateString) => {
                setEndDate(dateString);
                handleChange(start_date, dateString, status, searchTerm);
              }}
              placeholder={intl.formatMessage({ id: 'END_DATE' })}
              disabledDate={(current) => {
                // disable future dates
                const today = moment();
                return (
                  (current && current > moment(today, "YYYY-MM-DD")) ||
                  (current && current < moment(start_date, "YYYY-MM-DD"))
                );
              }}
            />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Filters;
