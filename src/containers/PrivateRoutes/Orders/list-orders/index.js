import { connect } from "react-redux";
import { Creators } from "../../../../services/redux/orders/actions";
import Orders from "./components";

const mapStateToProps = (state) => {
  const { orders, isloading, errors, hasMore, nextPage } = state.orders;
  return {
    orders,
    isloading,
    errors,
    hasMore,
    nextPage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchOrders: (query = { page: 1 }) => {
      dispatch(Creators.fetchOrders(query));
    },
    resetPagination: () => {
      dispatch(Creators.resetPagination());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
