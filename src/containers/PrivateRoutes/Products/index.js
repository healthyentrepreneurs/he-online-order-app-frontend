import { connect } from "react-redux";
import { Creators } from "../../../services/redux/products/actions";
import { Creators as CartCreators } from "../../../services/redux/cart/actions";
import { Creators as AuthCreators } from "../../../services/redux/auth/actions";
import { Creators as AccountingCreators } from "../../../services/redux/accounting/actions"
import Products from "./components";

const mapStateToProps = (state) => {
  return {
    loading: state.products.fetchingProduct,
    data: state.products.products,
    hasMore: state.products.hasMoreProducts,
    fetchingMore: state.products.fetchingMore,
    error: state.products.error,
    fetchingCategory: state.products.fetchingCategory,
    category: state.products.categories,
    order: state.cart.order,
    toggle: state.products.toggle,
    syncingProducts: state.products.syncingProducts,
    syncProductsSuccess: state.products.syncProductsSuccess,
    user: state.auth.user,
    profileLastUpdated: state.auth.lastUpdated,
    credit_balance: state.accounting.credit_balance,
    credit_balance_loading: state.accounting.loading,
    paymentProcessing: state.payment.paymentProcessing,
    paymentProcessingTelco: state.payment.telco,
    productListPriceData: state.products.productListPriceData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProducts: (query) => {
      dispatch(Creators.getProductListRequest(query));
    },
    getUserDetailsWithSalt: (id) => {
      dispatch(AuthCreators.getUserDetailsWithSalt(id))
    },
    fetchCategory: () => {
      dispatch(Creators.getProductCategoryRequest());
    },
    addItem: (item, action, quantity = null) => {
      dispatch(CartCreators.addItem(item, action, quantity));
    },
    refresh: () => {
      dispatch(Creators.refresh());
    },
    cleanProduct: () => {
      dispatch(Creators.cleanProducts());
    },
    saveDeviceToken: registration_id => {
      dispatch(AuthCreators.saveDeviceToken(registration_id))
    },
    syncProducts: (query) => {
      dispatch(Creators.syncProducts(query));
    },
    setAccountingTab: tab => dispatch(AccountingCreators.setTab(tab)),
    refreshProductListCache: () => dispatch(Creators.refreshProductListCache()),
    cleanUnacceptedProductsFromCart: () => dispatch(CartCreators.cleanUnacceptedProductsFromCart())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
