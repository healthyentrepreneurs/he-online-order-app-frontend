import React from "react";
import { Row, Spin, Card, Button, Empty, Col } from "antd";
import _ from "lodash";
import Modal from "react-modal";

import AppLayout from "../../../../components/Layout/Layout";
import Category from "./category";
import ProductItem from "./product-item";
import Search from "./search";
import ProductButton from "./product-button";
import CloseModalIcon from "../../../../assets/Close_in_circle.svg";
import { numberWithCommas } from "../../../../util/helpers/reusableFunctions";
import { askForPermissionToReceiveNotification } from "../../../../firebase";
import moment from "moment";
import { PRODUCT_SALE_OK } from "../../../../services/api/products";
import PromotionBar from "./promotion-bar";
import CreditButton from "./credit-button";
// import PaymentProcessingBar from "./payment-processing-bar";
import { history } from "../../../../util/helpers/browserHistory";
// import initializePushNotification from "../../../../util/helpers/notificationHelpers";

class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expandSearch: true,
      searchValue: "",
      hasMore: false,
      products: [],
      firstProduct: false,
      modal: {
        visible: false,
        data: null,
      },
    };
    this.handleOnFocus = this.handleOnFocus.bind(this)
    this.handleOnBlur = this.handleOnBlur.bind(this)
  }

  async componentDidMount() {
    const { syncProductsSuccess, cleanProduct, user } = this.props
    cleanProduct()
    Modal.setAppElement("#root");
    let profile;
    try {
      profile = JSON.parse(localStorage.getItem("he-che-user"));
    } catch (e) {
      profile = {}
    }
    //this.props.syncProducts(`?qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`);
    if(!syncProductsSuccess && user?.country?.id) {
      this.props.syncProducts(`?qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`);
    }
    
    if (profile?.country?.id) {
      this.props.fetchProducts(`?qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`);
      this.props.fetchCategory();
    }
    
    const deviceToken = await askForPermissionToReceiveNotification();
    if (deviceToken) {
      this.props.saveDeviceToken(deviceToken);
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      nextProps &&
      nextProps.data &&
      nextProps.data.results &&
      !this.state.firstProduct
    ) {
      this.setState({ products: nextProps.data, firstProduct: true });
    }
  }

  componentDidUpdate(prevProps, nextProps) {
    window.onscroll = _.debounce(() => {
      const { hasMore, fetchingMore, error, location, loading } = this.props;
      if (error || fetchingMore || !hasMore || loading) return;
      const node = document.documentElement;
      if (
        node.scrollHeight - node.scrollTop - 140 < node.clientHeight &&
        location.pathname === "/dashboard"
      ) {
        const nextPage = this.props.data?.next?.split("?") || [];
        // this.props.fetchProducts(`?${nextPage[1]}&sale_ok=true`);
        this.props.fetchProducts(`?${nextPage[1]}`);
      }
    }, 100);
  }

  handleOnFocus = (e) => {
    this.setState({expandSearch: true})
  }

  handleOnBlur = (e) => {
    this.setState({expandSearch: false})
  }
  
  onSearch = (e) => {
    const profile = JSON.parse(localStorage.getItem("he-che-user"));
    this.props.refresh();
    this.props.cleanProduct();
    this.props.fetchProducts(
      `?search=${e.target.value}&qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`
    );
  };

  productData = () => {
    const { data, order } = this.props;
    if (data && data.results && data.results.length) {
      const results = data.results.reduce((acc, cur) => {
        let item = null;
        item = order.find((item) => item.id === cur.id);
        if (item) cur.quantity = item.quantity;
        else cur.quantity = 0;
        return (acc = [...acc, cur]);
      }, []);
      return results;
    }

    return [];
  };

  hasMoreProducts = () => {
    const { data } = this.props;
    if (data && data.next) {
      return true;
    }
  };

  showPromotionMessage = (promotionalMessage) => {
    const now = moment()
    const expiryDate = moment(promotionalMessage?.expiry_date)
    const result = now < expiryDate
    if(!promotionalMessage?.has_expiry && promotionalMessage?.message) {
      return true
    } else {
      return result && promotionalMessage?.message
    }
  }

  renderModal = () => {
    const customStyles = {
      content: {
        transform: "translate(-50%, -45%)",
        position: "absolute",
        top: "54%",
        left: "50%",
        padding: "0px",
        width: "18rem",
        height: "fit-content",
        zIndex: 50000,
      },
    };
    const { modal } = this.state;
    const image = modal.data.image ? modal.data.image : require("../../../../assets/default.png");
    return (
      <Modal
        isOpen={modal.visible}
        onRequestClose={() => this.setState({ modal: {} })}
        style={customStyles}
      >
        <Card
          cover={
            <div style={{width: '100%', height: 'auto', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
              <img
                src={image}
                preview={false}
                alt={modal.data.name}
                style={{
                  height: "9rem",
                  width: "9rem"
                }}
              />
            </div>
          }
          className="product-modal"
        >
          <Button
            className="icon-close"
            icon={<img src={CloseModalIcon} alt="close icon" />}
            onClick={() => this.setState({ modal: {} })}
          />
          <p className="item-name">{modal.data.name}</p>
          <p className="item-price">
            <strong>
              {modal.data.currency.name}{" "}
              {modal.data.lst_price &&
                numberWithCommas(Number(modal.data.lst_price).toFixed(0))}
            </strong>
          </p>
          <div className="product-description" style={{marginBottom: 5, maxHeight: '6rem', overflowY: "auto"}}>
            <p>{modal.data.description }</p>
          </div>
          <ProductButton
            addItem={this.props.addItem}
            refresh={this.props.refresh}
            item={modal.data}
          />
        </Card>
      </Modal>
    );
  };

  render() {
    //const userProfile = JSON.parse(localStorage.getItem("he-che-user"));
    const {
      fetchingCategory,
      category,
      user,
      fetchProducts,
      loading,
      refresh,
      addItem,
      cleanProduct
    } = this.props;
    return (
      <AppLayout
        showNotificationBar={false}
        message={user?.promotional_message?.message}
        noFooter
        category={
          <Category
            loading={fetchingCategory}
            data={category}
            fetchCategoryProducts={fetchProducts}
            cleanProduct={cleanProduct}
          />
        }
      >
        <PromotionBar
          message={user?.promotional_message?.message}
          hasExpiry={user?.promotional_message?.has_expiry}
          expiryDate = {user?.promotional_message?.expiry_date}
        />
        {/* <PaymentProcessingBar 
          message="We are still processing your payment with MTN"
        /> */}
        {(!this.state.expandSearch && user?.credit_balance) ? (
          <Row>
            <Col span={24} className="search-bar-wrapper">
              <div className="search-button-wrapper">
                <Search
                  onFocus={this.handleOnFocus}
                  onBlur={this.handleOnBlur}
                  handleChange={(val) => {
                    const profile = JSON.parse(localStorage.getItem("he-che-user"));
                    this.props.fetchProducts(
                      `?search=${val}&qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`
                    );
                  }}
                  cleanProduct={cleanProduct}
                />
              </div>
              <div className="credit-button-wrapper">
                <CreditButton 
                  onClick={e => {
                    history.push("/credit-invoices")
                  }}
                  credit_balance={numberWithCommas(user?.credit_balance)}
                  currency={user?.country?.currency}
                  lastUpdated={moment(this.props.profileLastUpdated).format("DD/MM/YYYY h:mm")}
                />
              </div>
            </Col>
          </Row>
        ) : null}
        <Row style={{padding: 10}}>
          <Col span={24}>
            {(!user?.credit_balance || this.state.expandSearch) ? (
            <div className="search-bar-wrapper-full">
              <Search
                onFocus={this.handleOnFocus}
                onBlur={this.handleOnBlur}
                handleChange={(val) => {
                  const profile = JSON.parse(localStorage.getItem("he-che-user"));
                  this.props.fetchProducts(
                    `?search=${val}&qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`
                  );
                }}
                cleanProduct={cleanProduct}
              />
            </div>
          ) : null}
          <div className="he-products">
            {this.productData().map((data) => (
              <ProductItem
                {...data}
                key={data.id}
                onClick={() => this.setState({ modal: { visible: true, data } })}
                addItem={addItem}
                refresh={refresh}
                item={data}
              />
              ))
            }
          </div>
          {loading ? (
            <div className="load-more">
              <Spin size="large" tip="Loading..." />
            </div>
          ) : (
            <Row>
              {!this.productData().length ? (
                <div className="center-container">
                  <Empty description={<FormattedMessage id="NO_DATA" />} />
                </div>
              ) : null}
            </Row>
          )}
          {this.state.modal.visible && this.renderModal()}
          </Col>
        </Row>
      </AppLayout>
    );
  }
}

export default Products;
