import React from "react";
import { Button } from "antd";

import "../../../../styles/products.less";
import TooltipIcon from "../../../../assets/Tooltip.svg";
import ProductButton from "./product-button";
import { numberWithCommas } from "../../../../util/helpers/reusableFunctions";
import { PictureFilled } from "@ant-design/icons";
import { IMGAE_PREFIX } from "../../../../util/configs";
import { roundUpFloat } from "../../../../util/helpers/commonHelper";

const List = (props) => {
  //const image = props.image ? props.image : require("../../../../assets/default.png")
  //const image = require("../../../../assets/default.png")
  return (
    <div className="product-item">
      <div className="product-item-top-container">
        <div className="item-image">
          <div style={{ width: '100%', height: '10rem', display: 'flex', justifyContent:'center', alignItems: 'center', position:'relative' }}>
            {props.image ? (
              <img
                className="item-image-img"
                src={`${props?.image}?${IMGAE_PREFIX}`}
                alt={props.name}
              />
            ) : (
              <PictureFilled style={{ fontSize: 120, color: '#b1b3b5' }}/>
            )}
            {/* <img
              className="item-image-img"
              src={props?.image}
              alt={props.name}
            /> */}
          </div>
          <Button
            icon={<img src={TooltipIcon} alt="tooltip" style={{width: 20, height: 20}}/>}
            className="tooltip-btn"
            onClick={props.onClick}
          />
        </div>
        <div className="item-info">
          <h4 className="title">{props.name}</h4>
          <p className="price">
            {props.currency.name}{" "}
            {props.lst_price && numberWithCommas(roundUpFloat(props.lst_price, 2))}
          </p>
        </div>
      </div>
      <ProductButton {...props} />
    </div>
  );
};

export default List;
