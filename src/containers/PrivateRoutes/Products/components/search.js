import React, { useState, forwardRef } from "react";
import { Input } from "antd";

import SearchIcon from "../../../../assets/search.svg";
import "../../../../styles/products.less";
import { useIntl } from "react-intl";



const Search = forwardRef((props, ref) => {
  const [searchTerm, setSearchTerm] = useState("");
  const intl = useIntl()

  const onChange = (e) => {
    const val = e.target.value;
    setSearchTerm(val);
    // props.cleanProduct();
    props.handleChange(val);
  };
  return (
    <div className="search-wrapper">
      <Input
        ref={ref}
        onBlur={props.onBlur}
        onFocus={props.onFocus}
        placeholder={intl.formatMessage({ id: 'SEARCH' }) + "..."}
        suffix={<img src={SearchIcon} alt="search" />}
        className="input"
        onChange={onChange}
        value={searchTerm}
      />
    </div>
  );
});

export default Search;
