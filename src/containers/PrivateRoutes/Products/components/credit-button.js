import React from 'react'
import PropTypes from "prop-types"
import { FormattedMessage } from "react-intl";


const CreditButton = ({credit_balance, currency, onClick, lastUpdated, openedPayments}) => {
  return (
    <div className='credit-btn-wrapper' onClick={e => onClick()}>
      <div className='top-wrapper'>
        <span className="credit-btn-label"><FormattedMessage id="CREDIT_BALANCE" />:</span>
        <span className='credit-btn-value'>{credit_balance} {currency}</span>
      </div>
      <div className='middle-wrapper'>
        <span className="credit-btn-label"><FormattedMessage id="OPEN_PAYMENTS" />:</span>
        <span className='credit-btn-value'>{openedPayments || 0}</span>
      </div>
      <div className='bottom-wrapper'>
        <span className='credit-btn-label'><FormattedMessage id="UPDATED"/>: {lastUpdated}</span>
      </div>
    </div>
  )
}

CreditButton.propTypes = {
  credit_balance: PropTypes.any,
  lastUpdated: PropTypes.string,
  currency: PropTypes.string,
  onClick: PropTypes.func,
  openedPayments: PropTypes.number
}

export default CreditButton;