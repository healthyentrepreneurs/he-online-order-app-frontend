import { Card, Button, Row, Col, Spin, Empty } from 'antd';
import moment from 'moment';
import React, { useEffect, useState } from 'react'
import AppLayout from "../../../../components/Layout/Layout"
import { askForPermissionToReceiveNotification } from '../../../../firebase';
import Search from "./search";
import { PRODUCT_SALE_OK } from '../../../../services/api/products';
import { numberWithCommas } from '../../../../util/helpers/reusableFunctions';
import Category from './category';
import ProductButton from "./product-button";
import CloseModalIcon from "../../../../assets/Close_in_circle.svg";
import PromotionBar from "./promotion-bar";
import PaymentProcessingBar from "../../../../reusable-components/PaymentProcessingBar"
import CreditButton from './credit-button';
import ProductItem from "./product-item";
import { history } from "../../../../util/helpers/browserHistory";
import { debounce } from 'lodash';
import Modal from "react-modal";
import { PictureFilled } from '@ant-design/icons';
import { IMGAE_PREFIX } from '../../../../util/configs';
import { FormattedMessage } from 'react-intl';


const Products = (props) => {
  //#################Props#########################
  const {
    fetchingCategory,
    category,
    user,
    fetchProducts,
    fetchCategory,
    loading,
    refresh,
    addItem,
    cleanProduct,
    saveDeviceToken,
    syncProductsSuccess,
    profileLastUpdated,
    paymentProcessing,
    cleanUnacceptedProductsFromCart
  } = props;
  

  //###################Local States##############################
  const [searchFocus, setSearchFocus] = useState(false)
  //const [searchValue, setSearchValue] = useState("")
  // const [hasMore, setHasMore] = useState(false)
  // const [products, setProducts] = useState([])
  // const [firstProduct, setFirstProduct] = useState(false)
  const [modal, setModal] = useState({visible: false, data: null})


  //*****************Events***************** */
  const handleOnFocus = (e) => {
    setSearchFocus(true)
  }

  const handleOnBlur = (e) => {
    setSearchFocus(false)
  }

  const handleScroll = debounce(() => {
    const { hasMore, fetchingMore, error, loading } = props;
    if (error || fetchingMore || !hasMore || loading) return;
    const node = document.documentElement;
    if (
      node.scrollHeight - node.scrollTop - 140 < node.clientHeight
    ) {
      const nextPage = props.data?.next?.split("?") || [];
      fetchProducts(`?${nextPage[1]}`);
    }
  }, 100)


  //***************Helper functions************* */
  const setDeviceToken = async() => {
    const deviceToken = await askForPermissionToReceiveNotification();
    if (deviceToken) {
      saveDeviceToken(deviceToken);
    } 
  }

  const productData = () => {
    const { data, order } = props;
    if (data && data.results && data.results.length) {
      const results = data.results.reduce((acc, cur) => {
        let item = null;
        item = order.find((item) => item.id === cur.id);
        if (item) cur.quantity = item.quantity;
        else cur.quantity = 0;
        return (acc = [...acc, cur]);
      }, []);
      return results;
    }

    return [];
  };

  const handleSearchTerChange = debounce((value) => {
    props.cleanProduct();
    const profile = JSON.parse(localStorage.getItem("he-che-user"));
    fetchProducts(
      `?search=${value}&qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`
    );
  }, 1000)

  const renderModal = () => {
    const customStyles = {
      content: {
        transform: "translate(-50%, -45%)",
        position: "absolute",
        top: "54%",
        left: "50%",
        padding: "0px",
        width: "18rem",
        height: "fit-content",
        zIndex: 50000,
      },
    };
    // const image = modal.data.image ? modal.data.image : require("../../../../assets/default.png");
    const image = modal.data.image;
    return (
      <Modal
        isOpen={modal.visible}
        onRequestClose={() => setModal({ visible: false, data: null })}
        style={customStyles}
      >
        <Card
          cover={
            <div style={{width: '100%', height: 'auto', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
              {image ? (
                <img
                src={`${image}?${IMGAE_PREFIX}`}
                preview={false}
                alt={modal.data.name}
                style={{
                  height: "9rem",
                  width: "9rem"
                }}
              />
              ) : (
                <PictureFilled style={{ fontSize: 120, color: '#b1b3b5' }}/>
              )}
            </div>
          }
          className="product-modal"
        >
          <Button
            className="icon-close"
            icon={<img src={CloseModalIcon} alt="close icon" />}
            onClick={() => setModal({ visible: false, data: null })}
          />
          <p className="item-name">{modal.data.name}</p>
          <p className="item-price">
            <strong>
              {modal.data.currency.name}{" "}
              {modal.data.lst_price &&
                numberWithCommas(Number(modal.data.lst_price).toFixed(0))}
            </strong>
          </p>
          <div className="product-description" style={{marginBottom: 5, maxHeight: '6rem', overflowY: "auto"}}>
          <p dangerouslySetInnerHTML={{ __html: modal.data.description  }} />
          </div>
          <ProductButton
            addItem={props.addItem}
            refresh={props.refresh}
            item={modal.data}
          />
        </Card>
      </Modal>
    );
  };

  //*************************************Effects*********************************************** */

  useEffect(() => {
    cleanUnacceptedProductsFromCart()
    setSearchFocus(false)
    if(localStorage.getItem("he-che-user")) {
      const user = localStorage.getItem("he-che-user") || {}
      props.getUserDetailsWithSalt(user?.id)
    }
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    Modal.setAppElement("#root");
    if(props?.data?.results?.length > 0) {
      return;
    }
    let profile;
    try {
      profile = JSON.parse(localStorage.getItem("he-che-user"));
    } catch (e) {
      profile = {}
    }
    //props.syncProducts(`?qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`);
    if(!syncProductsSuccess && user?.country?.id && props.data.length < 1) {
      props.syncProducts(`?qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`);
    }
    
    if (user?.country?.id && !props?.data?.results) {
      cleanProduct()
      fetchCategory();
      fetchProducts(`?qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`);
    }
    // if(user?.credit_balance) {
    //   setExpandSearch(false)
    // }
    setDeviceToken()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user?.id])

  useEffect(() => {
    window.addEventListener("scroll", handleScroll)
    return () => {
      window.removeEventListener("scroll", handleScroll)
    }
  })

  return (
    <>
      <AppLayout
        showNotificationBar={false}
        message={user?.promotional_message?.message}
        noFooter
        category={
          <Category
            loading={fetchingCategory}
            data={category}
            fetchCategoryProducts={fetchProducts}
            cleanProduct={cleanProduct}
          />
        }
      >
        {!paymentProcessing ? (
          <PromotionBar
            message={user?.promotional_message?.message}
            hasExpiry={user?.promotional_message?.has_expiry}
            expiryDate = {user?.promotional_message?.expiry_date}
          />
        ): (
          <PaymentProcessingBar
            provider={props.paymentProcessingTelco}
          />
        )}

        {/* <Row>
          <Col span={24}>
            <ProductHeaderBar
              handleChange={(val) => {
                const profile = JSON.parse(localStorage.getItem("he-che-user"));
                fetchProducts(
                  `?search=${val}&qty_available__gt=${profile?.country?.minimum_threshold}&sale_ok=${PRODUCT_SALE_OK}&limit=20`
                );
              }}
              cleanProduct={cleanProduct}
            />
          </Col>
        </Row> */}
        
        <Row>
          <Col span={24}>
            {searchFocus ? (
              <div className='search-bar-wrapper-full'>
                <Search
                  onFocus={handleOnFocus}
                  onBlur={handleOnBlur}
                  handleChange={(val) => {
                    handleSearchTerChange(val)
                  }}
                  cleanProduct={cleanProduct}
                />
              </div>
            ) : (
              <div className='search-bar-wrapper'>
                <div className="search-input-wrapper">
                  {/* <img 
                    style={{cursor: "pointer"}}
                    src="images/magnifier.png" width={40} height={40} 
                    onClick={e => {
                      handleSearchButtonClick(e)
                    }}
                  /> */}
                  <Search 
                    onFocus={handleOnFocus}
                    onBlur={handleOnBlur}
                  />
                </div>
                <div className="credit-button-wrapper">
                  <CreditButton
                    onClick={e => {
                      props.setAccountingTab(1)
                      history.push("/credit-invoices")
                    }}
                    credit_balance={numberWithCommas(user?.credit_balance)}
                    currency={user?.country?.currency}
                    lastUpdated={moment(profileLastUpdated).format("DD/MM/YYYY-hh:mm A")}
                    openedPayments={user?.open_invoices_count || 0}
                  />
                </div>
              </div>
            )}
          </Col>
        </Row>
        <Row style={{padding: "0px 10px 0px 10px"}}>
          <Col span={24}>
            <div className="he-products">
              {productData().map((data) => (
                <ProductItem
                  {...data}
                  key={data.id}
                  onClick={() => setModal({ visible: true, data })}
                  addItem={addItem}
                  refresh={refresh}
                  item={data}
                />
                ))
              }
            </div>
            {loading ? (
              <div className="load-more">
                <Spin size="large" tip={<><FormattedMessage id="LOADING" />...</>} />
              </div>
            ) : (
              <Row>
                {!productData().length ? (
                  <div className="center-container">
                    <Empty description={<FormattedMessage id="NO_DATA" />} />
                  </div>
                ) : null}
              </Row>
            )}
            {modal.visible && renderModal()}
          </Col>
        </Row>
      </AppLayout>
    </>
  )
}

export default Products