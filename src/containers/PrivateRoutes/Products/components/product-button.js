import React from "react";
import { Button, Input } from "antd";

const ProductButton = (props) => {
	return (
		<div className="product-button">
			<Button
				onClick={(e) => {
					e.preventDefault();
					props.addItem({...props.item}, "decrement");
					props.refresh();
				}}
				className="decrement"
			>
				-
			</Button>
			<Input
				name="quantity"
				inputMode="numeric"
				pattern="[0-9]*"
				value={(props.item && props.item.quantity) || 0}
				onChange={(e) => {
					let qty = parseInt(e.target.value) || 0;
					if (qty < 0) qty = 0;

					props.addItem({...props.item}, "change_quantity", qty);
					props.refresh();
				}}
			/>
			<Button
				onClick={(e) => {
					e.preventDefault();
					props.addItem({...props.item}, "increment");
					props.refresh();
				}}
				className="increment"
			>
				+
			</Button>
		</div>
	);
};

export default ProductButton;
