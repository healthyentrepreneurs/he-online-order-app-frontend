import React, { useState } from 'react';
import { Button, Spin } from 'antd';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';

import '../../../../styles/products.less';
import { PRODUCT_SALE_OK } from '../../../../services/api/products';
import { FormattedMessage } from 'react-intl';


const Category = props => {
    const [category, setCategory] = useState(1);
    const scrollStep = 200;
    const scrollContent = document.getElementById('paginated_gallery')

    const handleScrollPrevious = e => {
        e.preventDefault();
        let sl = scrollContent.scrollLeft;
        if ((sl - scrollStep) <= 0) {
            scrollContent.scrollTo(0, 0);
        } else {
            scrollContent.scrollTo((sl - scrollStep), 0);
        }
    }

    const handleScrollNext = e => {
        let sl = scrollContent.scrollLeft,
        cw = scrollContent.scrollWidth;
        if ((sl + scrollStep) >= cw) {
            scrollContent.scrollTo(cw, 0);
        } else {
            scrollContent.scrollTo((sl + scrollStep), 0);
        }
    }
    return (
        <div className="category-wrapper">
            {props.loading && <Spin tip={<><FormattedMessage id="LOADING" />...</>} />}
            <div className="category-container">
                <Button
                    className="cat-action-btn cat-prev"
                    icon={<LeftOutlined />}
                    onClick={handleScrollPrevious}
                />
                <div className="category-list" id="paginated_gallery">    
                    <Button
                        className={`cat-btn ${1 === category ? 'active' : ''}`}
                        onClick={() => {
                            setCategory(1);
                            props.cleanProduct();
                            props.fetchCategoryProducts(`?qty_available__gt=${props.user?.country?.minimum_threshold || 15}&sale_ok=${PRODUCT_SALE_OK}&limit=20`)}
                        }
                    >
                        <FormattedMessage id="ALL" />
                    </Button>
                    {props.data && props.data.results && props.data.results.map(data => {
                        return (
                            <Button
                                className={`cat-btn ${data.odoo_id === category ? 'active' : ''}`}
                                key={data.id}
                                onClick={() => {
                                    setCategory(data.odoo_id);
                                    props.cleanProduct();
                                    props.fetchCategoryProducts(`?categ_id__id=${data.odoo_id}&sale_ok=${PRODUCT_SALE_OK}&qty_available__gt=${props.user?.country?.minimum_threshold || 15}`)}
                                    }
                            >
                                {data.name}
                            </Button>
                        )
                    })}
                </div>
                <Button
                    className="cat-action-btn cat-next"
                    icon={<RightOutlined />}
                    onClick={handleScrollNext}
                />
            </div>
        </div>
    )
}

export default Category;
