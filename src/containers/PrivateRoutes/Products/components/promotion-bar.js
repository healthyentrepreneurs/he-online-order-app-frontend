import React from 'react'
import { Row, Col } from "antd"
import PropTypes from "prop-types"
import { NotificationFilled } from '@ant-design/icons'
import moment from 'moment'

const PromotionBar = (props) => {
  const {
    message,
    hasExpiry,
    expiryDate
  } = props

  const [expiryDateValid, setExpiryDateValid] = React.useState(true)
  const [showNotification, setShowNotification] = React.useState(false)


  const isExpiryDateValid = (expiry_date) => {
    const now = moment()
    const expiryDate = moment(expiry_date)
    const result = now < expiryDate
    return result
  }

  const isShowNotification = (expiryDate, hasExpiry, message) => {
    if(!message) {
      return false;
    } else if(!hasExpiry && message) {
      return true
    } else {
      return isExpiryDateValid(expiryDate) && expiryDateValid;
    }
  }

  React.useEffect(() => {
    let setTimer = setInterval(() => {
      const isDateValid = isExpiryDateValid(expiryDate)
      if(!hasExpiry) {
        setExpiryDateValid(isDateValid)
      }
    }, 5000);
    return () => {
      clearInterval(setTimer)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  React.useEffect(()=> {
    const isShow = isShowNotification(expiryDate, hasExpiry, message)
    setShowNotification(isShow)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hasExpiry, expiryDate, message])

  return (
    <>
    {showNotification && (
      <Row style={{padding: 5, minHeight: 50, marginTop: "-15px", marginBottom: "15px"}} className="notification-bar">
        <Col span={4} style={{display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
          <NotificationFilled />
        </Col>
        <Col span={20} style={{display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "flex-start"}}>
          <span style={{fontSize: 12}}>{message}</span>
        </Col>
      </Row>
    )}
    </>
  )
}

PromotionBar.propTypes = {
  message: PropTypes.string,
  hasExpiry: PropTypes.bool,
  expiryDate: PropTypes.string
}

export default PromotionBar