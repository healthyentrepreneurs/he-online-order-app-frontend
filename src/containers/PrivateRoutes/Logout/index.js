import { connect } from "react-redux";
//import { history } from "../../../util/helpers/browserHistory";
import { Creators } from "../../../services/redux/auth/actions";
import { Creators as CartCreators } from "../../../services/redux/cart/actions";
import storage from "redux-persist/lib/storage";
import { Redirect } from "react-router-dom";
import React, { useEffect } from 'react'

const Logout = (props) => {
	const phone_number = localStorage.getItem('phone_number');

  const removePersistRoot = async () => {
    console.log("lets remove persist:root")
    localStorage.removeItem("persist:root")
    await storage.removeItem("persist:root");
  }

  const clearApiCache = async () => {
    await caches.delete('endpoints-cache')
    await caches.delete('images-cache')
  }

  useEffect(() => {
    props.logout();
    localStorage.clear();
    removePersistRoot()
    localStorage.removeItem("employee_ordering_for")
    clearApiCache()
    props.resetOrder()
    if (phone_number) localStorage.setItem('phone_number', phone_number)
    
    
    //history.replace("/login");
    //window.location.href = "/login";
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
	
	return <Redirect to="/login" />;
};

const mapDispatchToProps = (dispatch) => {
	return {
		logout: () => {
			dispatch(Creators.logout());
		},
		resetOrder: () => {
			dispatch(CartCreators.resetOrder());
		},
	};
};

export default connect(null, mapDispatchToProps)(Logout);
