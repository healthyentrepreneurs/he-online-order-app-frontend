import { connect } from 'react-redux';
import { Creators } from '../../services/redux/auth/actions';
import SetPin from './components/SetPin';

const mapStateToProps = state => {
  return {
    settingPin: state.auth.settingPin
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setPin: (pin, key) => {
      dispatch(Creators.setPinRequest(pin, key))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SetPin);
