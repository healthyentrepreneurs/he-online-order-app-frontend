import { connect } from "react-redux";
import { Creators } from "../../services/redux/auth/actions";
import TokenExpireRedirect from "./components/TokenExpireRedirect";

const mapStateToProps = (state) => {
	return {
    refreshingExpiredToken: state.auth.refreshingExpiredToken
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
    refreshExpiredToken: (refreshToken) => {
      dispatch(Creators.refreshExpiredToken(refreshToken))
    }
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(TokenExpireRedirect);