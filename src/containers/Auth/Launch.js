import { connect } from "react-redux";
import { Creators } from "../../services/redux/auth/actions";
import Launch from "./components/Launch";

const mapStateToProps = (state) => {
	return {
		isAuthenticating: state.auth.isAuthenticating,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		launch: (phone_number) => {
			dispatch(Creators.launchRequest(phone_number));
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Launch);
