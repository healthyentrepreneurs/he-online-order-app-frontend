import { Spin } from 'antd'
import React, { useEffect } from 'react'
import { Redirect } from "react-router-dom"

const LaunchRefresh = (props) => {
  const {
    isRefreshing,
    launch
  } = props
  useEffect(() => {
    const phoneNumber = localStorage.getItem("phone_number")
    if(phoneNumber) {
      launch(phoneNumber)
    } else {
      return <Redirect to="/dashboard" />
    }
    //eslint-disable-next-line
  }, [isRefreshing])

  return (
    <>
      <div className="load-more">
        <Spin size="large" tip="Refreshing token..." />
      </div>
    </>
  )
}

export default LaunchRefresh