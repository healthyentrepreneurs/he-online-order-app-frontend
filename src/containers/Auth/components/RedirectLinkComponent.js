import { Col, Row } from 'antd'
import React from 'react'
import { history } from '../../../util/helpers/browserHistory'
import ActivateQrCodeRedirectBtn from './ActivateQrCodeRedirectBtn'
import LoginRedirectBtn from './LoginRedirectBtn'

const RedirectLinkComponent = (props) => {
  const { onQrcodeBtnClick } = props
  const onRedirectToLogin = () => {
    history.push('/login')
  }

  const onActivateByQrCode = () => {
    onQrcodeBtnClick()
  }
  return (
    <Row>
      <Col
        span={12} 
        style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}
      >
        <LoginRedirectBtn onClick={() => onRedirectToLogin()} />
      </Col>
      <Col
        span={12}
        style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }} 
      >
        <ActivateQrCodeRedirectBtn onClick={() => onActivateByQrCode()}/>
      </Col>
    </Row>
  )
}

export default RedirectLinkComponent