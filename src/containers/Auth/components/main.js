import React from 'react';
import { Col, Form, Row } from 'antd';
import '../../../styles/auth.less';
import { InputField } from '../../../components/InputField';
import Button from '../../../components/Button';
import AppLayout from '../../../components/Layout/Layout';
import { history } from '../../../util/helpers/browserHistory';

const AuthMainWrapper = (props) => {
  const { redirectLinkComponent, suffixElement = null } = props
  const [form] = Form.useForm();
  return (
    <AppLayout authHeader={true} title={props.title} noFooter={true}>
      <h4 className="auth-title">{props.title}</h4>
      <div className="auth-content">
        <div className="form-wrappers">
          <Form
            name="basic"
            initialValues={props.initialValues}
            form={form}
            onFinish={props.onFinish}
            onFinishFailed={props.onFinishFailed}
          >
            {props.forms.map((form, index) => {
              return (
                <InputField
                  key={`${form.label}${index}`}
                  label={form.label}
                  placeholder={form.placeholder}
                  name={form.name}
                  type={form.type || 'text'}
                  rules={form.rules}
                  inputmode={form.inputmode}
                  pattern={form.pattern}
                  defaultValue={form.defaultValue}
                  disabled={form.disabled}
                  suffix={form?.suffix && suffixElement && suffixElement}
                />
              )
            })}
            {props.authRedirectLink && props.authRedirectText && (
              <Form.Item>
                <span
                  className="redirect-link"
                  onClick={() => history.push(props.authRedirectLink)}
                >
                  {props.authRedirectText}
                </span>
              </Form.Item>
            )}
            <Form.Item>
              <Button
                text={props.submitText}
                htmlType="submit"
                loading={props.loading}
              />
            </Form.Item>
          </Form>
          {redirectLinkComponent && (
            <Row style={{ marginTop: 40 }}>
              <Col span={24}>
                {redirectLinkComponent}
              </Col>
          </Row>
          )}
        </div>
      </div>
    </AppLayout>
  )
}

export default AuthMainWrapper;