import { LockOutlined } from '@ant-design/icons'
import React from 'react'


const LoginRedirectBtn = (props) => {
  const { onClick } = props

  return (
    <div 
      onClick={() => onClick()}
      style={{ width: 100, height: 100, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center',
        borderRadius: 20, backgroundColor: '#ffffff', border: '1px #b9babd solid', padding: 5, color: '#b9babd'
      }}
    >
      <LockOutlined />
        <span style={{ textAlign: 'center' }}>
          Go to login screen
        </span>
    </div>
  )
}

export default LoginRedirectBtn