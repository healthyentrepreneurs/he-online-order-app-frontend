import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { checkAuth } from '../../../util/helpers/requireAuthentication';
import AuthMainWrapper from './main';
import RedirectLinkComponent from './RedirectLinkComponent';
import { message, Modal, Spin } from 'antd'
import QrcodeReader from './QrcodeReader';
// import { excludeFromMaintenanceScreen } from '../../../util/helpers/commonHelper';

const ActivateLogin = props => {
  const [qrcodeScanVisible, setQrcodeVisible] = useState(false)

  const handleCloseScan = () => {
    setQrcodeVisible(false)
  }
  
  
  const onFinish = (values) => {
    localStorage.setItem('phone_number', values.phone_number);
    props.activateLogin(values.phone_number, values.password);
  }

  const onFinishFailed = (error) => {

  }

  const processQrcodeResult = (data) => {
    console.log('data', data)
    setQrcodeVisible(false)
    const scanText = data?.text
    
    let authCode = scanText.split('?auth_code=')[1] || null
    let uid = scanText.split('/')[4] || null
    props.setActivateLoginKey(authCode)
    if(authCode && uid) {
      props.activateUserByQrcode({authCode, uid})
    } else {
      message.error('Invalid QR-Code')
    }
  }

  // if(!excludeFromMaintenanceScreen(localStorage.getItem("phone_number"))) {
  //   return <Redirect to="/maintenance" />
  // }
  if (checkAuth()) {
    return <Redirect to="/dashboard" />;
  } 
  // else {
  //   const phoneNumber = localStorage.getItem("phone_number")
  //   const currentPathname = window.location.pathname
  //   if(phoneNumber && currentPathname !== '/') {
  //     return <Redirect to="/launch-refresh" />
  //   }
  // }

  // useEffect(() => {
  //   if (checkAuth()) {
  //     console.log('user authenticated')
  //     return <Redirect to="/dashboard" />;
  //   } else {
  //     console.log('user not authenticated')
  //   }
  // })

  // useEffect(()=> {
  //   if(props.activateQrcodeSuccess && !props.activatingQrcode) {
  //     console.log('activate success lets go-----')
  //     console.log('key', props.activateLoginkey)
  //     if(props.activateLoginkey) {
  //       history.push("/set-pin", { key: props.activateLoginkey });
  //     }
  //   }
  // }, [props.activatingQrcode, props.activateQrcodeSuccess])

  return(
    <>
      <AuthMainWrapper
        title="Activate Account"
        redirectLinkComponent={
          <RedirectLinkComponent
            onQrcodeBtnClick ={() => {
              setQrcodeVisible(true)
            }}
          />
        }
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        submitText="Activate Account"
        loading={props.activatingLogin}
        forms={[
          {
            label: 'Phone Number',
            placeholder: 'Enter your phone number',
            name:'phone_number'
          },
          {
            label: 'Enter the auto-generated password',
            placeholder: 'Enter the password given to you',
            name:'password',
            type: 'password'
          }
        ]}
      />
      <Modal
        visible={qrcodeScanVisible}
        title='Read QR-code'
        footer={null}
        onCancel={() => setQrcodeVisible(false)}
      >
        {props.activatingQrcode ? <Spin/> : (
          <QrcodeReader
            onClose={() => handleCloseScan()}
            visible={qrcodeScanVisible}
            onSubmit={(data) => {
              processQrcodeResult(data)
            }}
            loading={props.activatingQrcode}
          />
        )}
      </Modal>
    </>
  );
}

export default ActivateLogin;
