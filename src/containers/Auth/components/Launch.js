import React from 'react';
import { Redirect } from 'react-router-dom';
import { checkAuth } from '../../../util/helpers/requireAuthentication';

import AuthMainWrapper from './main';

const ActivateLaunch = props => {
  const onFinish = (values) => {
    localStorage.setItem('phone_number', values.phone_number)
    props.launch(values.phone_number);
  }

  const onFinishFailed = (error) => {
  }

  if (checkAuth()) {
    return <Redirect to="/dashboard" />
  }

  const phone_number = localStorage.getItem('phone_number')

  return(
    <React.Fragment>
      <AuthMainWrapper
        title="Sign In"
        authRedirectLink="/"
        authRedirectText="Account not activated?"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        submitText="Sign In"
        loading={props.isAuthenticating}
        showCheckBox={true}
        initialValues={{ phone_number }}
        forms={[
          {
            label: 'Phone Number',
            placeholder: 'Enter your phone number',
            name:'phone_number',
            disabled: !!phone_number,
          },
        ]}
      />
    </React.Fragment>
  );
}

export default ActivateLaunch;
