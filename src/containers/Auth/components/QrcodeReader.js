import { Button, Col, Row } from 'antd'
import React from 'react'
import { QrReader } from 'react-qr-reader'

const QrcodeReader = (props) => {
  const { onClose, visible, onSubmit } = props

  const handleError = (error) => {
    console.log('error', error)
  }

  
  return (
    <Row>
      <Col span={24}>
        <Row>
          <Col span={24}>
            {visible && (
              <QrReader
                constraints={{ facingMode: "environment" }}
                delay={1000}
                onError={handleError}
                style={{ width: "300px", height: "300px" }}
                onResult={(data) => {
                  if(data && data !== undefined && data !== "") {
                    onSubmit(data)
                  }
                }}
              />
            )}
          </Col>
        </Row>
        <Row style={{ marginTop: 5 }}>
          <Col span={24}>
            <Button onClick={() => onClose()} type='danger' style={{ width: '100%' }}>Stop scan</Button>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

export default QrcodeReader