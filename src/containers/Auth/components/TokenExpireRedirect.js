import { Col, message, Row } from 'antd';
import React from 'react'
import Button from '../../../components/Button';
import AppLayout from '../../../components/Layout/Layout';
import { getRefreshToken } from '../../../services/api/axiosDefaults';

const TokenExpireRedirect = (props) => {
  const {
    refreshingExpiredToken,
    refreshExpiredToken
  } = props

  const handleOnClick = () => {
    const refreshToken = getRefreshToken()
    if(refreshToken) {
      refreshExpiredToken(refreshToken)
    } else {
      message.error('Refresh token in available')
    }
    
  }
  
  return (
    <>
      <AppLayout authHeader={true} noFooter={true}>
        <Row className='auth-content'>
          <Col span={24}>
            <Row>
              <Col span={24} style={{ paddingTop: 100 }}>
                <span style={{ fontSize: 18 }}>
                  <span>Welcome back</span> <br/>
                  <span>It's been a while...</span>
                </span>
              </Col>
            </Row>
            <Row style={{ marginTop: 30 }}>
              <Col span={24}>
                <span>
                  <p>We need to refresh the application</p>
                  <p>
                    Please make sure you are connected to the internet
                  </p>
                </span>
              </Col>
            </Row>
            <Row style={{ marginTop: 20 }}>
              <Col span={24}>
                <Button text='Open HE Product App' 
                  loading={refreshingExpiredToken}
                  onClick={() => {
                    handleOnClick()
                  }}
                />
              </Col>
            </Row>
            </Col>
        </Row>
      </AppLayout>
    </>
  )
}

export default TokenExpireRedirect