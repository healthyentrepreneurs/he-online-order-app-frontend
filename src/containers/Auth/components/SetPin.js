import React from 'react';
import { Redirect } from 'react-router-dom';
import { checkAuth } from '../../../util/helpers/requireAuthentication';
import AuthMainWrapper from './main';

const SetPin = props => {
  const onFinish = (values) => {
    props.setPin(values.pin, props.history.location.state.key);
  }

  const onFinishFailed = (error) => {
  }

  const comparePins = ({ getFieldValue }) => ({
    validator(rule, value) {
      if (!value || getFieldValue('pin') === value) {
        return Promise.resolve();
      }
      return Promise.reject('The two pins that you entered do not match!');
    },
  })

  if (checkAuth()) {
    return <Redirect to="/dashboard" />
  }

  return(
    <AuthMainWrapper
      title="Create PIN"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      submitText="Create PIN"
      loading={props.settingPin}
      forms={[
        {
          label: 'Create PIN',
          placeholder: 'Enter your pin',
          name:'pin',
          inputmode:"numeric",
          pattern: "[0-9]*",
          rules: {
            max: 4,
            message: 'Pin cannot be more than 4 characters'
          }
        },
        {
          label: 'Confirm PIN',
          placeholder: 'Confirm your PIN',
          name:'confirm_pin',
          inputmode:"numeric",
          pattern: "[0-9]*",
          rules: comparePins
        }
      ]}
    />
  );
}

export default SetPin;
