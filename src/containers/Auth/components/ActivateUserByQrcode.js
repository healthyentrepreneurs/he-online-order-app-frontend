import { Col, Form, Input, Modal, Result, Row } from 'antd';
import React from 'react'
import { useLocation } from 'react-router-dom';
import Button from '../../../components/Button';
import AppLayout from '../../../components/Layout/Layout';
import { history } from '../../../util/helpers/browserHistory';

const ActivaUserByQrcode = (props) => {
  const { 
    activatingQrcode, 
    activateQrcodeSuccess, 
    error, 
    activatUserByQrcodeTries, 
    resetQrcodeScan 
  } = props

  const {search} = useLocation()
  const params = new URLSearchParams(search)
  const uid = params.get('uid') || null
  const authCode = params.get('auth_code') || null

  const onSubmit = (values) => {
    const payload = {
      authCode,
      uid
    }
    props.activateUserByQrcode(payload)
  }

  return (
    <AppLayout authHeader noFooter>
      <Row className='auth-content'>
        <Col span={24}>
          <Row style={{ marginTop: 30 }}>
            <Col span={24}>
              <Form
                layout='vertical'
                initialValues={{
                  uid: uid,
                  code: authCode
                }}
                onFinish={onSubmit}
              >
                <Form.Item name='uid' label='UID'>
                  <Input disabled />
                </Form.Item>
                <Form.Item name='code' label='Code'>
                  <Input disabled />
                </Form.Item>
                <Form.Item>
                  <Button
                    loading={activatingQrcode}
                    text="Activate Account" 
                    htmlType='submit'
                  />
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal
        visible={!activatingQrcode && activatUserByQrcodeTries}
        footer={null}
        onCancel={() => {
          resetQrcodeScan()
        }}
      >
        <Row>
          <Col span={24}>
            {activateQrcodeSuccess &&
              <Result
                status="success"
                title="Account Activated Successfully"
                extra={[
                  <Button
                    text="Login"
                    key="login"
                    onClick={() => {
                      history.replace("/login")
                    }}
                  />
                ]}
              />
            }
            {!activateQrcodeSuccess && 
              <Result
                status="error"
                title={error}
                extra={[
                  <Button 
                    text="Try Again"
                    key="login"
                    onClick={() => {
                      resetQrcodeScan()
                      history.replace("/")
                    }}
                  />
                ]}
              />
            }
          </Col>
        </Row>
      </Modal>
    </AppLayout>
  )
}

export default ActivaUserByQrcode