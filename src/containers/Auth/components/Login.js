import React, {useState} from 'react';
import { Redirect } from 'react-router-dom';
import { checkAuth } from '../../../util/helpers/requireAuthentication';

import AuthMainWrapper from './main';
import Icon from '@ant-design/icons/lib/components/Icon';

const ActivateLogin = props => {
  const [rerender, setRerender] = useState(false);
  const onFinish = (values) => {
    localStorage.setItem('phone_number', values.phone_number)
    props.login(values.phone_number, values.pin);
  }

  const onFinishFailed = (error) => {
  }

  if (checkAuth()) {
    return <Redirect to="/dashboard" />
  }

  const phone_number = localStorage.getItem('phone_number')

  return(
    <React.Fragment>
      <AuthMainWrapper
        title="Sign In"
        authRedirectLink="/"
        authRedirectText="Account not activated?"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        submitText="Sign In"
        loading={props.isAuthenticating}
        showCheckBox={true}
        initialValues={{ phone_number }}
        suffixElement = {<Icon component={() => (<img alt='edit_btn' width={20} height={20} src="/images/edit_image.png" />)} onClick={e => {
          
          let passwordInput = document.getElementById("basic_phone_number")
          passwordInput.value = ""
          passwordInput.classList.remove("ant-input-disabled")
          localStorage.removeItem("phone_number")
          setRerender(!rerender);
        }} />}
        forms={[
          {
            label: 'Phone Number',
            placeholder: 'Enter your phone number',
            name:'phone_number',
            disabled: !!phone_number,
            suffix: true
          },
          {
            label: 'PIN',
            placeholder: 'Enter your PIN',
            name:'pin',
            type: 'password',
            inputmode:"numeric",
            pattern: "[0-9]*",
            rules: {
              max: 4,
              message: 'Pin cannot be more than 4 characters'
            }
          }
        ]}
      />
    </React.Fragment>
  );
}

export default ActivateLogin;
