import { QrcodeOutlined } from '@ant-design/icons'
import React from 'react'


const ActivateQrCodeRedirectBtn = (props) => {
  const { onClick } = props

  return (
    <div 
      onClick={() => onClick()}
      style={{ width: 100, height: 100, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center',
        borderRadius: 20, backgroundColor: '#ffffff', border: '1px #b9babd solid', padding: 5, color: '#b9babd'
      }}
    >
      <QrcodeOutlined color='#b9babd' />
      <span style={{ textAlign: 'center' }}>
        Activate by QR code
      </span>
    </div>
  )
}

export default ActivateQrCodeRedirectBtn