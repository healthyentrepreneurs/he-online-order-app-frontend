import { connect } from 'react-redux';
import { Creators } from '../../services/redux/auth/actions';
import ActivateLogin from "./components/ActivateLogin";

const mapStateToProps = state => {
  return {
    activatingLogin: state.auth.activatingLogin,
    activateQrcodeUrlStr: state.auth.activateQrcodeUrlStr,
    activatingQrcode: state.auth.activatingQrcode,
    activateQrcodeSuccess: state.auth.activateQrcodeSuccess,
    activateLoginkey: state.auth.activateLogin,
    error: state.auth.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    activateLogin: (phone_number, password) => {
      dispatch(Creators.activateLoginRequest(phone_number, password));
    },
    activateUserByQrcode: (payload) => {
      dispatch(Creators.activateUserByQrcode(payload))
    },
    resetQrcodeScan: () => {
      dispatch(Creators.resetQrcodeScan())
    },
    setActivateLoginKey: (key) => {
      dispatch(Creators.setActivateLoginKey(key))
    },
    lauch: (phoneNumber) => {
      dispatch(Creators.launchRequest(phoneNumber))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ActivateLogin);

