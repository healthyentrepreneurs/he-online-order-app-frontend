import { connect } from "react-redux";
import { Creators } from "../../services/redux/auth/actions";
import LaunchRefresh from "./components/LaunchRefresh";

const mapStateToProps = (state) => {
	return {
		isAuthenticating: state.auth.isAuthenticating,
    isRefreshing: state.auth.isRefreshing
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		launch: (phone_number) => {
			dispatch(Creators.launchRequest(phone_number));
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LaunchRefresh);
