import { connect } from "react-redux";
import { Creators } from "../../services/redux/auth/actions";
import Login from "./components/Login";

const mapStateToProps = (state) => {
	return {
		isAuthenticating: state.auth.isAuthenticating,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		login: (phone_number, pin) => {
			dispatch(Creators.loginRequest(phone_number, pin));
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
