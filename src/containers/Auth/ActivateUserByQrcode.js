import { connect } from "react-redux";
import { Creators } from "../../services/redux/auth/actions";
import ActivateUserByQrcode from './components/ActivateUserByQrcode'

const mapStateToProps = state => {
  return {
    activatingLogin: state.auth.activatingLogin,
    activateQrcodeUrlStr: state.auth.activateQrcodeUrlStr,
    activatingQrcode: state.auth.activatingQrcode,
    activateQrcodeSuccess: state.auth.activateQrcodeSuccess,
    activatUserByQrcodeTries: state.auth.activatUserByQrcodeTries,
    error: state.auth.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    activateLogin: (phone_number, password) => {
      dispatch(Creators.activateLoginRequest(phone_number, password));
    },
    activateUserByQrcode: (payload) => {
      dispatch(Creators.activateUserByQrcode(payload))
    },
    resetQrcodeScan: () => {
      dispatch(Creators.resetQrcodeScan())
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ActivateUserByQrcode);
