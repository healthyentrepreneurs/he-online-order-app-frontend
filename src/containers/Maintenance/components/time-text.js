import { Col, Row } from 'antd'
import React from 'react'

const TimeText = ({ title, value }) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
      <Row style={{ height: '60%', paddingBottom: 5 }}>
        <Col span={24}>
          <span style={{ fontWeight: 'bold', fontSize: 24 }}>{value}</span>
        </Col>
      </Row>
      <Row style={{ height:'40%' }}>
        <Col span={24}>
          <span>{title}</span>
        </Col>
      </Row>
    </div>
  )
}

export default TimeText