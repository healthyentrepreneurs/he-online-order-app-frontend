import React from 'react'
import PropTypes from "prop-types"
import { Col, Row } from 'antd'
import TimeText from './time-text'

const Maintenance = ({
  days,
  hours,
  minutes,
  seconds
}) => {

  return (
    <Row style={{ height: '100%', padding: 20 }}>
      <Col span={24} style={{ height: '100%' }}>
        <Row style={{ padding: '10px 10px 10px 10px', marginBottom: 30 }}>
          <Col span={24} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start', position: 'relative', height: '210px' }}>
            <img width={150} height={150} src="images/touch/512.png" alt='logo' />
            <img width={70} height={70} src="images/tool.png" style={{ position: 'absolute', bottom: 0 }} alt='tools' />
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: '20%' }}>
            <span style={{ fontSize: 18, fontWeight: 'bold' }}>
              HE Product App
            </span>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: '20%' }}>
            <span style={{ fontSize: 18, fontWeight: 'bold' }}>
              undergoing maintenance
            </span>
          </Col>
        </Row>
        <Row style={{ marginTop: 20 }}>
          <Col span={24} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: '20%' }}>
            <span style={{ fontSize: 18, fontWeight: 'bold' }}>
              We are back in:
            </span>
          </Col>
        </Row>
        <Row style={{ marginBottom: 10 }}>
          <Col span={6}>
            <TimeText title="Days" value={days} />
          </Col>
          <Col span={6}>
            <TimeText title="Hours" value={hours} />
          </Col>
          <Col span={6}>
            <TimeText title="Minutes" value={minutes} />
          </Col>
          <Col span={6}>
            <TimeText title="Seconds" value={seconds} />
          </Col>
        </Row>
        <Row style={{ minHeight: '200px' }}>
          <Col span={24} style={{ padding: 10, backgroundColor: '#60ebd6', borderRadius: '10px' }}>
            <p>
              Dear CHE, due to the upgrade/ maintenance works the HE- PRODUCT APP will not be available between 27th, September 5:00 PM to 4th October 11:00 AM.
              Thank you for understanding.
            </p>
            <p>
              Musawo waffe, olw'emirimu gy'okulongoosa/ okuddaabiriza HE PRODUCT APP tegenda kubeerawo wakati wa 27th, September 5:00 PM Paka nga 4th October 11:00 AM. Weebale Nnyo.
            </p>
            <p>
              Nkulamwisa musawo, olw'okwendha okulongosa empereza yaife aya APP, Tukutegeza nti APP yaife teidha kubaku okuva nga 27th/September essawa 5pm paaka nga 4th/October essawa 11am.  Twetondha olw'okutataganizibwa okwo. Mwebale inho.
            </p>
            <p>
              Ahari iwe omushaho (CHE), ahabwokutereza empereza yaitu yokukorera order aha APP okugura emibazi, tekuza kubaho kuruga 27th September shaaha ikumi nemwe zomwazyo (5pm) kukoma 4th October shaaha itano zakasheeshe (11am). Webare munonga omushaho.
            </p>
            <p>
              Amoti yin ba CHE, pi yuba me dongo kede medo rwom onyo yiko ticere, HE PRODUCT APP abedo ame petye atic iyi akinakina nino dwe 27/09/2023 cakere cawa 11 me otyeno tunu kede naka inino dwe 4/10/2023 cawa 5 me odiko. Apwoyo ba CHE wa me amara.
            </p>
            <p>
              Ba customer bethu,
              thukabamanyisaya thuthi obuherezya bwethu okwa mobile app, sibwendisyabyaho eritsuka ebiro 27th september erihika ebiro 4th October. Wasingya kutsibu, HE
            </p>
            <p>
              Aba CHEs beitu abarungi, ahabo kutereza nokukora aha App yeitu yokuguriraho emibazi, App yeitu tengenda kubaho kutandika na biro 27th September Saha ikumi nemu  5:00pm zaryebajjo kuhika ebiro Bina 4th.octomber Saha itano zanyenkya Kara 11:00am. Mwebale aba CHEs beitu abarungi mukama abalinde.
            </p>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

Maintenance.propTypes = {
  days: PropTypes.string,
  hours: PropTypes.string,
  mins: PropTypes.string,
  seconds: PropTypes.string
}

export default Maintenance