import React from 'react'
import Maintenance from './components/maintenance';
import { Col, Row } from 'antd';
import useCountdown from './components/useCountdown';
import { padZero } from '../../util/helpers/commonHelper';
import { Redirect } from 'react-router-dom/cjs/react-router-dom.min';


const MaintenanceIndex = () => {
  
  const uptimeDate = process.env.REACT_APP_MAINTENANCE_DOWN_TIME || "Oct 4, 2023 8:00:00";

  const [ days, hours, minutes, seconds ] = useCountdown(uptimeDate)
  if(true) {
    return <Redirect to="/" />
  }
  
  return (
    <>
      <Row style={{ width: '100%', height: '100vh', }}>
        <Col style={{ width: '100%', height:'100%' }}>
          {(seconds < 0) ? (
            <Maintenance
            days="00"
            hours="00"
            minutes="00"
            seconds="00"
          />
          ) : (
            <Maintenance
            days={padZero(days, 2)}
            hours={padZero(hours, 2)}
            minutes={padZero(minutes, 2)}
            seconds={padZero(seconds, 2)}
          />
          )}
        </Col>
      </Row>
    </>
  )
}

export default MaintenanceIndex