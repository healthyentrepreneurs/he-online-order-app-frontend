const cacheName = "cache-v2";
const precacheResources = ["/", "index.html"];

// Install a service worker
self.addEventListener("install", (event) => {
  event.waitUntil(
    caches.open(cacheName).then((cache) => {
      return cache.addAll(precacheResources);
    })
  );
});

// Update a service worker
self.addEventListener("activate", (event) => {
  const cacheWhitelist = [];
  cacheWhitelist.push(cacheName);
  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.map((cache_name) => {
          if (!cacheWhitelist.includes(cache_name)) return caches.delete(cache_name);
        })
      );
    })
  );
});

// Cache and return requests
self.addEventListener("fetch", (event) => {
  event.respondWith(
    caches.match(event.request).then((cachedResponse) => {
      if (cachedResponse) {
        return cachedResponse;
      }
      return fetch(event.request);
    })
  );
});

// serviceworker.js
// self.addEventListener("notificationclick", function (event) {
//   console.log("notificationclick triggered");
//   // Close notification.
//   event.notification.close();
//   event.waitUntil(clients.openWindow("http://localhost:3000/dashboard"));
// });
// self.addEventListener("notificationclick", function (e) {
//   clients.matchAll().then(function (clis) {
//     var client = clis.find(function (c) {
//       c.visibilityState === "visible";
//     });
//     if (client !== undefined) {
//       client.navigate("http://localhost:3000/orders");
//       client.focus();
//     } else {
//       // there are no visible windows. Open one.
//       clients.openWindow("http://localhost:3000/dashboard");
//       notification.close();
//     }
//   });
// });
