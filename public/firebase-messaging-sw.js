importScripts("https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.1.1/firebase-messaging.js");

const firebaseConfig = {
  apiKey: "AIzaSyDLIMJ5BToRe1egZLnJuOm5igqLAxmbtz0",
  authDomain: "heorderapp.firebaseapp.com",
  databaseURL: "https://heorderapp.firebaseio.com",
  projectId: "heorderapp",
  storageBucket: "heorderapp.appspot.com",
  messagingSenderId: "406024613648",
  appId: "1:406024613648:web:529c163f8c89dd0990ff95",
  measurementId: "G-6505EFWP3X",
}

// firebase.initializeApp({
//   apiKey: "api-key",
//   authDomain: "project-id.firebaseapp.com",
//   databaseURL: "https://project-id.firebaseio.com",
//   projectId: "project-id",
//   storageBucket: "project-id.appspot.com",
//   messagingSenderId: "sender-id",
//   appId: "app-id",
//   measurementId: "G-measurement-id",
// });


firebase.initializeApp(
  firebaseConfig
)

const messaging = firebase.messaging();
messaging.onBackgroundMessage(function (payload) {
  if (Notification.permission == "granted") {
    
    try {
      const paymentData = JSON.parse(payload.data.extra_kwargs);
      var options = {
        body: payload.data.body,
        icon: "images/touch/128.png",
        vibrate: [100, 50, 100],
        data: {
          dateOfArrival: Date.now(),
          status: paymentData.status,
          orderId: paymentData.id,
          errorMessage: paymentData.message,
          payload: payload
        },
      };
    } catch (err) {
      var options = {
        body: payload.data.body,
        icon: "images/touch/128.png",
        vibrate: [100, 50, 100],
        data: {
          payload: payload
        },
      };
    }
    
    
    self.registration.showNotification(payload.data.title, options);
  }
});

// messaging.onMessage(function (payload) {
//     if (Notification.permission == "granted") {
//       const paymentData = JSON.parse(payload.data.extra_kwargs);
//       var options = {
//         body: payload.data.body,
//         icon: "images/touch/128.png",
//         vibrate: [100, 50, 100],
//         data: {
//           dateOfArrival: Date.now(),
//           status: paymentData.status,
//           orderId: paymentData.id
//         },
//       };
      
//       self.registration.showNotification(payload.data.title, options);
//     }
//   });

self.addEventListener("notificationclick", function (e) {
  
  var notification = e.notification;
  var action = e.action;
  var data = notification?.data
  const payloadData = JSON.parse(notification?.data?.payload?.data?.extra_kwargs)
  if (action === "close") {
    notification.close();
  } else if (data.payload && data.payload.notification && data.payload.notification.title === "Payment methods updated") {
    clients.openWindow(`${e.target.location.origin}/sync-payment`)
  } else if((data.payload && data.payload.notification &&  data.payload.notification.title === "Products list updated") 
    || (data.payload && data.payload.notification &&  data.payload.notification.title === "Products updated")) {
    clients.openWindow(`${e.target.location.origin}/sync-products`)
  } else if(notification && notification.title === "Product list for user group updated") {
    clients.openWindow(`${e.target.location.origin}/sync-user-group`)
  } else if(data?.payload?.notification?.title === "Promotions") {
    clients.openWindow(`${e.target.location.origin}/sync-promotions?refresh=${payloadData?.refresh || false}`)
  } else {
    try {
      let status = "details"
      if (data.status === "failed" ) {
        status = "failure"
      } else if (data.status === "successful") {
        status = "successful"
      }
      if(payloadData.order_id) {
        clients.openWindow(`${e.target.location.origin}/cart/${status}/${payloadData?.order_name}?msg=${data?.errorMessage}`);
      } else if(payloadData.invoice_id) {
        const ACCOUNTING_TAB = window.localStorage.getItem("ACCOUNTING_TAB") || 1
        const baseUrl = ACCOUNTING_TAB == 1 ? 'credit-invoices' : 'open-payments'
        clients.openWindow(`${e.target.location.origin}/${baseUrl}/?status=${status}&invoice_id=${payloadData.invoice_id}&msg=${payloadData.message}&amount=${payloadData.amount}`);
      }
    } catch (err) {
      clients.openWindow(`${e.target.location.origin}/dashboard`)
    }
    
    notification.close();
  }
});

self.addEventListener("notificationclose", function (e) {});
