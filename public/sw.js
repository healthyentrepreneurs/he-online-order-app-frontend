importScripts(
	"https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js"
);
//importScripts('swenv.js');
//console.log("process.env is ", process.env && process.env.REACT_APP_BASE_URL);

if (workbox) {
	console.log(`Yay! Workbox is loaded`);
	try {
		workbox.precaching.precacheAndRoute([{"revision":"f3d228c0056f6b8c015e81c6bcf5349c","url":"asset-manifest.json"},{"revision":"6e1267d9d946b0236cdf6ffd02890894","url":"favicon.ico"},{"revision":"762207d3a41e525c9c13e5591f4351d9","url":"firebase-messaging-sw.js"},{"revision":"77ed97adcef94720ac6c98e38fef8c33","url":"images/logo.png"},{"revision":"bf59dee01afac52872b18bd010e3fdbb","url":"images/touch/128.png"},{"revision":"681cb4ebc5f3d7f07734dd87cededc6d","url":"images/touch/192.png"},{"revision":"d01e939c3daf6f95312190a3fdb7ba2d","url":"images/touch/256.png"},{"revision":"6f39167130b62ebf124b263e0f7fac4b","url":"images/touch/384.png"},{"revision":"ff22c85076ce42bac63e94e2da06d65d","url":"images/touch/512.png"},{"revision":"2f4480cad7328728245022fd3044c164","url":"index.html"},{"revision":"d502d091331c8dd1a683ac7d0b55cb4d","url":"manifest.json"},{"revision":"786c64769fb8d4fff540e956a3c65206","url":"precache-manifest.786c64769fb8d4fff540e956a3c65206.js"},{"revision":"61c27d2cd39a713f7829422c3d9edcc7","url":"robots.txt"},{"revision":"59e4d93df826793d132b4d0b8a24c398","url":"service-worker.js"},{"revision":"da4e1dd5d5e0c3727b357b9204d33d78","url":"static/css/10.a022f29d.chunk.css"},{"revision":"9c48e2b73ee03b481d3c55c2ff2935aa","url":"static/css/11.bb20c6c1.chunk.css"},{"revision":"c4bb85936ef365950cb690a8fc8cb510","url":"static/css/12.bb20c6c1.chunk.css"},{"revision":"b62228b9b06afb636f757ee9977378eb","url":"static/css/13.bb20c6c1.chunk.css"},{"revision":"b0e6a0e0d93c9a6cb02cc0d9de427777","url":"static/css/14.04ad8676.chunk.css"},{"revision":"1CFJqTYi75eiUnEqUoHP2py9x7qphGpMHZ","url":"static/css/15.13970a13.chunk.css"},{"revision":"1CFJqTYi75eiUnEqUoHP2py9x7qphGpMHZ","url":"static/css/17.63555b2e.chunk.css"},{"revision":"1CFJqTYi75eiUnEqUoHP2py9x7qphGpMHZ","url":"static/css/18.1b0af91e.chunk.css"},{"revision":"47f5025a70c61a89635c3469a7ff85c4","url":"static/css/6.7a917956.chunk.css"},{"revision":"a42bfa3fd0ab623fff63c4b228ecf18e","url":"static/css/7.2ed1469f.chunk.css"},{"revision":"05b4c6cff85602416ef1e34ebfb683e7","url":"static/css/9.a022f29d.chunk.css"},{"revision":"1CFJqTYi75eiUnEqUoHP2py9x7qphGpMHZ","url":"static/css/main.70268191.chunk.css"},{"revision":"1CFJqTYi75eiUnEqUoHP2py9x7qphGpMHZ","url":"static/js/0.b2340d64.chunk.js"},{"revision":"0bf3e95f4002c2e94db6ce7c884ad52d","url":"static/js/1.84d18576.chunk.js"},{"revision":"ff55f4de726785604839c9becd8e6a76","url":"static/js/10.dfae1b91.chunk.js"},{"revision":"af8f9f0f46262daddc319d2f30e17f49","url":"static/js/11.0c28408f.chunk.js"},{"revision":"74d79f3466e8c27a8ce1e33400a8964a","url":"static/js/12.c2b16c69.chunk.js"},{"revision":"0a21568a09f3719c807870ec0e36900b","url":"static/js/13.61d2dcc1.chunk.js"},{"revision":"03f3bcd8d02b329dd334fe02f1ec1f11","url":"static/js/14.aea30c32.chunk.js"},{"revision":"87523a18fc6b7777e60191b592378123","url":"static/js/15.71f0a315.chunk.js"},{"revision":"095d4fcbbe3af0b9e9a618f67c740777","url":"static/js/16.e65613e3.chunk.js"},{"revision":"c360eff85ca112d098c080b19dff9b78","url":"static/js/16.e65613e3.chunk.js.LICENSE.txt"},{"revision":"00293beec62c8c59befcbebdaa46f849","url":"static/js/17.66a2421e.chunk.js"},{"revision":"81feea8e6a80ad0f340fbb0c97b54a24","url":"static/js/18.8e7bd7d7.chunk.js"},{"revision":"f6b21ca67210830007ffdeea31404859","url":"static/js/19.02626c19.chunk.js"},{"revision":"59a9abf95d4472389e53af1ab01e64e4","url":"static/js/2.b865cd14.chunk.js"},{"revision":"cb2a3706d798456fbebdb9f6ba1e38d1","url":"static/js/3.edf92e7e.chunk.js"},{"revision":"985305658097b22cdf83c0d0ee43f23d","url":"static/js/6.3e0bcb3c.chunk.js"},{"revision":"cea3eebf195d04637447318df6baed71","url":"static/js/6.3e0bcb3c.chunk.js.LICENSE.txt"},{"revision":"8dea691af2a77fc7ec7c36dfbd050b65","url":"static/js/7.4d7ceaab.chunk.js"},{"revision":"c360eff85ca112d098c080b19dff9b78","url":"static/js/7.4d7ceaab.chunk.js.LICENSE.txt"},{"revision":"bfad443cbb9e6713f3b682f530073a43","url":"static/js/8.62aa4f48.chunk.js"},{"revision":"9740fcbaf4a97a1b7fe9f9cf1b106d5b","url":"static/js/9.57d6d12e.chunk.js"},{"revision":"6f402d768d9226208aaa03edbd04a4c2","url":"static/js/main.a624b3fb.chunk.js"},{"revision":"4dcfbe20bde70ab4bdb2e67123586172","url":"static/js/runtime-main.5f6c038b.js"},{"revision":"b361b93b393c62ca592c559e253fa872","url":"static/media/black-dot.b361b93b.svg"},{"revision":"a2277c2f8f54fd773f6094e282365e40","url":"static/media/cancel.a2277c2f.svg"},{"revision":"55f94aae54ccfc19c981cff69eedd7ef","url":"static/media/cart-delete-btn.55f94aae.svg"},{"revision":"fd31681bd9c83b9537ab964bc9df9668","url":"static/media/check-mark.fd31681b.svg"},{"revision":"642bd792680571640f4c5f061b80cbc3","url":"static/media/Close_in_circle.642bd792.svg"},{"revision":"9c6f6b4556cbcda2f2478981a0393748","url":"static/media/confirmed.9c6f6b45.svg"},{"revision":"c9a7f746416447cd931b8313c50960c7","url":"static/media/default.c9a7f746.png"},{"revision":"45b5614de11f0c414b483e9a03c68d96","url":"static/media/empty-cart.45b5614d.svg"},{"revision":"93af711cf92cda9c078921f7018a235d","url":"static/media/error.93af711c.svg"},{"revision":"60a35575fe0cf67af663530ba56ed9e9","url":"static/media/he-full-logo.60a35575.svg"},{"revision":"c45cac15448ac2a045a75c934c63a887","url":"static/media/he-small-logo.c45cac15.svg"},{"revision":"50cad250d530c08fc9aed42b2a976d70","url":"static/media/left-warehouse.50cad250.svg"},{"revision":"73c5f0d8fc84e9c1e723f7ef31bfbeff","url":"static/media/price-arrow-down.73c5f0d8.svg"},{"revision":"1CFJqTYi75eiUnEqUoHP2py9x7qphGpMHZ","url":"static/media/price-arrow-up.38ebfc5d.svg"},{"revision":"1CFJqTYi75eiUnEqUoHP2py9x7qphGpMHZ","url":"static/media/price-decreased.38fde2e3.svg"},{"revision":"1CFJqTYi75eiUnEqUoHP2py9x7qphGpMHZ","url":"static/media/price-increased.131ed967.svg"},{"revision":"7d704480976b0f142da844923205d2a2","url":"static/media/red-dot.7d704480.svg"},{"revision":"816e3323b97bd97947311cad073c672e","url":"static/media/search.816e3323.svg"},{"revision":"9e865a72b11527f28a315f78fa2821af","url":"static/media/shopping-cart-small.9e865a72.svg"},{"revision":"fd31681bd9c83b9537ab964bc9df9668","url":"static/media/success.fd31681b.svg"},{"revision":"03909fbb7502377673923c4df028ab44","url":"static/media/to-be-confirmed.03909fbb.svg"},{"revision":"55a8220f9af57b37a82f06c6fb6763ec","url":"static/media/Tooltip.55a8220f.svg"}]);

		// workbox.routing.registerRoute(
		// 	// /https:\/\/he-staging\.herokuapp\.com\/api/, //handle url more dynamically.
		// 	({url}) => url.href.indexOf('/profile/?salt') !== -1,
		// 	new workbox.strategies.CacheFirst({
		// 		cacheName: "updated-endpoints-cache",
		// 		plugins: [
		// 			new workbox.expiration.ExpirationPlugin({
		// 				maxEntries: 50,
		// 				maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
		// 			}),
		// 		],
		// 	})
		// );

		workbox.routing.registerRoute(
			// /https:\/\/he-staging\.herokuapp\.com\/api/, //handle url more dynamically.
			({url}) => url.pathname.startsWith('/api/') && url.href.indexOf('/profile/?salt') === -1,
			new workbox.strategies.CacheFirst({
				cacheName: "endpoints-cache",
				plugins: [
					new workbox.expiration.ExpirationPlugin({
						maxEntries: 50,
						maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
					}),
				],
			})
		);

		//cache images
		workbox.routing.registerRoute(
			//({url}) => url.startsWith('https://he-products-images.s3-us-west-2.amazonaws.com'),
			// new RegExp('^https://he-products-images.s3-us-west-2.amazonaws.com'),
			/.*\.(?:png|jpg|jpeg|svg|gif)/,
			new workbox.strategies.StaleWhileRevalidate({
				cacheName: 'images-cache',
				plugins: [
					new workbox.expiration.ExpirationPlugin({
						maxEntries: 400,
						maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
						purgeOnQuotaError: true // Automatically cleanup if quota is exceeded
					})
				]
			})
		)

		// Cache the Google Fonts stylesheets with a stale-while-revalidate strategy.
		workbox.routing.registerRoute(
			({ url }) => url.origin === "https://fonts.googleapis.com",
			new workbox.strategies.StaleWhileRevalidate({
				cacheName: "google-fonts-stylesheets",
			})
		);

		// Cache the underlying font files with a cache-first strategy for 1 year.
		workbox.routing.registerRoute(
			({ url }) => url.origin === "https://fonts.gstatic.com",
			new workbox.strategies.CacheFirst({
				cacheName: "google-fonts-webfonts",
				plugins: [
					new workbox.cacheableResponse.CacheableResponsePlugin({
						statuses: [0, 200],
					}),
					new workbox.expiration.ExpirationPlugin({
						maxAgeSeconds: 60 * 60 * 24 * 365,
						maxEntries: 30,
					}),
				],
			})
		);

		// for SPA navigation routes to work offline on page refresh...
		const handler = workbox.precaching.createHandlerBoundToURL("/index.html");
		const navigationRoute = new workbox.routing.NavigationRoute(handler, {
			// allowlist: [new RegExp("/blog/")],
			// denylist: [new RegExp("/blog/restricted/")],
		});
		workbox.routing.registerRoute(navigationRoute);
	} catch (err) {
		console.log("An error occurred on workbox ", err);
	}
} else {
	console.log(`Boo! Workbox didn't load`);
}

self.addEventListener("activate", event => {
	clients.claim();
  console.log('Ready!');
})

self.addEventListener("message", (event) => {
	if (event.data && event.data === "SKIP_WAITING") {
		console.log('SKIP_WAITING MESSAGE EVENT TRIGGERED!')
		self.skipWaiting();
	}
});
