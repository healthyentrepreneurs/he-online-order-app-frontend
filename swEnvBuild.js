//swEnvBuild.js - script that is separate from webpack
const envVars = require("dotenv").config({ path: './.env.production' }); // make sure you have '.env' file in pwd
const fs = require("fs");

fs.writeFileSync(
	"./build/swenv.js",
	`
		const process = {
		  env: {
		    REACT_APP_BASE_URL: "${envVars.parsed.REACT_APP_BASE_URL}"
		  }
		}
	`
);

